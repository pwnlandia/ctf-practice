import requests
import json
import time
import sys
import random

apikey = 'c17625c41e2d921ee937b54fb16f467145eabf2b'
stock = sys.argv[1]
print("stock is: " + stock)
venue = sys.argv[2]
print("venue is: " + venue)
account = sys.argv[3]
print("account is: " + account)
base_url = 'https://api.stockfighter.io/ob/api'
book_url = base_url + '/venues/' + venue + '/stocks/' + stock
quote_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/quote'
orders_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/orders'
authheader = {'X-Starfighter-Authorization' : apikey}

def isOpen(orderid):
	orderid_url = orders_url+'/'+str(orderid)
	isopen=True
	r = requests.get(orderid_url, headers=authheader)
	r_data = r.json()
	isopen = r_data['open']
	return isopen

def issueLimitOrder(amount,direction,price):
	myorder['qty'] = amount
	myorder['direction'] = direction
	myorder['price'] = price
	myorder['orderType'] = 'limit'
	r = requests.post(orders_url, data=json.dumps(myorder), headers=authheader)
	orderid = r.json()['id']
	print("    "+direction+" "+str(amount)+" at "+str(price))
	return orderid

def doLimitOrder(amount,direction,price):
	myorder['direction'] = direction
	myorder['orderType'] = 'limit'
	myorder['qty'] = amount
	myorder['price'] = price
	r = requests.post(orders_url, data=json.dumps(myorder), headers=authheader)
	orderid = r.json()['id']
	print("    " +direction+" at "+str(price)+" order: "+str(orderid))
	while isOpen(orderid):
		time.sleep(5)

# Set up the order
myorder = {
  'account' : account,
  'venue' : venue,
  'symbol' : stock,
  'price' : 0,  #$44.00
  'qty' : 100,
  'direction' : '',
  'orderType' : 'limit'  # See the order docs for what a limit order is
}

s_orderid=doLimitOrder(100,'sell',100000)
s_orderid=issueLimitOrder(110,'buy',9500)
s_orderid=issueLimitOrder(10,'sell',1000)
