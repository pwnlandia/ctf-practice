Two aggressive traders: one selling, one buying

Strategy is to put buy limit order at low price and sell limit order at
high price.

When either order is filled, try to quickly flip for a profit
so that you can re-establish low and high positions again.
