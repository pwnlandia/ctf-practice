# Alexander Simchuk, Stockfighter lvl 2 solution

import requests
import json

# lines 7-26 are preparing the order just like the API Ruby code shows it. The only changes made
# were altering the ruby code to match python syntax
apikey = "cadbf19806a7a595f388a2f533d85bc477bbcf19" # use your API key here
venue = "ETKBEX" # this will be different every time the level is reset
stock = "EJYW" # this will be different every time the level is reset
base_url = "https://api.stockfighter.io/ob/api"

account = "SWB1886430"  # Printed in bold in the level instructions. Replace with your real value.

# Set up the order

qty = 100

order = {
        'account' : account,
        'venue' : venue,
        'symbol' : stock,
        'price' : 8000,  # $80.00, this will need to be changed for the current stock being purchased
        'qty' : qty,
        'direction' : 'buy',
        'orderType' : 'limit'  # See the order docs for what a limit order is
        }

# the rest of the code does the purchasing. The logic behind the code is pretty straightforward:
# 1) Place an order for 100 shares based on the data we provided earlier
# 2) Save the ID of the order
# 3) While the number of shares purchased is less than the order placed
#  a) proceed to check the number of shares currently bought
#  b) if bought == qty, proceed to next order
# 4) repeat for a 1000 times, totaling 100,000 shares
for i in range(0,1000):
    r = requests.post(base_url + "/venues/" + venue + "/stocks/" + stock + "/orders",
                      data = json.dumps(order),
                      headers = {'X-Starfighter-Authorization' : apikey}
                     )
    if r.status_code == requests.codes.ok:
        data = json.loads(r.text)
        current = data['id']

        print current # status message

        # prepare the structure to check the current order status
        status = {
                 'id' : current,
                 'venue' : venue,
                 'stock' : stock
                 }
        total = 0
        while total < qty:
            r2 = requests.get(base_url + "/venues/" + venue + "/stocks/" + stock + "/orders/" + str(current),
                              data = json.dumps(status),
                              headers = {'x-starfighter-authorization' : apikey}
                             )
            data2 = json.loads(r2.text)
            total = data2['totalFilled']
            print total # status message

    else:
        print "Purchase failed"
        break
