import requests
import json
import time
import sys
import random

apikey = 'c17625c41e2d921ee937b54fb16f467145eabf2b'
stock = sys.argv[1]
print("stock is: " + stock)
venue = sys.argv[2]
print("venue is: " + venue)
account = sys.argv[3]
print("account is: " + account)
target = int(sys.argv[4])
print("target is: {}".format(target))
base_url = 'https://api.stockfighter.io/ob/api'
book_url = base_url + '/venues/' + venue + '/stocks/' + stock
quote_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/quote'
orders_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/orders'
authheader = {'X-Starfighter-Authorization' : apikey}

def get_quote():
        ask,bid,last = 0,0,0
        while (ask==0) or (bid==0) or (last==0):
                r = requests.get(quote_url, headers=authheader)
                json_data = r.json()
                if 'ask' in json_data:
                        ask = json_data['ask']
                if 'bid' in json_data:
                        bid = json_data['bid']
                if 'last' in json_data:
                        last = json_data['last']
        return ask,bid,last

def get_maxbid_minask():
	ask,bid = 0,0
	while (ask==0) or (bid==0):
		r = requests.get(book_url, headers=authheader)
		json_data = r.json()
		if json_data['bids']:
			foo = [f['price'] for f in json_data['bids']]
			bid=max(foo)
		if json_data['asks']:
			foo = [f['price'] for f in json_data['asks']]
			ask=min(foo)
	return(ask,bid)

def isOpen(orderid):
	orderid_url = orders_url+'/'+str(orderid)
	isopen=True
	r = requests.get(orderid_url, headers=authheader)
	r_data = r.json()
	isopen = r_data['open']
	return isopen

def cancelOrder(orderid):
	orderid_url = orders_url+'/'+str(orderid)
	gross=0
	r = requests.post(orderid_url+"/cancel",headers=authheader)
	r_data = r.json()
	filled=r_data['totalFilled']
	if r_data['fills']:
		for f in r_data['fills']:
			gross = gross + f['price']*f['qty']
	return filled,gross

def issueLimitOrder(amount,direction,price):
	myorder['qty'] = amount
	myorder['direction'] = direction
	myorder['price'] = price
	myorder['orderType'] = 'limit'
	r = requests.post(orders_url, data=json.dumps(myorder), headers=authheader)
	r_data = r.json()
	orderid = r_data['id']
	print("    "+direction+" "+str(amount)+" at "+str(price))
	return orderid

def doLimitOrder(amount,direction,price):
	filled=0
	gross=0
	myorder['direction'] = direction
	myorder['orderType'] = 'limit'
	while filled < amount:
		myorder['qty'] = amount-filled
		myorder['price'] = price
		r = requests.post(orders_url, data=json.dumps(myorder), headers=authheader)
		orderid = r.json()['id']
		print("    " +direction+" at "+str(price)+" order: "+str(orderid))
		time.sleep(5)
		ofill,ogross=cancelOrder(orderid)
		if ofill:
			filled = filled+ofill
			gross = gross+ogross
			print("    Limit fill: "+str(ofill)+" Total fill: "+str(filled)+" Total gross: "+str(gross)+" AvgPrice="+str(gross/(100*filled)))
		if direction=='buy':
			price=price+200
		else:
			price=price-200
	return filled,gross

# Set up the order
myorder = {
  'account' : account,
  'venue' : venue,
  'symbol' : stock,
  'price' : 0,  #$44.00
  'qty' : 100,
  'direction' : '',
  'orderType' : 'limit'  # See the order docs for what a limit order is
}

def update_ask_bid(m_ask,m_bid):
	ask,bid = get_maxbid_minask()
	m_ask=0.2*ask+0.8*m_ask
	m_bid=0.2*bid+0.8*m_bid
	print("    m_ask("+str(m_ask)+") ask("+str(ask)+")   "+"m_bid("+str(m_bid)+") bid("+str(bid)+")")
	return m_ask,m_bid

def update_ask_bid_last(m_ask,m_bid,m_last):
	ask,bid,last = get_quote()
	m_ask=0.2*ask+0.8*m_ask
	m_bid=0.2*bid+0.8*m_bid
	m_last=0.2*last+0.8*m_last
	print("    m_ask("+str(int(m_ask))+") ask("+str(ask)+") m_bid("+str(int(m_bid))+") bid("+str(bid)+") m_last("+str(int(m_last))+") last("+str(last)+")")
	return m_ask,m_bid,m_last

shares=0
cash=0
cash_left=100000*target
ask,bid,last=get_quote()
m_ask,m_bid,m_last=update_ask_bid_last(1.0*ask,1.0*bid,1.0*last)
for i in range(5):
    time.sleep(1)
    m_ask,m_bid,m_last=update_ask_bid_last(m_ask,m_bid,m_last)

i=0
while True:
	i=i+1
	avg_needed=int(cash_left/(100000-shares))
	print("Round {}: Shares={}, AvgNeeded={}".format(i,shares,avg_needed))
	b_orderid=issueLimitOrder(3000,'buy',min(int(m_ask),avg_needed))
	if shares > 1000:
    		s_orderid=issueLimitOrder(500,'sell',int(avg_needed+500))
	else:
    		s_orderid=issueLimitOrder(10,'sell',int(avg_needed+1000))
	iters = 0
	while (isOpen(b_orderid)) and (isOpen(s_orderid)) and (iters < 10):
    		m_ask,m_bid,m_last=update_ask_bid_last(m_ask,m_bid,m_last)
    		time.sleep(2)
    		iters=iters+1
	filled,gross = cancelOrder(b_orderid)
	if filled:
		print("    Bought: shares="+str(filled)+"  gross="+str(gross)+" AvgPrice="+str(gross/(100*filled)))
		shares = shares+filled
		cash_left = cash_left-gross
	filled,gross = cancelOrder(s_orderid)
	if filled:
		print("    Sold: shares="+str(filled)+"  gross="+str(gross)+" AvgPrice="+str(gross/(100*filled)))
		shares = shares-filled
		cash_left = cash_left+gross
