require 'rubygems'
require 'httparty'
require 'json'

apikey = "c17625c41e2d921ee937b54fb16f467145eabf2b"
venue = "ARSTEX"   # Replace this with your real value.
stock = "ARI"  #Fun fact: Japanese programmers often use "hogehoge" where Americans use "foobar."  You should probably replace this with your real value.
base_url = "https://api.stockfighter.io/ob/api"

account = "HAS2338831"  # Printed in bold in the level instructions. Replace with your real value.
	# Set up the order

order = {
  "account" => account,
  "venue" => venue,
  "symbol" => stock,
  "price" => 6108,  #$250.00 -- probably ludicrously high
  "qty" => 1000,
  "direction" => "buy",
  "orderType" => "limit"  # See the order docs for what a limit order is
}


for i in 0..100 do
	response = HTTParty.post("#{base_url}/venues/#{venue}/stocks/#{stock}/orders",
				 :body => JSON.dump(order),
				 :headers => {"X-Starfighter-Authorization" => apikey}
				 )

	#Now we analyze the order response

	puts response.body
end

### Here is what the response looked like.

# {
#   "ok": true,
#   "symbol": "HOGE",
#   "venue": "FOOEX",
#   "direction": "buy",
#   "originalQty": 100,
#   "qty": 0,
#   "price": 25000,
#   "orderType": "limit",
#   "id": 6408,
#   "account": "HB61251714",
#   "ts": "2015-08-18T04:00:08.340298024+09:00",
#   "fills": [
#     {
#       "price": 5960,
#       "qty": 100,
#       "ts": "2015-08-18T04:00:08.340299592+09:00"
#     }
#   ],
#   "totalFilled": 100,
#   "open": false
# }

# As we can see, I got 100 fills of the 100 shares I ordered.  Whee!
# This order is now closed (open: false).
