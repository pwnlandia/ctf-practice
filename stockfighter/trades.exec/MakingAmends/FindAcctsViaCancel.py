import concurrent.futures
import collections
import requests
import re
import json
import time
import sys
import random

apikey = 'c17625c41e2d921ee937b54fb16f467145eabf2b'
stock = sys.argv[1]
print("stock is: " + stock)
venue = sys.argv[2]
print("venue is: " + venue)
account = sys.argv[3]
print("account is: " + account)
orderid = sys.argv[4]
print("orderid is: " + orderid)
base_url = 'https://api.stockfighter.io/ob/api'
book_url = base_url + '/venues/' + venue + '/stocks/' + stock
quote_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/quote'
orders_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/orders'
authheader = {'X-Starfighter-Authorization' : apikey}

rex = re.compile("[A-Z]{3}[0-9]{8}")
pool = concurrent.futures.ThreadPoolExecutor(max_workers=8)

def cancelOrder(orderid):
	orderid_url = orders_url+'/'+str(orderid)
	gross=0
	r = requests.post(orderid_url+"/cancel",headers=authheader)
	r_data = r.json()
	if 'error' in r_data:
		return rex.findall(r_data['error'])
	return []

ctr = collections.Counter()
done = 0
for r in pool.map(cancelOrder, range(int(orderid))):
	done += 1
	print("done={} mc=#{}".format(done,ctr.most_common(5)))
	ctr.update(r)
#print(ctr.most_common(5))
for key in ctr.keys():
	print("key is: " + str(key))
