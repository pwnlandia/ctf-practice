import requests
import json
import time
import sys

apikey = 'c17625c41e2d921ee937b54fb16f467145eabf2b'
orders_url = 'https://api.stockfighter.io/gm/instances/22808/judge'
authheader = {'X-Starfighter-Authorization' : apikey}

myorder = {
  'account' : "MLB27609927",
  'explanation_link' : "https://oregonctf.org/stockfighter",
  'executive_summary' : "We dump out the price of the stock, the estimated net asset value per account, and the number of shares each account has over time.  In examining the data.... One of these things is not like the others -- (Big Bird) https://www.youtube.com/watch?v=ueZ6tvqhk8U"}

r = requests.post(orders_url, json=myorder, headers=authheader)
print(r.text)
