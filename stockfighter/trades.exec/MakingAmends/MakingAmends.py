import collections
from dateutil import parser
import datetime
import threading
import requests
import re
import os
import json
import time
import sys
import random
import websocket
import pickle
import pytz

apikey = 'c17625c41e2d921ee937b54fb16f467145eabf2b'
stock = sys.argv[1]
print("stock is: " + stock)
venue = sys.argv[2]
print("venue is: " + venue)
account = sys.argv[3]
print("account is: " + account)
base_url = 'https://api.stockfighter.io/ob/api'
orders_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/orders'
authheader = {'X-Starfighter-Authorization' : apikey}

rex = re.compile("[A-Z]{3}[0-9]{8}")

def cancelOrder(orderid):
    orderid_url = orders_url+'/'+str(orderid)
    gross=0
    r = requests.post(orderid_url+"/cancel",headers=authheader)
    r_data = r.json()
    if 'error' in r_data:
        return rex.findall(r_data['error'])
    else:
        print(r_data)
    return []

isLast = lambda order: order['standingComplete'] or order['incomingComplete']

def logAccount(stock,account,zerotime):
    os.makedirs(stock+"/transactions/" + account, exist_ok=True)
    fd = open(stock+'/'+account+'.txt','w')
    ws_base_url = 'wss://api.stockfighter.io/ob/api/ws/' + account
    ws_orders_url  = ws_base_url + '/venues/' + venue + '/executions/stocks/' + stock

    while True:
        ws = websocket.create_connection(ws_orders_url)
        if ws:
            break
    order_raw = ws.recv()
    order_data = json.loads(order_raw)
    shares = cash = 0
    counter = 0
    while True:
        try:
            order_raw = ws.recv()
        except Exception as e:
            ws = websocket.create_connection(ws_orders_url)
            continue
        order_data=json.loads(order_raw)
        if isLast(order_data):
            pickle.dump(order_data,
                    open(stock+"/transactions/{}/{}.pkl".format(account,counter),
                        "wb"))
            counter += 1
            orderid = order_data['order']['id']
            if orderid:
                dt_raw=parser.parse(order_data['filledAt'])
                dt=dt_raw.replace(tzinfo=pytz.utc)
                timestamp=(dt-zerotime).total_seconds()
                shares,cash=logEntry(fd,timestamp,order_data,shares,cash)

def logEntry(fd,timestamp,od,shares,cash):
    fills,gross=0,0
    direction=od['order']['direction']
    totalFilled=od['order']['totalFilled']
    if od['order']['fills']:
        for f in od['order']['fills']:
            gross = gross + f['price']*f['qty']
            fills = fills + f['qty']
        originalQty=od['order']['originalQty']
        price=od['order']['fills'][0]['price']
        order=od['order']['id']
        if direction=='buy':
            shares = shares+fills
            cash = cash-gross
        else:
            shares = shares-fills
            cash = cash+gross
        fd.write("{} {} ({} {} {} {} {}) cash={} shares={}\n".format(timestamp,cash+(shares*price),order,direction,totalFilled,originalQty,price,cash,shares))
        fd.flush()
        os.fsync(fd.fileno())
    return (shares, cash)

ctr = collections.Counter()
os.makedirs(stock,exist_ok=True)
if not os.path.isdir(stock):
    raise

#for i in range(1,1000):
i=0
now_raw=datetime.datetime.now()
now=now_raw.replace(tzinfo=pytz.timezone('US/Pacific'))
while True:
    i = i+1
    acct = cancelOrder(i)
    if acct and (acct[0] not in ctr.keys()):
        print("New acct: "+str(acct[0]))
        ctr.update(acct)
        threading.Thread(target=logAccount,args=(stock,acct[0],now)).start()
