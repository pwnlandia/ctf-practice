import requests
import websocket
import json
import time
import sys

apikey = 'c17625c41e2d921ee937b54fb16f467145eabf2b'
stock = sys.argv[1]
print("stock is: " + stock)
venue = sys.argv[2]
print("venue is: " + venue)
account = sys.argv[3]
print("account is: " + account)
base_url = 'https://api.stockfighter.io/ob/api'
ws_base_url = 'wss://api.stockfighter.io/ob/api/ws/' + account
book_url = base_url + '/venues/' + venue + '/stocks/' + stock
quote_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/quote'
orders_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/orders'
authheader = {'X-Starfighter-Authorization' : apikey}
ws_orders_url  = ws_base_url + '/venues/' + venue + '/executions/stocks/' + stock

ws = websocket.create_connection(ws_orders_url)
while True:
	order = ws.recv()
	od = json.loads(order)
	print(od)
