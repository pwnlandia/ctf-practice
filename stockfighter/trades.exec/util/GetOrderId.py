import requests
import json
import time
import sys
import random

apikey = 'c17625c41e2d921ee937b54fb16f467145eabf2b'
stock = sys.argv[1]
print("stock is: " + stock)
venue = sys.argv[2]
print("venue is: " + venue)
account = sys.argv[3]
print("account is: " + account)
orderid = sys.argv[4]
print("orderid is: " + orderid)
base_url = 'https://api.stockfighter.io/ob/api'
book_url = base_url + '/venues/' + venue + '/stocks/' + stock
quote_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/quote'
orders_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/orders'
authheader = {'X-Starfighter-Authorization' : apikey}

def getOrder(orderid):
	orderid_url = orders_url+'/'+str(orderid)
	isopen=True
	r = requests.get(orderid_url, headers=authheader)
	r_data = r.json()
	print(r_data)

getOrder(orderid)
