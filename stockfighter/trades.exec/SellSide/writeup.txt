Get the orderbook over time and track an exponentially weighted moving
average of both the maximum bid and the minimum ask.  (m_bid and m_ask).

Then, do rounds of selling/buying to make the market.  If the current
minimum ask is *larger* than the moving average (m_ask), then you can guess
that it's going to be a seller's market.  So, (short) sell 200 shares using a
limit order that is set to just below the current minimum ask.  Wait a bit
and check up on your order.  If it can't complete after a certain time, then
cancel the order and see how many shares you've shorted.  Turn around and
try to buy that many shares using a limit order that starts well below what
you sold it at.  If you can't buy, then add a bit to your limit and try
again until all shorts are covered.  Note: you aren't guaranteed to make
money, but you will most of the time.  This keeps you trading and not
waiting.  If the current maximum bid is *smaller* than the moving average
(m_bid), then you can guess that it's going to be a buyer's market.  So,
buy 200 shares using a limit order that is set to just above the maximum
ask....(follow a similar approach as with the sell-first)
