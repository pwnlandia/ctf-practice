import requests
import json
import time
import sys

apikey = 'c17625c41e2d921ee937b54fb16f467145eabf2b'
stock = sys.argv[1]
print("stock is:", stock)
venue = sys.argv[2]
print("venue is:", venue)
account = sys.argv[3]
print("account is:", account)
base_url = 'https://api.stockfighter.io/ob/api'
book_url = base_url + '/venues/' + venue + '/stocks/' + stock
quote_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/quote'
orders_url = base_url + '/venues/' + venue + '/stocks/' + stock + '/orders'
authheader = {'X-Starfighter-Authorization' : apikey}

def get_orderbook():
	ask,bid = 0,0
	while (ask==0) or (bid==0):
		r = requests.get(book_url, headers=authheader)
		json_data = r.json()
		time.sleep(1)
		if json_data['bids']:
			foo = [f['price'] for f in json_data['bids']]
			bid=max(foo)
		if json_data['asks']:
			foo = [f['price'] for f in json_data['asks']]
			ask=min(foo)
	return(ask,bid)

def doLimitOrder(amount,direction,price,iterations):
	myorder['qty'] = amount
	myorder['direction'] = direction
	myorder['price'] = price
	myorder['orderType'] = 'limit'
	r = requests.post(orders_url, json=myorder, headers=authheader)
	orderid = r.json()['id']
	print("\t{} at {} order: {}".format(direction, price, orderid))
	orderid_url = orders_url+'/'+str(orderid)
	isopen=True
	iters = 0
	while (isopen==True):
		r = requests.get(orderid_url, headers=authheader)
		r_data = r.json()
		isopen = r_data['open']
		iters = iters + 1
		if iters > iterations:
			r = requests.post(orderid_url+"/cancel",headers=authheader)
			r_data = r.json()
			print("\tCancelling totalFilled is:", r_data['totalFilled'])
			return r_data['totalFilled']
		time.sleep(3)
	return r_data['totalFilled']

# Set up the order
myorder = {
  'account' : account,
  'venue' : venue,
  'symbol' : stock,
  'price' : 0,  #$44.00
  'qty' : 100,
  'direction' : '',
  'orderType' : 'limit'  # See the order docs for what a limit order is
}

def get_ask_bid(m_ask,m_bid):
	ask,bid = get_orderbook()
	m_ask=0.4*ask+0.6*m_ask
	m_bid=0.4*bid+0.6*m_bid
	return m_ask,m_bid,ask,bid

ask,bid = get_orderbook()
m_ask,m_bid,ask,bid=get_ask_bid(1.0*ask,1.0*bid)
for i in range(5):
	m_ask,m_bid,ask,bid = get_ask_bid(m_ask,m_bid)
	print("m_ask("+str(m_ask)+") ask("+str(ask)+")   "+"m_bid("+str(m_bid)+") bid("+str(bid)+")")

i=0
while True:
	m_ask,m_bid,ask,bid = get_ask_bid(m_ask,m_bid)
	print("New round: "+str(i))
	i += 1

	print(" Seller (ask-m_ask)={seller} m_ask({m_ask}) ask({ask})".format(**{
		'seller': ask-m_ask,
		'm_ask': m_ask,
		'ask': ask}))
	print(" Buyer (m_bid-bid)={buyer} m_bid({m_bid}) bid({bid})".format(**{\
		'buyer': m_bid-bid,
		'm_bid': m_bid,
		'bid': bid}))

	if (ask > m_ask):
		multiplier = ((ask-m_ask)*100 // m_ask) + 4
		print("Sell {}00 at {} then buy lower".format(int(multiplier),ask-1))
		total = doLimitOrder(100*int(multiplier),'sell',ask-1,3)
		while total > 0:
			m_ask,m_bid,ask,bid = get_ask_bid(m_ask,m_bid)
			bought = doLimitOrder(total,'buy',int(m_bid),2)
			total -= bought
	elif (m_bid > bid):
		multiplier = ((m_bid-bid)*100 / m_bid) + 4
		print("Sell {}00 at {} then buy lower".format(int(multiplier),bid+1))
		total = doLimitOrder(100*int(multiplier),'buy',bid+1,3)
		while total > 0:
			m_ask,m_bid,ask,bid = get_ask_bid(m_ask,m_bid)
			sold = doLimitOrder(total,'sell',int(m_ask),2)
			total -= sold
