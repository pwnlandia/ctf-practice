#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <CL/cl.h>

uint32_t table[256] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba,
	0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
	0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
	0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
	0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,
	0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
	0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
	0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940,
	0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116,
	0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
	0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
	0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,

	0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a,
	0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818,
	0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
	0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
	0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c,
	0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
	0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
	0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
	0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
	0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086,
	0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4,
	0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,

	0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
	0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
	0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
	0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe,
	0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
	0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
	0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
	0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252,
	0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60,
	0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
	0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
	0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
	0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04,
	0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,

	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a,
	0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
	0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
	0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
	0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e,
	0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
	0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
	0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
	0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
	0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0,
	0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6,
	0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
	0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d,
};

void print_size(size_t bytes) {
	const char* prefixes = "KMGTY";

	float tmp = bytes;
	int levels = 0;
	while(tmp > 1024) {
		tmp /= 1024;
		levels++;
	}

	if(levels == 0)
		printf("%lu B", bytes);
	else
		printf("%0.2f %ciB", tmp, prefixes[levels-1]);
}

void check_error(int err, const char* msg) {
	if(err != CL_SUCCESS) {
		fprintf(stderr, "Error: %s (%d)\n", msg, err);
		exit(1);
	}
}

void show_device_info(cl_device_id dev) {
	cl_uint temp_ui;
	char namebuf[128];

	clGetDeviceInfo(dev, CL_DEVICE_NAME, 128, namebuf, NULL);
	printf("\tName: %s\n", namebuf);
	clGetDeviceInfo(dev, CL_DEVICE_VENDOR, 128, namebuf, NULL);
	printf("\tVendor: %s\n", namebuf);

	printf("\tGlobal memory cache: ");
	clGetDeviceInfo(dev, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(temp_ui), &temp_ui, NULL);
	print_size(temp_ui);
	printf("\n\tGlobal memory: ");
	clGetDeviceInfo(dev, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(temp_ui), &temp_ui, NULL);
	print_size(temp_ui);
	printf("\n\tLocal memory: ");
	clGetDeviceInfo(dev, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(temp_ui), &temp_ui, NULL);
	print_size(temp_ui);

	clGetDeviceInfo(dev, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(temp_ui), &temp_ui, NULL);
	printf("\n\tCompute units: %d\n", temp_ui);
	clGetDeviceInfo(dev, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(temp_ui), &temp_ui, NULL);
	printf("\tMaximum work group size: %d items\n", temp_ui);
}

char* load_file(const char* fname) {
	FILE *f = fopen(fname, "r");
	if(f == NULL) return NULL;
	fseek(f, 0, SEEK_END);
	size_t len = ftell(f);
	char* buf = (char*)calloc(1, len+1);
	fseek(f, 0, SEEK_SET);
	fread(buf, 1, len, f);
	return buf;
}

// This is actually just a basic crc32, but we use it to precompute the seed value
uint32_t precompute_seed(char* buffer, uint32_t len) {
	uint32_t seed = 0;
	for(;len != 0;len--,buffer++)
		seed = table[((*buffer) ^ seed) & 0xff] ^ (seed >> 8);
	return seed;
}

#define BATCH_SIZE 67108864
#define CHUNK_SIZE 1024
#define TARGET 0xe1ca95ee

uint64_t find_append(cl_context ctx, cl_command_queue queue, cl_kernel kernel, uint32_t seed) {
	uint64_t counter = 0;
	uint32_t target = TARGET;

	// Set fixed argument values
	clSetKernelArg(kernel, 0, 4, &seed);
	clSetKernelArg(kernel, 3, 4, &target);

	// Allocate buffer space
	int err;
	uint64_t res_mirror = 0;
	cl_mem res_buf = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, 8, &res_mirror, &err);
	check_error(err, "Cannot allocate result buffer");
	check_error(clSetKernelArg(kernel, 2, sizeof(cl_mem), &res_buf), "Cannot set kernel arg");

	// Run each batch sequentially
	size_t work_size = BATCH_SIZE, chunk_size = CHUNK_SIZE;
	cl_event kern_evt;

	// Initial batch
	clSetKernelArg(kernel, 1, 8, &counter);
	check_error(clEnqueueNDRangeKernel(queue,
				kernel,
				1, NULL,
				&work_size,
				&chunk_size,
				0, NULL, &kern_evt), "Cannot enqueue kernel");

	// Main loop
	int itvl = 0;
	for(;counter < 0xffffffffff;) {
		// Enqueue a pending read
		check_error(clEnqueueReadBuffer(queue,
					res_buf, CL_TRUE,
					0, 8, &res_mirror,
					1, &kern_evt, NULL), "Cannot queue read operation");

		// Check results
		if(res_mirror != 0) {
			// Check for nulls
			char *tmp = (char*)&res_mirror;
			if(strlen((char*)(&res_mirror)) >= 5) {
				putchar('\n');
				clReleaseMemObject(res_buf);

				// Byteswap the result before returning
				uint64_t res = 0;
				char* out = (char*)&res;
				out[0] = tmp[4];
				out[1] = tmp[3];
				out[2] = tmp[2];
				out[3] = tmp[1];
				out[4] = tmp[0];
				return res;
			}
		}
		printf("\rc=%010lx", counter);
		if(++itvl % 20 == 0) fflush(stdout);

		// Next round
		counter += BATCH_SIZE;
		clSetKernelArg(kernel, 1, 8, &counter);
		check_error(clEnqueueNDRangeKernel(queue,
					kernel,
					1, NULL,
					&work_size,
					&chunk_size,
					0, NULL, &kern_evt), "Cannot enqueue kernel");
	}
	putchar('\n');
	clReleaseMemObject(res_buf);
	return 0;
}

int main(int argc, char **argv) {
	if(argc != 2) {
		fprintf(stderr, "Usage: %s [prefix]\n", argv[0]);
		return 0;
	}

	int err; // Error code

	// Find platform
	cl_platform_id platform;
	cl_uint num_devs;
	err = clGetPlatformIDs(1, &platform, &num_devs);
	check_error(err, "Unable to find usable platform");

	// Create context and command queue
	cl_context_properties props[] = {CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 0};
	cl_context ctx = clCreateContextFromType(props, CL_DEVICE_TYPE_GPU, NULL, NULL, &err);
	check_error(err, "Cannot create context");
	cl_device_id dev_id;
	
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev_id, &num_devs);
	if(num_devs <= 0) {
		fprintf(stderr, "Error: Cannot find devices\n");
		return 1;
	}
	cl_command_queue queue = clCreateCommandQueue(ctx, dev_id, 0, &err);
	check_error(err, "Cannot create command queue");

	// Show info about the acquired device
	printf("Found compute device\n");
	show_device_info(dev_id);

	// Load the CRC program
	char* program_src = load_file("crc.cl");
	if(program_src == NULL) {
		fprintf(stderr, "Error: Cannot find crc.cl\n");
		return 1;
	}
	size_t src_len = strlen(program_src);
	cl_program prog = clCreateProgramWithSource(ctx, 1, (const char**)&program_src, &src_len, &err);
	check_error(err, "Cannot load program");
	err = clBuildProgram(prog, 1, &dev_id, NULL, NULL, NULL);
	if(err != CL_SUCCESS) {
		printf("Error: Program build failed\n");
		char buffer[16384];
		check_error(clGetProgramBuildInfo(prog,
					dev_id,
					CL_PROGRAM_BUILD_LOG,
					16384, buffer, NULL), "Cannot get build log");
		printf("Build log:\n%s\n", buffer);
		return 1;
	}

	cl_kernel kernel = clCreateKernel(prog, "crc32", &err);
	check_error(err, "Cannot find kernel");

	// Compute initial seed
	uint32_t seed = precompute_seed(argv[1], strlen(argv[1]));
	printf("Seed: %08x\n", seed);

	// Check all possibilities
	uint64_t res = find_append(ctx, queue, kernel, seed);
	if(res != 0) {
		printf("Suffix found: %010lx\n", res);
	} else {
		printf("No suffix found\n");
	}

	// Tear down CL stuff
	clReleaseKernel(kernel);
	clReleaseProgram(prog);
	clReleaseCommandQueue(queue);
	clReleaseContext(ctx);
	return 0;
}
