# UTCTF

## Do Not Stop

### Examining the different protocols
We're provided with a pcap file named `capture.pcap`. Opening this up we'll see some a bunch of TCP streams between different machines. Upon further examination of any particular stream, there isn't much to note.
![TCP traffic]("./imgs/tcp.png")


### Further examination of DNS
We do see that there are some DNS requests, along with some ARP requests. ARP has its own set of attacks like ARP spoofing on a LAN, but lets look at DNS first (the challenge is Do Not Stop after all). Good intuition may lead one to think about DNS tunneling. 

![DNS in request](imgs/base64.png)

Ok, base64 isn't usually in DNS requests. Iterating through the different requests we find a larger amount of base64 traffic.

![DNS ls -la](imgs/base64_2.png)

Let's decode this hex. Right click copy as Hex + ASCII dump and paste in your favorite editor. Tidy up the file and decode with a CLI tool or website.

Running `base64 -D base64.txt` yields:
```
total 2512
drwxr-xr-x    1 root     root          4096 Mar  6 04:44 .
drwxr-xr-x    1 root     root          4096 Mar  6 08:09 ..
-rw-r--r--    1 root     root         12288 Mar  6 04:42 .Makefile.swp
-rw-r--r--    1 root     root           104 Mar  5 23:50 Dockerfile
-rw-r--r--    1 root     root           119 Mar  5 23:50 Makefile
-rw-r--r--    1 root     root            28 Mar  5 23:50 flag.txt
-rwxr-xr-x    1 root     root       2533823 Mar  6 04:44 server
-rw-r--r--    1 root     root          1693 Mar  5 23:50 server.go
```

We see flag.txt and some go server stuff. Looking at the answer section we see that the server has responded with all of this text. Check out the Answer section in Wireshark. The query was `bHMgLWxhCg==` which is simply `ls -la` base64 encoded. 

![DNS answer](imgs/answer.png)

How do we get TXT records off a DNS server? The `dig` tool works great! RTFM:
```
SIMPLE USAGE
       A typical invocation of dig looks like:

            dig @server name type

       where:

       server
           is the name or IP address of the name server to query.
           This can be an IPv4 address in dotted-decimal notation 
           or an IPv6 address in colon-delimited notation. When 
           the supplied server argument is a hostname, dig resolves
           that name before querying that name server.

           If no server argument is provided, dig consults 
           /etc/resolv.conf; if an address is found there, it queries 
           the name server at that address. If either of the -4 or -6 
           options are in use, then only addresses for the corresponding 
           transport will be tried. If no usable addresses are found, 
           dig will send the query to the local host. The reply from 
           the name server that responds is displayed.

       name
           is the name of the resource record that is to be looked up.

       type
           indicates what type of query is required -- ANY, A, MX, 
           SIG, etc.  type can be any valid query type. If no type 
           argument is supplied, dig will performa lookup for an 
           A record.
```
So `35.188.185.68` is the target because it responds to DNS requests with base64 tunneled information. But it has moved! We can't seem to find out where that DNS server has relocated, but the machine with all of the questions `192.168.232.129` asked for the A record of dns.google.com. Effectively we're asking for the IP address associated with dns.google.com and we get the following response.

![Google DNS]("imgs/../imgs/dnsgoogle.png")

Querying with `dig` for the actual service yields:
```
;; ANSWER SECTION:
dns.google.com.         13      IN      A       8.8.8.8
dns.google.com.         13      IN      A       8.8.4.4
```

It's fair to say that this response is a lie, and now our DNS server of interest is `35.225.16.21`. Encode your desired command and send to the fake dns.google.com. Base64 decode the response and you may find flag.txt

`utflag{$al1y_s3L1S_sE4_dN$}`