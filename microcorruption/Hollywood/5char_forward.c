/* This is a brute-force solver to find 5 characters that will result in Hollywood's hash function to have
   r4 = feb1 and r6 = 9298.  It relies on the fact that after the first stage, r4 holds the first two bytes
   in reverse order while r6 holds the first two bytes.  The two outer for loops iterate over all combinations
   of the first two characters.  The inner for loop iterates over all combinations for the next three 
   characters (i.e. 0 to 0xffffff), while assuming a NULL as the last character.  The final answers are:
   B: one: 4949  two: 4949  r4: feb1  r6: 9298  x: 96deb5    (4949b5de96)
   B: one: 4959  two: 5949  r4: feb1  r6: 9298  x: a6cea5    (5949a5cea6)
   B: one: 4d49  two: 494d  r4: feb1  r6: 9298  x: 9adeb5
   B: one: 4d4d  two: 4d4d  r4: feb1  r6: 9298  x: 96dab1
   B: one: 4d59  two: 594d  r4: feb1  r6: 9298  x: aacea5
   B: one: 4d5d  two: 5d4d  r4: feb1  r6: 9298  x: a6caa1
   B: one: 5959  two: 5959  r4: feb1  r6: 9298  x: 96cea5
   B: one: 5d59  two: 595d  r4: feb1  r6: 9298  x: 9acea5
   B: one: 5d5d  two: 5d5d  r4: feb1  r6: 9298  x: 96caa1
   B: one: c9c9  two: c9c9  r4: feb1  r6: 9298  x: 965e35
   B: one: c9d9  two: d9c9  r4: feb1  r6: 9298  x: a64e25
   B: one: cdc9  two: c9cd  r4: feb1  r6: 9298  x: 9a5e35
   B: one: cdcd  two: cdcd  r4: feb1  r6: 9298  x: 965a31
   B: one: cdd9  two: d9cd  r4: feb1  r6: 9298  x: aa4e25
   B: one: cddd  two: ddcd  r4: feb1  r6: 9298  x: a64a21
   B: one: d9d9  two: d9d9  r4: feb1  r6: 9298  x: 964e25
   B: one: ddd9  two: d9dd  r4: feb1  r6: 9298  x: 9a4e25
   B: one: dddd  two: dddd  r4: feb1  r6: 9298  x: 964a21
   */ 
#include <stdio.h>
#include <string.h>
short r4 = 0;
short r6 = 0;

unloop(short c1, short c2) {
        short tmp;
        r4 += c2 << 8;
        r4 += c1;
        r4 = (r4 << 8) | ((r4 >> 8) & 0xff);
        r6 ^= (c2 << 8) + c1;
        tmp = r4;
        r4 = r6;
        r6 = tmp;

}

main(int argc, char** argv) {
        unsigned int x;
        unsigned int one, two;
        short i,j;

        unsigned char ci;
        unsigned char cj;

        short nibi;
        short nibj;

        for (i = 0; i < 256; i++) {
                ci = 0xff & i;
                for (j = 0; j < 256; j++) {
                        cj = 0xff & j;
                        one = (ci << 8) | cj;
                        two = (cj << 8) | ci;
                        for (x=0; x <= 0xffffff; x++ ) {
                          r4 = (short) one;
                          r6 = (short) two;
                          nibi = (short) (x & 0xff);
                          nibj = (short) ((x >> 8) & 0xff);
                          unloop((short)nibi,(short)nibj);
                          if ((r4 == -335) && ((r6 & 0xff00) == 0x9200)) {
                                printf("A: one: %4x  two: %4x  r4: %4x  r6: %4x  x: %x\n",(unsigned short) one, (unsigned short) two, (unsigned short) r4,(unsigned short) r6,x);
                          }
                          nibi = (short) ((x >> 16) & 0xff);
                          nibj = 0;
                          unloop((short)nibi,(short)nibj);
                          if ((r4 == -335) && ((r6 & 0xff00) == 0x9200)) {
                                printf("B: one: %4x  two: %4x  r4: %4x  r6: %4x  x: %x\n",(unsigned short) one, (unsigned short) two, (unsigned short) r4,(unsigned short) r6,x);
                          }
                        }
                }
        }

        return 0;
}