#ifndef _HASHFUNCTION_H
#define _HASHFUNCTION_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdlib.h>

#define R4_DESIRED -335
#define R6_DESIRED -28008

bool test(char * password, bool log);
bool testWithInitValues(short r4, short r6, char * password, bool log);
void loop(short * r4, short * r6, short c1, short c2);

#endif
