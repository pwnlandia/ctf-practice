#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hashfunction.h"

#define R4_DESIRED -335
#define R6_DESIRED -28008

/*
 * Implements the function reverse-engineered from the e-range loop.
 */

void loop(short * r4, short * r6, short c1, short c2)
{
    *r4 += c2 << 8;
    *r4 += c1;

    // swpb r4
    *r4 = (*r4 << 8) | ((*r4 >> 8) & 0xff);

    // XOR reversed characters into r6
    *r6 ^= (c2 << 8) + c1;

    int temp = *r4;
    *r4 = *r6;
    *r6 = temp;
}

/*
 * Takes a password, applies the loop function over its length, and returns a
 * boolean indicating whether or not the password is correct.
 */

bool test(char * password, bool log)
{
    return testWithInitValues(0, 0, password, log);
}

bool testWithInitValues(short r4, short r6, char * password, bool log)
{
    if(!password)
    {
        return false;
    }

    int length = strlen(password);
    if(length == 0) return false;

    for(int i = 0; i < length; i += 2)
    {
        loop(&r4, &r6, password[i], password[i + 1]);
       
       
        
        if(log)
        {
            printf("\tr4: %4x\tr6: %4x\n", r4, r6);
        }
    }

    return (r4 == R4_DESIRED && r6 == R6_DESIRED);
}
