#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define PASS_MAX 100
#define R4_DESIRED -335
#define R6_DESIRED -28008

void loop(short * r4, short * r6, short c1, short c2);
bool test(char * password);
bool crack(char * password, int changeIndex);

char * characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

int main(void)
{
    char password[PASS_MAX + 1];

    // Let's assume that the password is between 8 and 16 characters, as the
    // passwords for previous exercises ostensibly were. Let's also assume that
    // the password has an even length, although this may not be the case
    // (one byte in a two-byte block could just be null).
    for(int length = 8; length <= 16; length += 2)
    {
        // Initialize length characters
        password[length] = '\0';
        if(crack(password, 0))
        {
            printf("Password found: %s\n", password);
            return 0;
        }
    }
    
    printf("Password not found.\n");
    return 0;
}

/*
 * Implements the function reverse-engineered from the e-range loop.
 */

void loop(short * r4, short * r6, short c1, short c2)
{
    *r4 += c2 << 8;
    *r4 += c1;

    // swpb r4
    *r4 = (*r4 << 8) | (*r4 >> 8);

    // XOR reversed characters into r6
    *r6 ^= (c2 << 8) + c1;

    int temp = *r4;
    *r4 = *r6;
    *r6 = temp;
}

/*
 * Takes a password, applies the loop function over its length, and returns a
 * boolean indicating whether or not the password is correct.
 */

bool test(char * password)
{
    if(!password)
    {
        return false;
    }

    int length = strlen(password);
    if(length == 0) return false;

    short r4 = 0;
    short r6 = 0;

    for(int i = 0; i < length; i += 2)
    {
        loop(&r4, &r6, password[i], password[i + 1]);
    }


    return (r4 == R4_DESIRED && r6 == R6_DESIRED);
}

/*
 * Recursive function to crack password.
 *
 * Takes a pointer to the password and the index of the character to iterate.
 */

bool crack(char * password, int changeIndex)
{
    if(!password || changeIndex < 0 || changeIndex >= strlen(password))
    {
        return false;
    }

    // If this is the last character, iterate and test
    if(password[changeIndex + 1] == '\0')
    {
        for(int i = 0, j = strlen(characters); i < j; ++i)
        {
            password[changeIndex] = characters[i];
            if(test(password))
            {
                return true;
            }
        }

        // No match found
        return false;
    }

    // Otherwise, iterate and initiate recursive call
    for(int i = 0, j = strlen(characters); i < j; ++i)
    {
        password[changeIndex] = characters[i];
        if(crack(password, changeIndex + 1))
        {
            return true;
        }
    }

    // No match found
    return false;
}
