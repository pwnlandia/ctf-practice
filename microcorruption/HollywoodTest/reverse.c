#include <string.h>
#include <stdio.h>
#define R4_DESIRED -335
#define R6_DESIRED -28008
short r4 = R4_DESIRED;
short r6 = R6_DESIRED;

unloop(short c1, short c2) {
        short tmp;
        tmp = r6;
        r6 = r4;
        r4 = tmp;

        r6 ^= (c2 << 8) + c1;
        r4 = (r4 << 8) | ((r4 >> 8) & 0xff);
        r4 -= c1;
        r4 -= c2 << 8;

        printf("r4: %4x  r6: %4x\n",(unsigned short) r4,(unsigned short) r6);
}

main(int argc, char** argv) {
        int i;
        int j;
        int x;
        char input[80];
        char tmp[3];
        char *in;
        sscanf(argv[1],"%79s",input);

        printf("r4: %4x  r6: %4x\n",(unsigned short) r4,(unsigned short) r6);

        tmp[2]=0;
        for (x=0; x < strlen(input); x += 4) {
           strncpy(tmp,input+x,2);
           sscanf(tmp,"%x",&i);
           strncpy(tmp,input+x+2,2);
           sscanf(tmp,"%x",&j);
           unloop((short)i,(short)j);
        }
}