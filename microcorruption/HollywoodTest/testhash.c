#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "hashfunction.h"

int main(int argc, char ** argv)
{
    if(argc < 2)
    {
        printf("Usage: testhash password\n");
        return 1;
    }

    int r4 = 0;
    int r6 = 0;

    char * input = NULL;

    bool dynamic = false;

    if(argc == 4)
    {
        sscanf(argv[1], "%4x", &r4);
        sscanf(argv[2], "%4x", &r6);
        input = argv[3];
    }
    else if(argc == 3 && (strcmp(argv[1], "hex") == 0))
    { 
        dynamic = true;
        int length = strlen(argv[2]) / 2;
        input = malloc((length + 1) * sizeof(char));
        int a;
        for(int i = 0; i < length; ++i)
        {
            sscanf(argv[2] + (2 * i), "%2x", &a);
            input[i] = a;
        }
        input[length] = '\0';
    }
    else if(argc == 5 && (strcmp(argv[3], "hex") == 0))
    {
        sscanf(argv[1], "%4x", &r4);
        sscanf(argv[2], "%4x", &r6);

        dynamic = true;
        int length = strlen(argv[4]) / 2;
        input = malloc((length + 1) * sizeof(char));
        int a;
        for(int i = 0; i < length; ++i)
        {
            sscanf(argv[4] + (2 * i), "%2x", &a);
            input[i] = a;
        }
        input[length] = '\0';
    }
    else
    {
        input = argv[1];
    }



    printf("Testing input \"%s\":\n", input);
    testWithInitValues(r4, r6, input, true);

    if(dynamic)
    {
        free(input);
    }
}
