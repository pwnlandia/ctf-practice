import socket
HOST = 'shell2017.picoctf.com'
PORT = 22531
BUF_SIZE = 2048

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
instructions = s.recv(BUF_SIZE).decode('ascii')
print(instructions)
f = instructions.split("'")
print(f[1])
print(f[3])
print(f[5])

thestr = f[1]*int(f[3]) + f[5] + "\n"
print(thestr)

s.send(thestr.encode('ascii'))

instructions = s.recv(BUF_SIZE).decode('ascii')
print(instructions)
