#include <stdio.h>

#define MAX_STACK 1000000 // very silly

int main(void) {
  char input[MAX_STACK];
  int in_len = 0; 
  char temp = 0;
  while(temp != ' ') {
    temp = getc(stdin);
    if(temp == EOF) temp = -1;

    input[in_len++] = temp;
  }

  in_len--;
  
  int don_pedro = 0, don_john = 0;
  while(in_len > 0) {
    int c = input[--in_len] - 32;
    don_john = c;

    c = (c + don_pedro) % 96;

    don_pedro = don_john;
    c += 32;
    putc(c, stdout);
  }
  
  return 0;
}
