#!/usr/bin/python
#  Compile the reversed code in MuchAdoAboutHacking.c into a.out
import subprocess

def hash(x):
    return subprocess.run('./a.out', input=x + b' ', stdout=subprocess.PIPE).stdout

GOAL = bytearray(b"tu1|\h+&g\OP7@% :BH7M6m3g=")
CURRENT = bytearray(b"!"*len(GOAL))

idx = -1
result = hash(CURRENT)
while result != GOAL:
    result = hash(CURRENT)
    print("{} vs {}".format(GOAL.decode('utf8'), result.decode('utf8')))
    if result[-1-idx] == GOAL[-1-idx]:
        idx -= 1
        continue
    CURRENT[idx] = CURRENT[idx]+1
print(CURRENT)
