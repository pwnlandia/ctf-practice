from PIL import Image
img = Image.open("littleschoolbus.bmp")
inpix = img.load()
ostr=""
for j in range(img.size[0]):
    ostr+="{}".format(inpix[j,198][2]%2)
    ostr+="{}".format(inpix[j,198][1]%2)
    ostr+="{}".format(inpix[j,198][0]%2)
print(''.join(chr(int(ostr[i:i+8],2)) for i in range(0, len(ostr), 8)))
