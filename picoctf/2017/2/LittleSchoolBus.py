# coding: utf-8
import PIL
import PIL.Image
from functools import reduce
img = PIL.Image.open('littleschoolbus.bmp')

def lsbs(xs):
    return list(map(lambda x: x & 1, xs))

def group(n,xs):
    xs = xs.copy()
    while len(xs) > 0:
        r = []
        while len(xs) > 0 and len(r) < n:
            r.append(xs.pop(0))
        yield r
        
bottom_row = list(map(lambda x: img.getpixel((x, img.height-1)), range(img.width)))
while bottom_row[-1] == (255,255,255):
    bottom_row.pop(-1)
bdata = list(reduce(lambda l,r: l+r, bottom_row))
blsbs = lsbs(bdata)

def ungroup(xs):
    r = []
    for i in xs:
        r += i
    return r

def swizzle(xs):
    gs = group(3, xs)
    return ungroup(map(lambda x: x[::-1], gs))

print(bytes(map(eval, map(lambda x: '0b'+''.join(map(str, x)), (group(8, swizzle(blsbs)))))))
