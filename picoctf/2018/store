The goal of this problem is to overflow the cost of the imitation flags to
a large negative number. This way once it is subtracted from your cost it
will give you enough money to buy a real flag.

This is the important part of the source code to look at.

if(number_flags > 0){
    int total_cost = 0;
    total_cost = 1000*number_flags;
    printf("\nYour total cost is: %d\n", total_cost);
    if(total_cost <= account_balance){
        account_balance = account_balance - total_cost;
        printf("\nYour new balance: %d\n\n", account_balance);
    }
    else{
        printf("Not enough funds\n");
    }

The code only checks to see if your cost is less than your balance so if it
is negative it will always let you buy it. Additionally, since the cost and
number of imitation flags you're purchasing are multiplied you can overflow
this value.

The max value of a signed int in this case is 2^31 - 1 or roughly 2.15 billion.
We can exceed this number with any number of imitation flags that are at least
2.15 million. Buying this many imitation flags gives us this result.

Welcome to the Store App V1.0
World's Most Secure Purchasing App

[1] Check Account Balance
[2] Buy Stuff
[3] Exit

Enter a menu selection
2
Current Auctions
[1] I Can't Believe its not a Flag!
[2] Real Flag
1
Imitation Flags cost 1000 each, how many would you like?
2150000

Your total cost is: -2144967296
Your new balance: 2144968396

Now we can use our large balance to buy a real flag and get the flag to the problem.