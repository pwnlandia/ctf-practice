from PIL import Image
import binascii
myimg = Image.open('steg.png','r')
pix = myimg.load()

n = ""
for i in xrange(800):
	n = n + ''.join([str(p % 2) for p in pix[i,0]])

chars = []
for b in range(300):
	byte = n[b*8:(b+1)*8]
	chars.append(chr(int(byte,2)))

print ''.join(chars)	
