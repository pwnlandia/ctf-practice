#!/usr/bin/python
pixels=bytearray(open("pixel.png","rb").read())
masked=bytearray(open("masked_key.png","rb").read())

xor = [ pixels[i]^masked[i] for i in range(len(pixels)) ]
print "XOR of pixel.png and masked gives you: " + bytearray(xor)

key = masked[-64:]

for i in range(len(masked)-64):
	masked[i] ^= key[i%64]

image_out = open("unmasked.png","w")
image_out.write(masked)
image_out.close()
