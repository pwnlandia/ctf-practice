import requests
s = requests.Session()

# The security level is specified as a cookie.  In order to set it up
#   front, you will need to create a CookieJar.
# Set a cookie in the CookieJar to indicate the security level of the
#   exercise you are trying to solve. (low, medium, or high)
# Then bind it to a domain (the IP address of your DVWA instance, and
#   a path in that domain.
# Finally, set the session's cookie
jar = requests.cookies.RequestsCookieJar()
jar.set('security','medium',domain='35.185.236.115', path='/')
s.cookies=jar

# You can then login.  Note that the login button has a third hidden
#   field called Login that must be sent
url = 'http://35.185.236.115/DVWA/login.php'
formdata = {'username':'admin','password':'password','Login':'Login'}
r = s.post(url,data=formdata)

# You are now logged in.
