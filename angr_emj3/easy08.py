
valid_inputs = range(0x40, 0x5b)
indices = range(16)


def complex_function(c, i):
    ecx = c - 0x41 + i * 0x35
    prod = ecx * 0x4ec4ec4f
    edx_1 = prod >> 32
    return chr(ecx - ((edx_1 >> 3) - (ecx >> 0x1f)) * 0x1a + 0x41)

mapping = {}
for i in indices:
    for v in valid_inputs:
        x = complex_function(v, 0xf - i)
        mapping[(x, i)] = chr(v)

desired = "EUWPHYMHLVRPOUGY"

res = []
for i, c in enumerate(desired):
    res.append(mapping[(c, i)])

print(''.join(res))