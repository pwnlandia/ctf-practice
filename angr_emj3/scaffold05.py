import angr
import claripy
import sys
import time

def main(argv):
  path_to_binary = argv[1]
  project = angr.Project(path_to_binary)

  start_address = 0x08048601
  initial_state = project.factory.blank_state(addr=start_address)

  # The binary is calling scanf("%8s %8s %8s %8s").
  # (!)
  passwords = [claripy.BVS("password{}".format(i), 64) for i in range(4)]

  # Determine the address of the global variable to which scanf writes the user
  # input. The function 'initial_state.memory.store(address, value)' will write
  # 'value' (a bitvector) to 'address' (a memory location, as an integer.) The
  # 'address' parameter can also be a bitvector (and can be symbolic!).
  # (!)
  pw_base = 0xa948a60
  password_addresses = [pw_base + 0x8*i for i in range(4)]
  for (addr, pw) in zip(password_addresses, passwords):
    initial_state.memory.store(addr, pw)

  simulation = project.factory.simgr(initial_state)

  def is_successful(state):
    stdout_output = state.posix.dumps(sys.stdout.fileno())
    return b"Good Job." in stdout_output

  def should_abort(state):
    stdout_output = state.posix.dumps(sys.stdout.fileno())
    return b"Try again." in stdout_output

  simulation.explore(find=is_successful, avoid=should_abort)

  if simulation.found:
    solution_state = simulation.found[0]

    # Solve for the symbolic values. We are trying to solve for a string.
    # Therefore, we will use eval, with named parameter cast_to=bytes
    # which returns a string instead of an integer.
    # (!)
    solutions = [str(solution_state.solver.eval(pw,cast_to=bytes), 'utf8') for pw in passwords]

    print(' '.join(solutions))
  else:
    raise Exception('Could not find the solution')

if __name__ == '__main__':
  start = time.perf_counter()
  main(sys.argv)
  stop = time.perf_counter()
  print(start, stop, stop - start)
