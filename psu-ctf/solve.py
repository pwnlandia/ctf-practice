#!/usr/bin/pypy

import angr
import sys

def main():
  # Open the binary passed as a command line argument.
  proj = angr.Project(sys.argv[1])

  # Instantiate the entry state of the binary to begin execution.
  initial_state = proj.factory.entry_state()

  # Create a path group (a set of possible paths of execution.)
  pg = proj.factory.path_group(initial_state)

  # Run through the binary, branching at all possible branches, until
  # we find a path that prints 'Good Job.' to stdout.
  pg.explore(find=lambda p: 'Good Job.' in p.state.posix.dumps(1))

  # Print information about the paths and the solution.
  print pg
  print repr(pg.found[0].state.posix.dumps(0))

if __name__ == '__main__':
  main()
