#!/usr/bin/python

# To run:
# python solve_all.py /directory/to/ctf/files/*

from functools import wraps
import errno
import os
import signal
import sys
import angr

class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator

@timeout(60)
def solve(pg, program):
  pg.explore(find=lambda x: "Good Job." in x.state.posix.dumps(1))
  if len(pg.found) > 0:
    print ('Password for %s is %s.' % (program, pg.found[0].state.posix.dumps(0)))
  else:
    print ('Could not find password for %s.' % (program,))

if __name__ == '__main__':
  for program in sys.argv[1:]:
    try:
      p = angr.Project(program)
      pg = p.factory.path_group()
      try:
        solve(pg, program)
      except:
        print ('Took too long to find password for %s.' % (program,))
    except:
      print ('An error occured when attempting to run %s.' % (program,))
