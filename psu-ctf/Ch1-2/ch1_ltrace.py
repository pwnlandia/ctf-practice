from manticore import Manticore

mc = Manticore("Ch1_Ltrace")

@mc.hook(0x40072c)
def scanf_hook(state):
    print("Hooking start")
    buf = state.new_symbolic_buffer(0x20)
    state.cpu.write_bytes(state.cpu.RBP-0x40, buf)
    state.cpu.EIP = 0x400731

@mc.hook(0x400748)
def fail_hook(state):
    print("Abandoning path")
    state.abandon()

@mc.hook(0x400754)
def ok_hook(state):
    print("Solving")
    buf = state.solve_buffer(state.cpu.RBP-0x40, 0x20)
    soln = buf[:buf.find(0)]
    print(''.join(map(chr, soln)))
    mc.terminate()

mc.should_profile = False
mc.run(procs=8)
