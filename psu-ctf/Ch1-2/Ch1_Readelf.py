#!/usr/bin/python2.7

from manticore import Manticore

m = Manticore("Ch1_Readelf")

@m.hook(0x4006e9)
def skip_scanf(state):
    rbp = state.cpu.read_register("RBP")

    sym = state.new_symbolic_buffer(9, cstring=True)
    state.cpu.write_bytes(rbp-0x20, sym)

    state.cpu.write_register("EIP", 0x4006ee)

# If we get to this instruction, we've won.
@m.hook(0x40070f)
def win(state):
    rbp = state.cpu.read_register("RBP")
    sym = state.cpu.read_bytes(rbp-0x20, 9)

    buf = ''.join(chr(state.solve_one(x)) for x in sym)
    print("solution: " + buf)
    m.terminate()

# This instruction means we've failed, so abandon the state if we reach it.
@m.hook(0x400703)
def fail(state):
    state.abandon()

m.run(procs=10)
