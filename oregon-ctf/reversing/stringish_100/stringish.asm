
stringish:     file format elf32-i386
stringish
architecture: i386, flags 0x00000112:
EXEC_P, HAS_SYMS, D_PAGED
start address 0x080484b0

Program Header:
    PHDR off    0x00000034 vaddr 0x08048034 paddr 0x08048034 align 2**2
         filesz 0x00000120 memsz 0x00000120 flags r-x
  INTERP off    0x00000154 vaddr 0x08048154 paddr 0x08048154 align 2**0
         filesz 0x00000013 memsz 0x00000013 flags r--
    LOAD off    0x00000000 vaddr 0x08048000 paddr 0x08048000 align 2**12
         filesz 0x000009d0 memsz 0x000009d0 flags r-x
    LOAD off    0x00000f08 vaddr 0x08049f08 paddr 0x08049f08 align 2**12
         filesz 0x00000130 memsz 0x00000160 flags rw-
 DYNAMIC off    0x00000f14 vaddr 0x08049f14 paddr 0x08049f14 align 2**2
         filesz 0x000000e8 memsz 0x000000e8 flags rw-
    NOTE off    0x00000168 vaddr 0x08048168 paddr 0x08048168 align 2**2
         filesz 0x00000044 memsz 0x00000044 flags r--
EH_FRAME off    0x00000894 vaddr 0x08048894 paddr 0x08048894 align 2**2
         filesz 0x0000003c memsz 0x0000003c flags r--
   STACK off    0x00000000 vaddr 0x00000000 paddr 0x00000000 align 2**4
         filesz 0x00000000 memsz 0x00000000 flags rw-
   RELRO off    0x00000f08 vaddr 0x08049f08 paddr 0x08049f08 align 2**0
         filesz 0x000000f8 memsz 0x000000f8 flags r--

Dynamic Section:
  NEEDED               libc.so.6
  INIT                 0x080483e8
  FINI                 0x08048824
  INIT_ARRAY           0x08049f08
  INIT_ARRAYSZ         0x00000004
  FINI_ARRAY           0x08049f0c
  FINI_ARRAYSZ         0x00000004
  GNU_HASH             0x080481ac
  STRTAB               0x080482a8
  SYMTAB               0x080481d8
  STRSZ                0x00000094
  SYMENT               0x00000010
  DEBUG                0x00000000
  PLTGOT               0x0804a000
  PLTRELSZ             0x00000048
  PLTREL               0x00000011
  JMPREL               0x080483a0
  REL                  0x08048388
  RELSZ                0x00000018
  RELENT               0x00000008
  VERNEED              0x08048358
  VERNEEDNUM           0x00000001
  VERSYM               0x0804833c

Version References:
  required from libc.so.6:
    0x0d696914 0x00 03 GLIBC_2.4
    0x0d696910 0x00 02 GLIBC_2.0

Sections:
Idx Name          Size      VMA       LMA       File off  Algn
  0 .interp       00000013  08048154  08048154  00000154  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .note.ABI-tag 00000020  08048168  08048168  00000168  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  2 .note.gnu.build-id 00000024  08048188  08048188  00000188  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .gnu.hash     0000002c  080481ac  080481ac  000001ac  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  4 .dynsym       000000d0  080481d8  080481d8  000001d8  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  5 .dynstr       00000094  080482a8  080482a8  000002a8  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  6 .gnu.version  0000001a  0804833c  0804833c  0000033c  2**1
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  7 .gnu.version_r 00000030  08048358  08048358  00000358  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  8 .rel.dyn      00000018  08048388  08048388  00000388  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  9 .rel.plt      00000048  080483a0  080483a0  000003a0  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 10 .init         00000023  080483e8  080483e8  000003e8  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 11 .plt          000000a0  08048410  08048410  00000410  2**4
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 12 .text         00000372  080484b0  080484b0  000004b0  2**4
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 13 .fini         00000014  08048824  08048824  00000824  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 14 .rodata       0000005c  08048838  08048838  00000838  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 15 .eh_frame_hdr 0000003c  08048894  08048894  00000894  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 16 .eh_frame     00000100  080488d0  080488d0  000008d0  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 17 .init_array   00000004  08049f08  08049f08  00000f08  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 18 .fini_array   00000004  08049f0c  08049f0c  00000f0c  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 19 .jcr          00000004  08049f10  08049f10  00000f10  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 20 .dynamic      000000e8  08049f14  08049f14  00000f14  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 21 .got          00000004  08049ffc  08049ffc  00000ffc  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 22 .got.plt      00000030  0804a000  0804a000  00001000  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 23 .data         00000008  0804a030  0804a030  00001030  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 24 .bss          00000028  0804a040  0804a040  00001038  2**5
                  ALLOC
 25 .comment      00000024  00000000  00000000  00001038  2**0
                  CONTENTS, READONLY
SYMBOL TABLE:
no symbols



Disassembly of section .init:

080483e8 <.init>:
 80483e8:	53                   	push   %ebx
 80483e9:	83 ec 08             	sub    $0x8,%esp
 80483ec:	e8 ef 00 00 00       	call   80484e0 <fputs@plt+0x40>
 80483f1:	81 c3 0f 1c 00 00    	add    $0x1c0f,%ebx
 80483f7:	8b 83 fc ff ff ff    	mov    -0x4(%ebx),%eax
 80483fd:	85 c0                	test   %eax,%eax
 80483ff:	74 05                	je     8048406 <fgets@plt-0x1a>
 8048401:	e8 5a 00 00 00       	call   8048460 <__gmon_start__@plt>
 8048406:	83 c4 08             	add    $0x8,%esp
 8048409:	5b                   	pop    %ebx
 804840a:	c3                   	ret    

Disassembly of section .plt:

08048410 <fgets@plt-0x10>:
 8048410:	ff 35 04 a0 04 08    	pushl  0x804a004
 8048416:	ff 25 08 a0 04 08    	jmp    *0x804a008
 804841c:	00 00                	add    %al,(%eax)
	...

08048420 <fgets@plt>:
 8048420:	ff 25 0c a0 04 08    	jmp    *0x804a00c
 8048426:	68 00 00 00 00       	push   $0x0
 804842b:	e9 e0 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

08048430 <memcmp@plt>:
 8048430:	ff 25 10 a0 04 08    	jmp    *0x804a010
 8048436:	68 08 00 00 00       	push   $0x8
 804843b:	e9 d0 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

08048440 <__stack_chk_fail@plt>:
 8048440:	ff 25 14 a0 04 08    	jmp    *0x804a014
 8048446:	68 10 00 00 00       	push   $0x10
 804844b:	e9 c0 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

08048450 <fwrite@plt>:
 8048450:	ff 25 18 a0 04 08    	jmp    *0x804a018
 8048456:	68 18 00 00 00       	push   $0x18
 804845b:	e9 b0 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

08048460 <__gmon_start__@plt>:
 8048460:	ff 25 1c a0 04 08    	jmp    *0x804a01c
 8048466:	68 20 00 00 00       	push   $0x20
 804846b:	e9 a0 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

08048470 <strlen@plt>:
 8048470:	ff 25 20 a0 04 08    	jmp    *0x804a020
 8048476:	68 28 00 00 00       	push   $0x28
 804847b:	e9 90 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

08048480 <__libc_start_main@plt>:
 8048480:	ff 25 24 a0 04 08    	jmp    *0x804a024
 8048486:	68 30 00 00 00       	push   $0x30
 804848b:	e9 80 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

08048490 <fputc@plt>:
 8048490:	ff 25 28 a0 04 08    	jmp    *0x804a028
 8048496:	68 38 00 00 00       	push   $0x38
 804849b:	e9 70 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

080484a0 <fputs@plt>:
 80484a0:	ff 25 2c a0 04 08    	jmp    *0x804a02c
 80484a6:	68 40 00 00 00       	push   $0x40
 80484ab:	e9 60 ff ff ff       	jmp    8048410 <fgets@plt-0x10>

Disassembly of section .text:

080484b0 <.text>:
 80484b0:	31 ed                	xor    %ebp,%ebp
 80484b2:	5e                   	pop    %esi
 80484b3:	89 e1                	mov    %esp,%ecx
 80484b5:	83 e4 f0             	and    $0xfffffff0,%esp
 80484b8:	50                   	push   %eax
 80484b9:	54                   	push   %esp
 80484ba:	52                   	push   %edx
 80484bb:	68 20 88 04 08       	push   $0x8048820
 80484c0:	68 b0 87 04 08       	push   $0x80487b0
 80484c5:	51                   	push   %ecx
 80484c6:	56                   	push   %esi
 80484c7:	68 16 86 04 08       	push   $0x8048616
 80484cc:	e8 af ff ff ff       	call   8048480 <__libc_start_main@plt>
 80484d1:	f4                   	hlt    
 80484d2:	66 90                	xchg   %ax,%ax
 80484d4:	66 90                	xchg   %ax,%ax
 80484d6:	66 90                	xchg   %ax,%ax
 80484d8:	66 90                	xchg   %ax,%ax
 80484da:	66 90                	xchg   %ax,%ax
 80484dc:	66 90                	xchg   %ax,%ax
 80484de:	66 90                	xchg   %ax,%ax
 80484e0:	8b 1c 24             	mov    (%esp),%ebx
 80484e3:	c3                   	ret    
 80484e4:	66 90                	xchg   %ax,%ax
 80484e6:	66 90                	xchg   %ax,%ax
 80484e8:	66 90                	xchg   %ax,%ax
 80484ea:	66 90                	xchg   %ax,%ax
 80484ec:	66 90                	xchg   %ax,%ax
 80484ee:	66 90                	xchg   %ax,%ax
 80484f0:	b8 3b a0 04 08       	mov    $0x804a03b,%eax
 80484f5:	2d 38 a0 04 08       	sub    $0x804a038,%eax
 80484fa:	83 f8 06             	cmp    $0x6,%eax
 80484fd:	77 01                	ja     8048500 <fputs@plt+0x60>
 80484ff:	c3                   	ret    
 8048500:	b8 00 00 00 00       	mov    $0x0,%eax
 8048505:	85 c0                	test   %eax,%eax
 8048507:	74 f6                	je     80484ff <fputs@plt+0x5f>
 8048509:	55                   	push   %ebp
 804850a:	89 e5                	mov    %esp,%ebp
 804850c:	83 ec 18             	sub    $0x18,%esp
 804850f:	c7 04 24 38 a0 04 08 	movl   $0x804a038,(%esp)
 8048516:	ff d0                	call   *%eax
 8048518:	c9                   	leave  
 8048519:	c3                   	ret    
 804851a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
 8048520:	b8 38 a0 04 08       	mov    $0x804a038,%eax
 8048525:	2d 38 a0 04 08       	sub    $0x804a038,%eax
 804852a:	c1 f8 02             	sar    $0x2,%eax
 804852d:	89 c2                	mov    %eax,%edx
 804852f:	c1 ea 1f             	shr    $0x1f,%edx
 8048532:	01 d0                	add    %edx,%eax
 8048534:	d1 f8                	sar    %eax
 8048536:	75 01                	jne    8048539 <fputs@plt+0x99>
 8048538:	c3                   	ret    
 8048539:	ba 00 00 00 00       	mov    $0x0,%edx
 804853e:	85 d2                	test   %edx,%edx
 8048540:	74 f6                	je     8048538 <fputs@plt+0x98>
 8048542:	55                   	push   %ebp
 8048543:	89 e5                	mov    %esp,%ebp
 8048545:	83 ec 18             	sub    $0x18,%esp
 8048548:	89 44 24 04          	mov    %eax,0x4(%esp)
 804854c:	c7 04 24 38 a0 04 08 	movl   $0x804a038,(%esp)
 8048553:	ff d2                	call   *%edx
 8048555:	c9                   	leave  
 8048556:	c3                   	ret    
 8048557:	89 f6                	mov    %esi,%esi
 8048559:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
 8048560:	80 3d 64 a0 04 08 00 	cmpb   $0x0,0x804a064
 8048567:	75 13                	jne    804857c <fputs@plt+0xdc>
 8048569:	55                   	push   %ebp
 804856a:	89 e5                	mov    %esp,%ebp
 804856c:	83 ec 08             	sub    $0x8,%esp
 804856f:	e8 7c ff ff ff       	call   80484f0 <fputs@plt+0x50>
 8048574:	c6 05 64 a0 04 08 01 	movb   $0x1,0x804a064
 804857b:	c9                   	leave  
 804857c:	f3 c3                	repz ret 
 804857e:	66 90                	xchg   %ax,%ax
 8048580:	a1 10 9f 04 08       	mov    0x8049f10,%eax
 8048585:	85 c0                	test   %eax,%eax
 8048587:	74 1f                	je     80485a8 <fputs@plt+0x108>
 8048589:	b8 00 00 00 00       	mov    $0x0,%eax
 804858e:	85 c0                	test   %eax,%eax
 8048590:	74 16                	je     80485a8 <fputs@plt+0x108>
 8048592:	55                   	push   %ebp
 8048593:	89 e5                	mov    %esp,%ebp
 8048595:	83 ec 18             	sub    $0x18,%esp
 8048598:	c7 04 24 10 9f 04 08 	movl   $0x8049f10,(%esp)
 804859f:	ff d0                	call   *%eax
 80485a1:	c9                   	leave  
 80485a2:	e9 79 ff ff ff       	jmp    8048520 <fputs@plt+0x80>
 80485a7:	90                   	nop
 80485a8:	e9 73 ff ff ff       	jmp    8048520 <fputs@plt+0x80>
 80485ad:	55                   	push   %ebp
 80485ae:	89 e5                	mov    %esp,%ebp
 80485b0:	eb 13                	jmp    80485c5 <fputs@plt+0x125>
 80485b2:	8b 45 08             	mov    0x8(%ebp),%eax
 80485b5:	0f b6 10             	movzbl (%eax),%edx
 80485b8:	8b 45 0c             	mov    0xc(%ebp),%eax
 80485bb:	88 10                	mov    %dl,(%eax)
 80485bd:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
 80485c1:	83 45 08 02          	addl   $0x2,0x8(%ebp)
 80485c5:	8b 45 08             	mov    0x8(%ebp),%eax
 80485c8:	0f b6 00             	movzbl (%eax),%eax
 80485cb:	3c ff                	cmp    $0xff,%al
 80485cd:	75 e3                	jne    80485b2 <fputs@plt+0x112>
 80485cf:	8b 45 0c             	mov    0xc(%ebp),%eax
 80485d2:	c6 00 00             	movb   $0x0,(%eax)
 80485d5:	90                   	nop
 80485d6:	5d                   	pop    %ebp
 80485d7:	c3                   	ret    
 80485d8:	55                   	push   %ebp
 80485d9:	89 e5                	mov    %esp,%ebp
 80485db:	eb 1c                	jmp    80485f9 <fputs@plt+0x159>
 80485dd:	8b 45 08             	mov    0x8(%ebp),%eax
 80485e0:	0f b6 10             	movzbl (%eax),%edx
 80485e3:	8b 45 0c             	mov    0xc(%ebp),%eax
 80485e6:	88 10                	mov    %dl,(%eax)
 80485e8:	8b 45 0c             	mov    0xc(%ebp),%eax
 80485eb:	83 c0 01             	add    $0x1,%eax
 80485ee:	c6 00 ff             	movb   $0xff,(%eax)
 80485f1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 80485f5:	83 45 0c 02          	addl   $0x2,0xc(%ebp)
 80485f9:	8b 45 08             	mov    0x8(%ebp),%eax
 80485fc:	0f b6 00             	movzbl (%eax),%eax
 80485ff:	84 c0                	test   %al,%al
 8048601:	74 0a                	je     804860d <fputs@plt+0x16d>
 8048603:	8b 45 08             	mov    0x8(%ebp),%eax
 8048606:	0f b6 00             	movzbl (%eax),%eax
 8048609:	3c 0a                	cmp    $0xa,%al
 804860b:	75 d0                	jne    80485dd <fputs@plt+0x13d>
 804860d:	8b 45 0c             	mov    0xc(%ebp),%eax
 8048610:	c6 00 ff             	movb   $0xff,(%eax)
 8048613:	90                   	nop
 8048614:	5d                   	pop    %ebp
 8048615:	c3                   	ret    
 8048616:	55                   	push   %ebp
 8048617:	89 e5                	mov    %esp,%ebp
 8048619:	57                   	push   %edi
 804861a:	56                   	push   %esi
 804861b:	53                   	push   %ebx
 804861c:	83 e4 f0             	and    $0xfffffff0,%esp
 804861f:	81 ec 30 08 00 00    	sub    $0x830,%esp
 8048625:	8b 45 0c             	mov    0xc(%ebp),%eax
 8048628:	89 44 24 1c          	mov    %eax,0x1c(%esp)
 804862c:	65 a1 14 00 00 00    	mov    %gs:0x14,%eax
 8048632:	89 84 24 2c 08 00 00 	mov    %eax,0x82c(%esp)
 8048639:	31 c0                	xor    %eax,%eax
 804863b:	8d 5c 24 2c          	lea    0x2c(%esp),%ebx
 804863f:	b8 00 00 00 00       	mov    $0x0,%eax
 8048644:	ba 00 01 00 00       	mov    $0x100,%edx
 8048649:	89 df                	mov    %ebx,%edi
 804864b:	89 d1                	mov    %edx,%ecx
 804864d:	f3 ab                	rep stos %eax,%es:(%edi)
 804864f:	8d 9c 24 2c 04 00 00 	lea    0x42c(%esp),%ebx
 8048656:	b8 00 00 00 00       	mov    $0x0,%eax
 804865b:	ba 00 01 00 00       	mov    $0x100,%edx
 8048660:	89 df                	mov    %ebx,%edi
 8048662:	89 d1                	mov    %edx,%ecx
 8048664:	f3 ab                	rep stos %eax,%es:(%edi)
 8048666:	c7 44 24 24 40 88 04 	movl   $0x8048840,0x24(%esp)
 804866d:	08 
 804866e:	c7 44 24 28 58 88 04 	movl   $0x8048858,0x28(%esp)
 8048675:	08 
 8048676:	a1 60 a0 04 08       	mov    0x804a060,%eax
 804867b:	89 44 24 0c          	mov    %eax,0xc(%esp)
 804867f:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
 8048686:	00 
 8048687:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 804868e:	00 
 804868f:	c7 04 24 7a 88 04 08 	movl   $0x804887a,(%esp)
 8048696:	e8 b5 fd ff ff       	call   8048450 <fwrite@plt>
 804869b:	a1 40 a0 04 08       	mov    0x804a040,%eax
 80486a0:	89 44 24 08          	mov    %eax,0x8(%esp)
 80486a4:	c7 44 24 04 00 01 00 	movl   $0x100,0x4(%esp)
 80486ab:	00 
 80486ac:	8d 44 24 2c          	lea    0x2c(%esp),%eax
 80486b0:	89 04 24             	mov    %eax,(%esp)
 80486b3:	e8 68 fd ff ff       	call   8048420 <fgets@plt>
 80486b8:	8d 84 24 2c 04 00 00 	lea    0x42c(%esp),%eax
 80486bf:	89 44 24 04          	mov    %eax,0x4(%esp)
 80486c3:	8d 44 24 2c          	lea    0x2c(%esp),%eax
 80486c7:	89 04 24             	mov    %eax,(%esp)
 80486ca:	e8 09 ff ff ff       	call   80485d8 <fputs@plt+0x138>
 80486cf:	8b 44 24 24          	mov    0x24(%esp),%eax
 80486d3:	89 04 24             	mov    %eax,(%esp)
 80486d6:	e8 95 fd ff ff       	call   8048470 <strlen@plt>
 80486db:	89 44 24 08          	mov    %eax,0x8(%esp)
 80486df:	8b 44 24 24          	mov    0x24(%esp),%eax
 80486e3:	89 44 24 04          	mov    %eax,0x4(%esp)
 80486e7:	8d 84 24 2c 04 00 00 	lea    0x42c(%esp),%eax
 80486ee:	89 04 24             	mov    %eax,(%esp)
 80486f1:	e8 3a fd ff ff       	call   8048430 <memcmp@plt>
 80486f6:	85 c0                	test   %eax,%eax
 80486f8:	75 6b                	jne    8048765 <fputs@plt+0x2c5>
 80486fa:	8d 84 24 2c 04 00 00 	lea    0x42c(%esp),%eax
 8048701:	89 44 24 04          	mov    %eax,0x4(%esp)
 8048705:	8b 44 24 28          	mov    0x28(%esp),%eax
 8048709:	89 04 24             	mov    %eax,(%esp)
 804870c:	e8 9c fe ff ff       	call   80485ad <fputs@plt+0x10d>
 8048711:	a1 60 a0 04 08       	mov    0x804a060,%eax
 8048716:	89 44 24 0c          	mov    %eax,0xc(%esp)
 804871a:	c7 44 24 08 06 00 00 	movl   $0x6,0x8(%esp)
 8048721:	00 
 8048722:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 8048729:	00 
 804872a:	c7 04 24 85 88 04 08 	movl   $0x8048885,(%esp)
 8048731:	e8 1a fd ff ff       	call   8048450 <fwrite@plt>
 8048736:	a1 60 a0 04 08       	mov    0x804a060,%eax
 804873b:	89 44 24 04          	mov    %eax,0x4(%esp)
 804873f:	8d 84 24 2c 04 00 00 	lea    0x42c(%esp),%eax
 8048746:	89 04 24             	mov    %eax,(%esp)
 8048749:	e8 52 fd ff ff       	call   80484a0 <fputs@plt>
 804874e:	a1 60 a0 04 08       	mov    0x804a060,%eax
 8048753:	89 44 24 04          	mov    %eax,0x4(%esp)
 8048757:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
 804875e:	e8 2d fd ff ff       	call   8048490 <fputc@plt>
 8048763:	eb 25                	jmp    804878a <fputs@plt+0x2ea>
 8048765:	a1 60 a0 04 08       	mov    0x804a060,%eax
 804876a:	89 44 24 0c          	mov    %eax,0xc(%esp)
 804876e:	c7 44 24 08 07 00 00 	movl   $0x7,0x8(%esp)
 8048775:	00 
 8048776:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
 804877d:	00 
 804877e:	c7 04 24 8c 88 04 08 	movl   $0x804888c,(%esp)
 8048785:	e8 c6 fc ff ff       	call   8048450 <fwrite@plt>
 804878a:	b8 00 00 00 00       	mov    $0x0,%eax
 804878f:	8b b4 24 2c 08 00 00 	mov    0x82c(%esp),%esi
 8048796:	65 33 35 14 00 00 00 	xor    %gs:0x14,%esi
 804879d:	74 05                	je     80487a4 <fputs@plt+0x304>
 804879f:	e8 9c fc ff ff       	call   8048440 <__stack_chk_fail@plt>
 80487a4:	8d 65 f4             	lea    -0xc(%ebp),%esp
 80487a7:	5b                   	pop    %ebx
 80487a8:	5e                   	pop    %esi
 80487a9:	5f                   	pop    %edi
 80487aa:	5d                   	pop    %ebp
 80487ab:	c3                   	ret    
 80487ac:	66 90                	xchg   %ax,%ax
 80487ae:	66 90                	xchg   %ax,%ax
 80487b0:	55                   	push   %ebp
 80487b1:	57                   	push   %edi
 80487b2:	31 ff                	xor    %edi,%edi
 80487b4:	56                   	push   %esi
 80487b5:	53                   	push   %ebx
 80487b6:	e8 25 fd ff ff       	call   80484e0 <fputs@plt+0x40>
 80487bb:	81 c3 45 18 00 00    	add    $0x1845,%ebx
 80487c1:	83 ec 1c             	sub    $0x1c,%esp
 80487c4:	8b 6c 24 30          	mov    0x30(%esp),%ebp
 80487c8:	8d b3 0c ff ff ff    	lea    -0xf4(%ebx),%esi
 80487ce:	e8 15 fc ff ff       	call   80483e8 <fgets@plt-0x38>
 80487d3:	8d 83 08 ff ff ff    	lea    -0xf8(%ebx),%eax
 80487d9:	29 c6                	sub    %eax,%esi
 80487db:	c1 fe 02             	sar    $0x2,%esi
 80487de:	85 f6                	test   %esi,%esi
 80487e0:	74 27                	je     8048809 <fputs@plt+0x369>
 80487e2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
 80487e8:	8b 44 24 38          	mov    0x38(%esp),%eax
 80487ec:	89 2c 24             	mov    %ebp,(%esp)
 80487ef:	89 44 24 08          	mov    %eax,0x8(%esp)
 80487f3:	8b 44 24 34          	mov    0x34(%esp),%eax
 80487f7:	89 44 24 04          	mov    %eax,0x4(%esp)
 80487fb:	ff 94 bb 08 ff ff ff 	call   *-0xf8(%ebx,%edi,4)
 8048802:	83 c7 01             	add    $0x1,%edi
 8048805:	39 f7                	cmp    %esi,%edi
 8048807:	75 df                	jne    80487e8 <fputs@plt+0x348>
 8048809:	83 c4 1c             	add    $0x1c,%esp
 804880c:	5b                   	pop    %ebx
 804880d:	5e                   	pop    %esi
 804880e:	5f                   	pop    %edi
 804880f:	5d                   	pop    %ebp
 8048810:	c3                   	ret    
 8048811:	eb 0d                	jmp    8048820 <fputs@plt+0x380>
 8048813:	90                   	nop
 8048814:	90                   	nop
 8048815:	90                   	nop
 8048816:	90                   	nop
 8048817:	90                   	nop
 8048818:	90                   	nop
 8048819:	90                   	nop
 804881a:	90                   	nop
 804881b:	90                   	nop
 804881c:	90                   	nop
 804881d:	90                   	nop
 804881e:	90                   	nop
 804881f:	90                   	nop
 8048820:	f3 c3                	repz ret 

Disassembly of section .fini:

08048824 <.fini>:
 8048824:	53                   	push   %ebx
 8048825:	83 ec 08             	sub    $0x8,%esp
 8048828:	e8 b3 fc ff ff       	call   80484e0 <fputs@plt+0x40>
 804882d:	81 c3 d3 17 00 00    	add    $0x17d3,%ebx
 8048833:	83 c4 08             	add    $0x8,%esp
 8048836:	5b                   	pop    %ebx
 8048837:	c3                   	ret    
