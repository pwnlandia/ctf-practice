def name_hash(s):
    result = 0
    for c in s:
        oc = ord(c)
        result = ((result + oc) * oc) ^ oc
    return result
