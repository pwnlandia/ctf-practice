#include <stdint.h>

uint32_t name_hash(char* name) {
    uint32_t result = 0;
    for(;*name != 0;name++) {
        result += *name;
        result *= *name;
        result ^= *name;
    }
    return result;
}

uint32_t mutate_serial(uint32_t s) {
    s += 0x21524111;
    s ^= 0x77777777;
    return s;
}
// 011101110111...

int main(int argc, char **argv) {
    uint32_t nh = name_hash(argv[1]);
    nh ^= 0x77777777;
    nh -= 0x21524111;
    printf("%u\n", nh);
    return 0;
}
