BYTES = open('flag.omgzip','rb')
OUTPUT = open("flag", "wb")
BYTES.read(8)

# it's run-length encoded. this decoder isn't perfect, but the results are
# recognizable
while True:
    b = BYTES.read(1)
    if(len(b) == 0):
        break
    if b[0] == 0xff:
        # Special stuff
        rlc = BYTES.read(1)[0]
        val = BYTES.read(1)[0]
        if b[0] == 0xff:
            OUTPUT.write(bytes([val])*(rlc+2))
        else:
            OUTPUT.write(bytes([val])*(rlc+3))
        pass
    else:
        OUTPUT.write(b)
OUTPUT.close()
BYTES.close()
