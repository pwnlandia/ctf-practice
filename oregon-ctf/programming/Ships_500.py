import socket, re, copy, random

conn = socket.create_connection(('ctf.raytheon.si', 8888))
inbuf = b''

def readline():
    global inbuf
    while 0x0a not in inbuf:
        inbuf += conn.recv(8192)
    idx = inbuf.find(b'\x0a')
    lbuf = inbuf[:idx]
    inbuf = inbuf[idx+1:]
    res = lbuf.decode('utf8')
    print("--> %s" % res)
    return res

def sendline(s):
    print("<<SEND '%s'>>" % s)
    conn.send(s.encode('utf8')+b'\n')

def readinput():
    print("Reading Input")
    mapsize = int(re.match("Map Size: (\d+)", readline()).group(1))
    ships = []
    ship_pat = re.compile("([^:]*):\((-?\d+), (-?\d+)\):\((-?\d+), (-?\d+)\)")
    ss_pat = re.compile("Starting side: (east|west|north|south)")
    es_pat = re.compile("Ending side: (east|west|north|south)")
    ssdir = None
    esdir = None
    while ssdir == None or esdir == None:
        l = readline()
        m = ship_pat.match(l)
        if m != None:
            name = m.group(1)
            start = (int(m.group(2)), int(m.group(3)))
            hdg = (int(m.group(4)), int(m.group(5)))
            ships.append((name, start, hdg))
        elif ss_pat.match(l) != None:
            ssdir = ss_pat.match(l).group(1)
        elif es_pat.match(l) != None:
            esdir = es_pat.match(l).group(1)
        else:
            print("WARN: Cannot parse: %s" % l)
            raise RuntimeError()
    return (mapsize, ships, ssdir, esdir)

def tick_ships(bs, ships):
    res = []
    for n,p,v in ships:
        np = (p[0]+v[0], p[1]+v[1])
        if p[0] >= 0 and p[0] < bs and p[1] >= 0 and p[1] < bs:
            res.append((n, np, v))
    return res

def at_goal(brd, pos):
    goal = brd[3]
    if goal == 'east':
        return pos[0] == brd[0]-1
    elif goal == 'west':
        return pos[0] == 0
    elif goal == 'north':
        return pos[1] == brd[0]-1
    elif goal == 'south':
        return pos[1] == 0

def all_vels_for(brd):
    vels = []
    for xv in range(-3,4):
        for yv in range(-3,4):
            if xv == 0 and yv == 0:
                continue
            vels.append((xv,yv))
    return vels

def all_sps(brd):
    if brd[2] == 'east':
        return [(brd[0]-1, m) for m in range(brd[0])]
    elif brd[2] == 'west':
        return [(0, m) for m in range(brd[0])]
    elif brd[2] == 'south':
        return [(m, 0) for m in range(brd[0])]
    elif brd[2] == 'north':
        return [(m, brd[0]-1) for m in range(brd[0])]

def collides(brd,pos):
    for i in brd[1]:
        #print("(%s) %r vs %r" % (i[0], i[1], pos))
        if i[1] == pos:
            #print("**** COLLIDED WITH %s AT %r vs %r" % (i[0], i[1], pos))
            return True
    return False

def off_edge(pos, brd):
    if pos[0] < 0:
        return 'west'
    elif pos[0] >= brd[0]:
        return 'east'
    elif pos[1] >= brd[0]:
        return 'north'
    elif pos[1] < 0:
        return 'south'
    return None

def try_vecpair(iboard, pos, vel):
    brd = list(copy.deepcopy(iboard))
    while True:
        if at_goal(brd, pos):
            return True
        elif collides(brd, pos):
            return False
        oe = off_edge(pos, brd)
        if oe != None:
            return oe == brd[3]
        pos = (pos[0]+vel[0], pos[1]+vel[1])
        if collides(brd,pos):
            return False
        sh = tick_ships(brd[0], brd[1])
        brd[1] = sh

def fmt_vec(v):
    return "(%d, %d)" % v

def solve(challenge):
    avels = all_vels_for(challenge)
    SHIPNAME = "Gunboat Diplomat"
    for sp in all_sps(challenge):
        for v in avels:
            if try_vecpair(challenge,sp,v):
                print("(%r, %r) Wins!" % (sp,v))
                sendline('%s:%s:%s' % (SHIPNAME, fmt_vec(sp), fmt_vec(v)))
                return

# Get past header
readline()
readline()
readline()
sendline('1')

# Start trying things
while True:
    solve(readinput())
    readline()
    readline()
    readline()
