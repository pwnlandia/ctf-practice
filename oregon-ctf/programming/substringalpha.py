# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# This challenge gave a string and two indices i and j. The goal was to       #
# return the substring starting at index i and ending at j.                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import socket

HOST = '54.89.227.20'
PORT = 2211

BUF_SIZE = 2048


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

for i in range(101):
  instructions = s.recv(BUF_SIZE).decode('ascii')
  print(instructions)
  if 'Congrat' in instructions:
    break
  instructionArray = instructions.split()

  wordCount = int(instructionArray[3])
  if wordCount == 0:
    print("No number found")
    break

  if instructionArray[2] == 'first':
    result = ''
    for j in range(wordCount):
      result += instructionArray[j + 5] + ' '
    print('result: ' + result)
    response = bytearray(result, 'ascii')
    response[len(response) - 1] = ord('\n')
    s.send(response)
  elif instructionArray[2] == 'last':
    result = ''
    for j in range(wordCount):
      result = instructionArray[len(instructionArray) - 1 - j] + ' ' + result
    print('result: ' + result)
    response = bytearray(result, 'ascii')
    response[len(response) - 1] = ord('\n')
    s.send(response)
  else:
    print('Can\'t find first/last keyword')
    break

instructions = s.recv(BUF_SIZE).decode('ascii')
print(instructions)
print('Done')
s.close()
