import socket, re, copy

conn = socket.create_connection(('ctf.raytheon.si', 1970))
inbuf = b''

def readline():
    global inbuf
    while 0x0a not in inbuf:
        inbuf += conn.recv(8192)
    idx = inbuf.find(0x0a)
    lbuf = inbuf[:idx]
    inbuf = inbuf[idx+1:]
    res = lbuf.decode('utf8')
    print("--> %s" % res)
    return res

def parse_res():
    goal = re.match("#* Round (\d+): (\d+) Generations", readline())
    if goal == None:
        return None
    idx = int(goal.group(1))
    gens = int(goal.group(2))

    # Read the board
    readline()
    blines = []
    while True:
        l = readline().strip('#')
        if len(l) == 0:
            break
        blines.append(list(map(lambda x: (1 if x == '*' else 0), l)))
    return (idx,gens,blines)

def get_at(state, x, y):
    if x < 0 or y < 0:
        return 0
    if y >= len(state) or x >= len(state[0]):
        return 0
    return state[y][x]

def golstep(state):
    scopy = copy.deepcopy(state)
    for y in range(len(state)):
        for x in range(len(state[0])):
            neighbors = sum(map(lambda k: get_at(state, k[0]+x, k[1]+y), [
                (-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]))
            if neighbors < 2:
                scopy[y][x] = 0
            elif (neighbors == 2 or neighbors == 3) and (state[y][x] == 1):
                scopy[y][x] = 1
            elif neighbors > 3 and state[y][x] == 1:
                scopy[y][x] = 0
            elif neighbors == 3 and state[y][x] == 0:
                scopy[y][x] = 1
    return scopy

def steps(start,n):
    for i in range(n):
        start = golstep(start)
    return start

def showboard(b):
    def showline(l):
        return "#%s#" % ''.join(map(lambda x: '*' if x == 1 else ' ', l))
    w = len(b[0])+2
    footer = "#"*w
    brds = "\n".join(map(showline, b))
    return footer + '\n' + brds + '\n' + footer + '\n'

# Read header
while len(readline()) > 4:
    pass

# Run
while True:
    t = parse_res()
    if t == None:
        break
    i,g,b = t
    r = showboard(steps(b,g))
    #print(r)
    conn.send(r.encode('utf8'))
    readline()
while True:
    readline()
