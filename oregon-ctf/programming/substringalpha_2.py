import socket, re, copy, random

conn = socket.create_connection(('54.89.227.20', 2211))
inbuf = b''

def readline():
    global inbuf
    while 0x0a not in inbuf:
        inbuf += conn.recv(8192)
    idx = inbuf.find(b'\x0a')
    lbuf = inbuf[:idx]
    inbuf = inbuf[idx+1:]
    res = lbuf.decode('utf8')
    print("--> %s" % res)
    return res

def sendline(s):
    print("<<SEND '%s'>>" % s)
    conn.send(s.encode('utf8')+b'\n')

fn = re.compile("Return the (first|last) (\d+) words: (.+)")

while True:
    l = readline()
    m = fn.match(l)
    n = int(m.group(2))
    if m.group(1) == 'last':
        sendline(' '.join(m.group(3).split(' ')[-n:]))
    elif m.group(1) == 'first':
        sendline(' '.join(m.group(3).split(' ')[:n]))
