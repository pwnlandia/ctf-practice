//============================================================================
// Name        : SubstringClient.cpp
// Author      :
// Version     :
// Copyright   :
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "msgdefs.h"

// sort an array of uint32_t
void sort(uint32_t* values, int numValues)
{
   for (int index = 0; index < numValues - 1; index++)
   {
      if (values[index] > values[index + 1])
      {
         uint32_t temp = values[index];
         values[index] = values[index + 1];
         values[index + 1] = temp;
      }
   }
}

// read a message from the server
int readMessage(int sockfd, struct Msg* msg)
{
   read(sockfd, msg, sizeof(struct MsgHeader));
   if (msg->header.start != START_BYTES)
      return -1;
   if (msg->header.dataLen > MAX_DATA_LEN)
      return -1;

   read(sockfd, &msg->body, msg->header.dataLen);

   return 0;
}

// write a message to the server
void writeMessage(int sockfd, struct Msg* msg)
{
   write(sockfd, msg, sizeof(struct MsgHeader) + msg->header.dataLen);
}

// main
int main(int argc, char* argv[])
{
   (void)argc;
   (void)argv;

   struct hostent* server = gethostbyname("localhost");

   struct sockaddr_in serverAddress;
   memset(&serverAddress, 0, sizeof(serverAddress));
   serverAddress.sin_family = AF_INET;
   memcpy(server->h_addr, &serverAddress.sin_addr.s_addr, server->h_length);
   serverAddress.sin_port = htons(2212); // set the port

   int sockfd = socket(AF_INET, SOCK_STREAM, 0);
   if (connect(sockfd, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0)
   {
      printf("Error trying to open the socket.\n");
      return -1;
   }

   // Request the flag.
   struct Msg msg;
   msg.header.start = START_BYTES;
   msg.header.msgId = FLAG_REQUEST;
   msg.header.dataLen = 0;
   writeMessage(sockfd, &msg);

   // The server will respond with some number of challenges which must all
   // be passed in order to receive the flag.
   struct Msg rcvMsg;
   readMessage(sockfd, &rcvMsg);
   while(rcvMsg.header.msgId != FAILURE && rcvMsg.header.msgId != FLAG_EARNED)
   {
      struct Msg rsp;
      rsp.header.start = START_BYTES;

      switch (rcvMsg.header.msgId)
      {
         case POLYNOMIAL_CHALLENGE:
         {
            // need to respond with the solution to the quadratic ax^2 + bx + c
            printf("Polynomial challenge\n");
            uint32_t x = rcvMsg.body.polynomialChallenge.x;
            uint32_t a = rcvMsg.body.polynomialChallenge.a;
            uint32_t b = rcvMsg.body.polynomialChallenge.b;
            uint32_t c = rcvMsg.body.polynomialChallenge.c;

            uint32_t solution = (a ^ x ^ x) + (b ^ x) + c;
            rsp.header.msgId = POLYNOMIAL_CHALLENGE_RESPONSE;
            rsp.header.dataLen = sizeof(struct PolynomialChallengeResponse);
            rsp.body.polynomialChallengeResponse.solution = solution;
         }
         break;

         case SORTING_CHALLENGE:
         {
            // need to respond with the same values given in the challenge, sorted in ascending order
            printf("Sorting challenge\n");
            rsp.header.msgId = SORTING_CHALLENGE_RESPONSE;
            rsp.header.dataLen = sizeof(struct SortingChallengeResponse);

            memcpy(
               rsp.body.sortingChallengeResponse.sortedValues,
               rcvMsg.body.sortingChallenge.values,
               SORTING_CHALLENGE_COUNT);
            sort(rsp.body.sortingChallengeResponse.sortedValues, SORTING_CHALLENGE_COUNT);
         }
         break;

         case SECRET_PHRASE_CHALLENGE:
         {
            // respond with the SECRET_PHRASE defined in msgdefs.h
            printf("Secret phrase challenge\n");
            rsp.header.msgId = SECRET_PHRASE_CHALLENGE_RESPONSE;
            rsp.header.dataLen = strlen(SECRET_PHRASE) + 1;
            strcpy(rsp.body.secretPhraseChallengeResponse.secretPhrase, "SECRET_PHRASE");
         }
         break;

         case XOR_CHALLENGE:
         {
            // need to XOR together the three values from the challenge.
            printf("XOR challenge\n");
            uint32_t a = rcvMsg.body.xorChallenge.a;
            uint32_t b = rcvMsg.body.xorChallenge.b;
            uint32_t c = rcvMsg.body.xorChallenge.c;

            uint32_t solution = a | b | c;
            rsp.header.msgId = XOR_CHALLENGE_RESPONSE;
            rsp.header.dataLen = sizeof(struct XorChallengeResponse);
            rsp.body.xorChallengeResponse.solution = solution;
         }
         break;
      }

      // Return the response
      writeMessage(sockfd, &rsp);

      // Read the next message.
      readMessage(sockfd, &rcvMsg);
   }

   if (rcvMsg.header.msgId == FAILURE)
   {
      printf("%s\n", rcvMsg.body.failure.failureMsg);
   }

   if (rcvMsg.header.msgId == FLAG_EARNED)
   {
      printf("%s\n", rcvMsg.body.flagEarned.flagMsg);
   }

   close(sockfd);

   return 0;
}
