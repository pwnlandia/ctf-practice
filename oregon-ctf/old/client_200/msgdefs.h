#ifndef MSG_DEFS_H
#define MSG_DEFS_H

#include <stdint.h>
#include <inttypes.h>

#define START_BYTES 0xAAAA
#define MAX_DATA_LEN 0xFFFF

#define SORTING_CHALLENGE_COUNT 100

const char* SECRET_PHRASE = "THE CAKE IS A LIE";

enum MsgType
{
   // Client -> Server
   FLAG_REQUEST = 0x8001,
   POLYNOMIAL_CHALLENGE_RESPONSE = 0x8002,
   SORTING_CHALLENGE_RESPONSE = 0x8003,
   SECRET_PHRASE_CHALLENGE_RESPONSE = 0x8004,
   XOR_CHALLENGE_RESPONSE = 0x8005,

   // Server -> Client
   FLAG_EARNED = 0x0001,
   POLYNOMIAL_CHALLENGE = 0x0002,
   SORTING_CHALLENGE = 0x0003,
   SECRET_PHRASE_CHALLENGE = 0x0004,
   XOR_CHALLENGE = 0x0005,
   FAILURE = 0x0100
};



struct MsgHeader
{
   uint16_t start;
   uint16_t msgId;
   uint32_t dataLen;
};

struct PolynomialChallenge
{
   uint32_t x;
   uint32_t a;
   uint32_t b;
   uint32_t c;
};

struct PolynomialChallengeResponse
{
   // Solution to polynomial challenge.
   // solution = ax^3 + bx^2 + cx
   uint32_t solution;
};

struct SortingChallenge
{
   uint32_t values[SORTING_CHALLENGE_COUNT];
};

struct SortingChallengeResponse
{
   // Solution to sorting challenge.
   // The values sorted into ascending order.
   uint32_t sortedValues[SORTING_CHALLENGE_COUNT];
};

struct SecretPhraseChallengeResponse
{
   // Should be "THE CAKE IS A LIE"
   char secretPhrase[512];
};

struct XorChallenge
{
   uint32_t a;
   uint32_t b;
   uint32_t c;
};

struct XorChallengeResponse
{
   uint32_t solution;
};

struct FlagEarned
{
   char flagMsg[512];
};

struct Failure
{
   char failureMsg[512];
};

struct Msg
{
   struct MsgHeader header;
   union body
   {
      unsigned char data[MAX_DATA_LEN];
      struct PolynomialChallenge polynomialChallenge;
      struct PolynomialChallengeResponse polynomialChallengeResponse;
      struct SortingChallenge sortingChallenge;
      struct SortingChallengeResponse sortingChallengeResponse;
      struct SecretPhraseChallengeResponse secretPhraseChallengeResponse;
      struct XorChallenge xorChallenge;
      struct XorChallengeResponse xorChallengeResponse;
      struct FlagEarned flagEarned;
      struct Failure failure;
   } body;
};


#endif
