import socket, struct

MT_FLAG_REQUEST = 0x8001
MT_POLY_RESP = 0x8002
MT_SORT_RESP = 0x8003
MT_SPHR_RESP = 0x8004
MT_XOR_RESP = 0x8005

MT_FLAG_EARN = 0x1
MT_POLY_CHAL = 0x2
MT_SORT_CHAL = 0x3
MT_SP_CHAL = 0x4
MT_XOR_CHAL = 0x5
MT_FAIL = 0x6

conn = socket.create_connection(('54.89.227.20', 12345))

def mget(n):
    ba = b''
    while len(ba) < n:
        ba += conn.recv(n-len(ba))
    return ba

def msgsend(t, start, data):
    conn.send(struct.pack("=HHI", start, t, len(data)) + data)

def msgrecv():
    hdr = struct.unpack("=HHI", mget(8))
    print('s=%04x t=%04x dl=%08x' % hdr)
    data = mget(hdr[2])
    print(data)
    return (hdr[0],hdr[1],hdr[2],data)

msgsend(MT_FLAG_REQUEST, 0xaaaa, b'')
msg = msgrecv()
while(msg[1] != MT_FAIL and msg[1] != MT_FLAG_EARN):
    mty = msg[1]
    content = msg[-1]
    response = [0, 0xaaaa, b'']
    if mty == MT_XOR_CHAL:
        a,b,c = struct.unpack("=III", content)
        r = a ^ b ^ c
        response[0] = MT_XOR_RESP
        response[2] = struct.pack("=I", r)
    elif mty == MT_POLY_CHAL:
        x,a,b,c = struct.unpack("=IIII", content)
        sol = (a * x * x) + (b * x) + c
        response[0] = MT_POLY_RESP
        response[2] = struct.pack("=I", sol)
    elif mty == MT_SORT_CHAL:
        values = list(struct.unpack("="+("I"*100), content))
        values.sort()
        response[0] = MT_SORT_RESP
        response[2] = struct.pack("="+("I"*100), *values)
    elif mty == MT_SP_CHAL:
        response[0] = MT_SPHR_RESP
        response[2] = b'THE CAKE IS A LIE\0'
    msgsend(*response)
    msg = msgrecv()
