foo() {
	asm("mov %eax,%ecx");
	asm("xor %eax,%eax");
	asm("label: add $0x1,%eax");
	asm("mov %eax,%edx");
	asm("imul %eax,%edx");
	asm("cmp %edx,%ecx");
	asm("jne label");
}
