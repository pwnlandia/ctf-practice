foo(int squared) {
	int i=0;
	int res;
	do {
		i = i+1;
		res = i*i;
	} while (res != squared);
	return i;
}
