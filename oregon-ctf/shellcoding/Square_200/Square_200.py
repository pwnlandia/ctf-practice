import socket
import sys
if len(sys.argv) == 1:
    print("Usage: Square.py <ip> <port>")
    sys.exit()

ip=sys.argv[1]
port=sys.argv[2]

s = socket.create_connection((ip,port))
s.send(b"\x90\x90\x90\x90\x90\x90\x90\x90")

while True:
    x = s.recv(1024)
    if len(x) == 0:
        break
    print(repr(x))

# Challenge was to write x86 code that caclulated the square root of a number
#  so iterate over integers and check
# 80483f0:       31 db                   xor    %ebx,%ebx
# 80483f2:       89 d9                   mov    %ebx,%ecx
# 80483f4:       0f af c9                imul   %ecx,%ecx
# 80483f7:       39 c1                   cmp    %eax,%ecx
# 80483f9:       74 03                   je     80483fe <main+0x11>
# 80483fb:       43                      inc    %ebx
# 80483fc:       eb f4                   jmp    80483f2 <main+0x5>
# 80483fe:       89 d8                   mov    %ebx,%eax

CODE = b"\x31\xDB\x89\xD9\x0F\xAF\xC9\x39\xC1\x74\x03\x43\xEB\xF4\x89\xD8"
TGTLEN = 32

if len(CODE) < TGTLEN:
    CODE += b'\x90' * (TGTLEN - len(CODE))

s = socket.create_connection((ip,port))
s.send(CODE)

while True:
    x = s.recv(1024)
    if len(x) == 0:
        break
    print(repr(x))

