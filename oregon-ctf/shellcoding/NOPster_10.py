import socket
import sys
if len(sys.argv) == 1:
    print("Usage: nop.py <ip> <port>")
    sys.exit()

ip=sys.argv[1]
port=sys.argv[2]

s = socket.create_connection((ip,port))
s.send(b"\x90\x90\x90\x90\x90\x90\x90\x90")

while True:
    x = s.recv(1024)
    if len(x) == 0:
        break
    print(repr(x))

