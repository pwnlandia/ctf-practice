f = open('secret.enc','r')
for c in f.read():
    if not(c.isalpha()):
        print(c,end="")
        continue
    if c==' ' or c=='\n':
        print(c,end="")
        continue
    if (ord(c) > ord('A')) & (ord(c) < ord('Z')) :
        print( chr(((ord(c)-ord('A')+26-10)%26) + ord('A')) , end="")
    else:
        print( chr(((ord(c)-ord('a')+26-10)%26) + ord('a')) , end="")
