import angr
import claripy
import sys

def main(argv):
  path_to_binary = argv[1]
  project = angr.Project(path_to_binary)

  start_address = 0x8048606
  initial_state = project.factory.blank_state(addr=start_address)

  # The binary is calling scanf("%8s %8s %8s %8s").
  # (!)
  password0 = claripy.BVS('password0', 64)
  password1 = claripy.BVS('password1', 64)
  password2 = claripy.BVS('password2', 64)
  password3 = claripy.BVS('password3', 64)

  # Determine the address of the global variable to which scanf writes the user
  # input. The function 'initial_state.memory.store(address, value)' will write
  # 'value' (a bitvector) to 'address' (a memory location, as an integer.) The
  # 'address' parameter can also be a bitvector (and can be symbolic!).
  # (!)
  password0_address = 0xaf83c78
  initial_state.memory.store(password0_address, password0)
  password1_address = 0xaf83c70
  initial_state.memory.store(password1_address, password1)
  password2_address = 0xaf83c68
  initial_state.memory.store(password2_address, password2)
  password3_address = 0xaf83c60
  initial_state.memory.store(password3_address, password3)

  path_group = project.factory.path_group(initial_state)

  def is_successful(path):
    stdout_output = path.state.posix.dumps(sys.stdout.fileno())
    return 'Good Job' in stdout_output

  def should_abort(path):
    stdout_output = path.state.posix.dumps(sys.stdout.fileno())
    return 'Try again' in stdout_output

  path_group.explore(find=is_successful, avoid=should_abort)

  if path_group.found:
    good_path = path_group.found[0]

    # Solve for the symbolic values. We are trying to solve for a string.
    # Therefore, we will use any_str, which returns a string instead of an
    # integer.
    # (!)
    solution0 = good_path.state.se.any_str(password0)
    solution1 = good_path.state.se.any_str(password1)
    solution2 = good_path.state.se.any_str(password2)
    solution3 = good_path.state.se.any_str(password3)
    solution = " ".join([solution0, solution1, solution2, solution3])

    print solution
  else:
    raise Exception('Could not find the solution')

if __name__ == '__main__':
  main(sys.argv)
