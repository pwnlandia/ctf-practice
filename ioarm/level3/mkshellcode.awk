#!/usr/bin/awk -f

BEGIN {
	ix = 1
	capture = 0
}

/shellcode_start/ {
	capture = 1
	next
}

/shellcode_stop/ {
	capture = 0
	next
}

capture {
	gsub("..", "0x& ", $2)
	len = split($2, opbytes, " ")
	for(i = len; i > 0; --i) {
		bytes[ix] = opbytes[i]
		++ix
	}
}

END {
	for(i = 1; i <= ix; ++i)
		printf("%c", bytes[i] + 0)
}
