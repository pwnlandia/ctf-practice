#include <stdio.h>
/* This is a program for determining the randomness of the stack over 
   multiple invocations.  It shows about 11 bits are randomized in the
   address.
	0xbe8d8cf4
	0xbebbccf4
	0xbe829cf4
	0xbe9dbcf4
	0xbe9f8cf4
	0xbe8cdcf4
	0xbea08cf4
	0xbeaeecf4
	0xbebcdcf4
	0xbee1fcf4
	0xbee65cf4
	0xbedcbcf4
*/
int main(int argc, char** argv) {
	printf("av=%p\n", argv);
	for(int i = 0;i < argc;i++)
		printf("av[%2d]=%p\n", i, argv);
	return 0;
}
