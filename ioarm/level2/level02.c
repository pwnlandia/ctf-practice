// level by bla
// this code has worked on x86 for decades, must be secure.
#include <stdio.h>
#include <unistd.h>

void win(){
        char *args[] = {"/bin/sh", 0};
        printf("win\n");
        setresuid(geteuid(), geteuid(), geteuid());
        execve(*args, args, 0);
}

void parse(char *arg){
        char buf[11] = {0}, index = 0;

        while(*arg){
                switch(*arg){
                        case 'b': index--; if(index < 0) index = 0; break;
                        case 'f': index++; if(index > 10) index = 10; break;
                        case 'w': buf[index] = *++arg; break;
                }
                arg++;
        }
        printf("you wrote: %s\n", buf);

}

int main(int argc, char**argv){

        printf("The source code for level02 is in /levels/level02.c\n");
        printf("The win is at %p\n", win);

        if(argc < 2)
                printf("Usage:\n\t%s [commands]\n", argv[0]);
        else
                parse(argv[1]);

        return 0;
}
