cmds = open("cmds", "r").read().split('\n')
pos = 0
output = []
for c in cmds:
    if len(c) == 0:
        continue
    if c[0] != '@':
        continue
    n = int(c.split(' ')[0][1:])
    x = eval(c.split(' ')[1])

    output.append(b'b' * ((256-n)-4))
    output.append(b'w' + bytes([(x & 0x000000ff)]))
    output.append(b'w' + bytes([(x & 0x0000ff00) >> 8]))
    output.append(b'w' + bytes([(x & 0x00ff0000) >> 16]))
    output.append(b'w' + bytes([(x & 0xff000000) >> 24]))
    output.append(b'b' * (n-4))
print(b''.join(output))
