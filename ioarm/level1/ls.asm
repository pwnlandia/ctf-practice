.text
.global _start
_start:
    adr   r0, path
    mov   r1, #0x0
    mov   r2, #0x0
    mov   r7, #0x5       // open
    svc   0

    adr   r1, buffer
    mov   r2, #256
    mov   r7, #0x8d
    svc   0              // getdents

    adr   r3, buffer
loop:
    ldr   r4, [r3]
    cmp   r4, #0x0
    beq   exit
    mov   r0, #0x1       // stdout
    add   r1, r3, #0xa   // char           d_name[]
    ldrh  r4, [r3, #0x8] // unsigned short d_reclen
    sub   r2, r4, #0xc
    mov   r7, #0x4       // write
    svc   0
    mov   r0, #0x1
    adr   r1, newline
    mov   r2, #0x1
    svc   0
    add   r3, r3, r4
    b     loop

exit:
    mov   r0, #0x0
    mov   r7, #0x1       // exit
    svc   0

path:
.asciz "/home/level2"

newline:
.asciz "\n"

buffer:
.space 256
