.text
.global _start
_start:
    adr r0, command
    adr r2, nullarray
    adr r3, nullarray

    mov r1, pc
    add r1, #24
    str r0, [r1]
    mov r7, #0x0b

    svc 0
    mov r0, #0x0
    mov r7, #0x1
    svc 0

argv:
.word 0
.word 0

command:
.asciz "/bin/sh"

nullarray:
.word 0
