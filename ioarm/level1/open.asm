.text
.global _start
_start:
    #adr r0, command
    #adr r2, nullarray
    #adr r3, nullarray

    #mov r1, pc
    #add r1, #24
    #str r0, [r1]
    #mov r7, #0x0b

    adr   r0, path
    mov   r1, #0x0
    mov   r2, #0x0
    mov   r7, #0x5      // open
    svc   0

    adr   r1, false
    cmp   r0, #0x0
    adrge r1, true
    mov   r0, #0x1
    mov   r2, #0x6
    mov   r7, #0x4      // write
    svc   0

    mov   r0, #0x0
    mov   r7, #0x1      // exit
    svc   0

path:
.asciz "/home/level2/passwd"

true:
.asciz "exists"

false:
.asciz "absent"
