#!/bin/bash
arm-linux-gnueabi-as wu.asm -o wu.o
arm-linux-gnueabi-ld -s -z max-page-size=8 -o wu.bin wu.o
arm-linux-gnueabi-strip wu.bin
arm-linux-gnueabi-strip -R '.ARM.attributes' wu.bin
xxd -p wu.bin > wu.hex
