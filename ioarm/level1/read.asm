.text
.global _start
_start:
    adr   r0, path
    mov   r1, #0x0
    mov   r2, #0x0
    mov   r7, #0x5      // open
    svc   0

    adr   r1, buffer
    mov   r2, #0x100
    mov   r7, #0x3      //read
    svc   0

    mov   r2, r0
    mov   r0, #0x1
    mov   r7, #0x4      // write
    svc   0

exit:
    mov   r0, #0x0
    mov   r7, #0x1      // exit
    svc   0

path:
.asciz "/home/level2/pw_vault.txt"

buffer:
.space 256
