#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>

struct credentials {
        char username[32];
        char password[32];

};

char dbfilename[] = "/opt/cwe311/cwe311.db";
char debug_log[] = "/opt/cwe311/cwe311.log";
FILE *debug_handle = NULL;

void debug_msg(char *format, ...)
{
        va_list args;

        if (debug_handle) {
                va_start(args, format);
                vfprintf(debug_handle, format, args);
                va_end(args);                                                                   }
}

int authenticate(struct credentials *authorized_creds, struct credentials *login_creds)
{
        int authenticated = 0;

        if (!strcmp(authorized_creds->username, login_creds->username) && !strcmp(authorized_creds->password, login_creds->password)) {
                authenticated = 1;
        }

        debug_msg("%d: login attempt: username=%s, password=%s\n", time(), login_creds->username, login_creds->password);

        return authenticated;
}

void null_out_newline(char *str)
{
        while(*str) {
                if (*str == '\r' || *str == '\n') {
                        *str = 0;
                        break;
                }
                str++;
        }
}

void read_credentials_db(char *dbfilename, struct credentials *creds)
{
        FILE *dbfile = fopen(dbfilename, "rb");
        if (!dbfile) {
                debug_msg("%d: could not open credentials database\n", time());
                perror("Could not open credentials database");
                exit(1);
        }

        if (fread(creds, 1, sizeof(struct credentials), dbfile) != sizeof(struct credentials)) {
                debug_msg("%d: could not read credentials database\n", time());
                fprintf(stderr, "Could not read credentials database\n");
                exit(1);
        }

        fclose(dbfile);
}

int main(int argc, char **argv)
{
        struct credentials authorized_creds;
        struct credentials login_creds;
        int authenticated = 0;

        debug_handle = fopen(debug_log, "a");

        debug_msg("%d: new session started\n", time());

        read_credentials_db(dbfilename, &authorized_creds);

        printf("RESTRICTED ACCESS ONLY\n");
        printf("======================\n");
        printf(" User ID: ");
        if(!fgets(login_creds.username, sizeof(login_creds.username), stdin)) {
                debug_msg("%d: did not receive username\n", time());
                printf("ERROR: ACCESS DENIED.\n");                                                      exit(1);
        }
        null_out_newline(login_creds.username);

        printf("Password: ");
        if(!fgets(login_creds.password, sizeof(login_creds.password), stdin)) {
                debug_msg("%d: did not receive password\n", time());
                printf("ERROR: ACCESS DENIED.\n");
                exit(1);
        }
        null_out_newline(login_creds.password);

        authenticated = authenticate(&authorized_creds, &login_creds);
        if (authenticated) {
                debug_msg("%d: successful login\n", time());
                printf("ACCESS GRANTED.\n");
                system("/bin/sh");
        } else {
                debug_msg("%d: failed login\n", time());
                printf("ERROR: ACCESS DENIED.\n");
                exit(1);
        }
}
