#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

const char *args[] = {
        "/bin/sh",
        NULL
};

char *envvars[] = {
        NULL,
        NULL
};

void prepare_environment(uint32_t size)
{
        char *buffer;
        uint32_t bytes_read;

        buffer = malloc(size);
        if (buffer) {
                strcpy(buffer, "A=");
                bytes_read = fread(buffer+2, 1, size-3, stdin);
                buffer[bytes_read] = 0;

                envvars[0] = buffer;

                setreuid(getuid(), getuid());
        }
}


int main(int argc, char **argv)
{
        uint32_t size, retval;

        size = strtoul(argv[1], NULL, 10);

        prepare_environment(size);

        execv("/bin/sh", args);
}
