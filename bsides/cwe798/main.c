#include <stdio.h>
#include <stdlib.h>

char authorized_username[] = "\x2c\x2b\x26\x21\x2f\x24";
char authorized_password[] = "\x20\x25\x39\x22\x3f\x2b";

void munge_string(char *dest, char *source)
{
        char ch;
        char tmp;

        while (ch = *source++) {
                *dest++ = ch ^ 'J';
        }
        *dest = '\x00';
}

int authenticate(char *username, char *password)
{
        char munged_username[128];
        char munged_password[128];
        int authenticated = 0;

        munge_string(munged_username, username);
        munge_string(munged_password, password);

        if (!strcmp(munged_username, authorized_username) && !strcmp(munged_password, authorized_password)) {
                authenticated = 1;
        }

        return authenticated;
}

void null_out_newline(char *str)
{
        while(*str) {
                if (*str == '\r' || *str == '\n') {
                        *str = 0;
                        break;
                }
                str++;
        }
}

int main(int argc, char **argv)
{
        char username[128];
        char password[128];
        char clear_greeting[128];
        char munged_greeting[] = "\x0d\x18\x0f\x0f\x1e\x03\x04\x0d\x19\x66\x6a\x1a\x18\x05\
x0c\x0f\x19\x19\x05\x18\x6a\x0c\x0b\x06\x01\x0f\x04\x64";
        int authenticated = 0;

        printf("RESTRICTED ACCESS ONLY\n");
        printf("======================\n");
        printf(" User ID: ");
        if(!fgets(username, sizeof(username), stdin)) {
                printf("ERROR: ACCESS DENIED.\n");
                exit(1);
        }
        null_out_newline(username);

        printf("Password: ");
        if(!fgets(password, sizeof(password), stdin)) {
                printf("ERROR: ACCESS DENIED.\n");
                exit(1);
        }
        null_out_newline(password);

        authenticated = authenticate(username, password);
        if (authenticated) {
                munge_string(clear_greeting, munged_greeting);
                printf("%s\n", clear_greeting);
                system("/bin/sh");
        } else {
                printf("ERROR: ACCESS DENIED.\n");
                exit(1);
        }
}
