#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
typedef unsigned int uint32_t;

void copy_data(char *dest, char *source, uint32_t max_length)
{
        uint32_t index = 0;
        while(source[index] && (index < max_length)) {
                dest[index] = source[index];
                index++;
        }
}

uint32_t output_packet(char *packet)
{
        uint32_t string_size, packet_size;
        char *packet_buffer;

        string_size = *(uint32_t *)packet;
        packet_size = sizeof(uint32_t) + string_size;

        if (packet_size >= 2048) {
                fprintf(stderr, "Packet contents too large!\n");
                exit(1);
        }

        packet_buffer = alloca(packet_size);

        *((uint32_t *)packet_buffer) = string_size;

        copy_data(packet_buffer + sizeof(uint32_t), packet + sizeof(uint32_t), string_size);

        fwrite(packet_buffer, string_size, 1, stdout);

        return packet_size;
}

int main(int argc, char **argv)
{
        char *file_buffer;
        struct stat statbuf;
        uint32_t number_of_packets, which_packet, packet_offset, packet_size;
        int fd;

        fd = open(argv[1], O_RDONLY);
        if (fd < 0) {
                perror("Could not open file");
                exit(1);
        }

        if(fstat(fd, &statbuf) < 0) {
                perror("Couldn't determine size of file");
                exit(1);
        }

        file_buffer = malloc(statbuf.st_size);
        if (!file_buffer) {
                fprintf(stderr, "Could not allocate file buffer");
                exit(1);
        }

        if (read(fd, file_buffer, statbuf.st_size) != statbuf.st_size) {
                perror("Couldn't read entire file");


                exit(1);
        }

        number_of_packets = *((uint32_t *)file_buffer);

        fwrite(&number_of_packets, sizeof(uint32_t), 1, stdout);

        packet_offset = sizeof(uint32_t);
        for (which_packet = 1; which_packet <= number_of_packets; which_packet++) {
                packet_size = output_packet(file_buffer + packet_offset);

                packet_offset += packet_size;
        }

        free(file_buffer);
        close(fd);

        return 0;
}
