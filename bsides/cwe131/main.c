#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char hexdigits[] = "0123456789abcdef";

void output_hex(char *dest,char *source)
{
        char ch = *source;

        *dest++ = hexdigits[ch >> 4];
        *dest++ = hexdigits[ch & 0x0f];
}

int escape_special_chars(char *dest, char *source)
{
        int escaped = 0;
        char *srcptr = source;
        char *dstptr = dest;
        char ch;

        while (ch = *srcptr++) {
                if (ispunct(ch) || isspace(ch)) {
                        *dstptr++ = '%';
                        output_hex(dstptr, srcptr);
                        dstptr += 2;
                        escaped++;
                }
        }
        return escaped;
}


int main(int argc, char **argv)
{
        char buffer[512];
        int length, escaped;

        length = strlen(argv[1]);
        if (length >= sizeof(buffer) - 1) {
                fprintf(stderr, "String too long!");
                exit(1);
        }

        escaped = escape_special_chars(buffer, argv[1]);

        return escaped;
}
