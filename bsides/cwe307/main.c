#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

uint32_t secret_number;

void dead_child_catcher(int signal)
{
        int status;

        // need to wait for child processes to exit...
        while(waitpid(-1, &status, 1) > 0);
}

int initialize_server(uint16_t port)
{
        int fd;
        int sockopt = 1;
        struct sockaddr_in sin;

        sin.sin_family = AF_INET;
        sin.sin_port = htons(port);
        sin.sin_addr.s_addr = INADDR_ANY;

        if(signal(SIGCHLD, dead_child_catcher) == SIG_ERR) {
                perror("Could not register signal handler");
                exit(1);
        }

        fd = socket(AF_INET, SOCK_STREAM, 0);
        if (fd == -1) {
                perror("Could not create socket");
                exit(1);
        }

        if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt))) {
                perror("Could not set socket options");
                exit(1);
        }

        if(listen(fd, 20)) {
                perror("Could not listen on socket");
                exit(1);
        }

        return fd;
}

void connection_handler(int fd)
{
        uint32_t guess;
        size_t input_size;
        void (*end_ptr)(int) = NULL;
        char input_buffer[256];
        char question[] = "What number am I thinking of?\n";
        char congrats[] = "Congrats!\n";
        char nope[] = "Gotta throw down that NOPE card...\n";

        memset(input_buffer, sizeof(input_buffer), 0);

        if(send(fd, question, sizeof(question), 0) != sizeof(question)) {
                fprintf(stderr, "Couldn't send output string\n");
                close(fd);
                exit(1);
        }

        input_size = recv(fd, input_buffer, sizeof(input_buffer)-1, 0);
        if (input_size < 0) {
                fprintf(stderr, "Error reading input string\n");
                close(fd);
                exit(1);
        }

        input_buffer[input_size] = '\x00';

        guess = strtoul(input_buffer, (char **)&end_ptr, 10);
        if ((guess == secret_number) && end_ptr) {
                send(fd, congrats, sizeof(congrats), 0);
                end_ptr(fd);
        } else {
                send(fd, nope, sizeof(nope), 0);
        }
}

void handle_connections(int listening_fd, void (*handler)(int))
{
        int notdone = 1;
        int accepted_fd;
        int child_pid;
        struct sockaddr_in sin;
        uint32_t sockaddr_len = sizeof(sin);

        while(notdone) {
                accepted_fd = accept(listening_fd, (struct sockaddr *)&sin, &sockaddr_len
);
                if (accepted_fd != -1) {
                        child_pid = fork();
                        if (child_pid != -1) {
                                if (child_pid) {
                                        close(accepted_fd);
                                } else {
                                        handler(accepted_fd);
                                        close(accepted_fd);
                                        exit(1);
                                }
                        }
                }
        }
}

int main(int argc, char **argv)
{
        int fd;

        srand(time(0) ^ (uint32_t)&fd);

        secret_number = rand();

        fd = initialize_server(10307);

        handle_connections(fd, connection_handler);

        return 0;
}
