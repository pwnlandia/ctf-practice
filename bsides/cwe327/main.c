#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#define _XOPEN_SOURCE       /* See feature_test_macros(7) */
#include <unistd.h>
#include <crypt.h>

struct credentials {
        char username[32];
        char password[32];
};

int authenticate(struct credentials *authorized_creds, struct credentials *login_creds)
{
        int authenticated = 0;
        char *encrypted_passwd;

        encrypted_passwd = crypt(login_creds->password, authorized_creds->password);

    if (encrypted_passwd &&
            !strcmp(authorized_creds->username, login_creds->username) &&
            !strcmp(authorized_creds->password, encrypted_passwd)) {
                authenticated = 1;
        }

        return authenticated;
}

void null_out_newline(char *str)
{
        while(*str) {
                if (*str == '\r' || *str == '\n') {
                        *str = 0;
                        break;
                }
                str++;
        }
}

void read_credentials_db(char *dbfilename, struct credentials *creds)
{
        char dbline[1024];
        FILE *dbfile = fopen(dbfilename, "rb");
        char *token_ptr, *username, *crypted_passwd;

        if (!dbfile) {
                perror("Could not open credentials database");
                exit(1);
        }

        fgets(dbline, sizeof(dbline), dbfile);

        token_ptr = &dbline;

        username = strsep(&token_ptr, ":");
        if (username) {
                strncpy(creds->username, username, sizeof(creds->username)-1);

                crypted_passwd = strsep(&token_ptr, ":");
                if (crypted_passwd) {
                        strncpy(creds->password, crypted_passwd, sizeof(creds->password)-
1);
                }
        }

        fclose(dbfile);
}

int main(int argc, char **argv)
{
        struct credentials authorized_creds;
        struct credentials login_creds;
        int authenticated = 0;

        read_credentials_db("/opt/cwe327/cwe327.db", &authorized_creds);

        printf("RESTRICTED ACCESS ONLY\n");
        printf("======================\n");
        printf(" User ID: ");
        if(!fgets(login_creds.username, sizeof(login_creds.username), stdin)) {
                printf("ERROR: ACCESS DENIED.\n");
                exit(1);
        }
        null_out_newline(login_creds.username);

        printf("Password: ");
        if(!fgets(login_creds.password, sizeof(login_creds.password), stdin)) {
                printf("ERROR: ACCESS DENIED.\n");
                exit(1);
        }
        null_out_newline(login_creds.password);

        authenticated = authenticate(&authorized_creds, &login_creds);
        if (authenticated) {
                printf("ACCESS GRANTED.\n");
                system("/bin/sh");
        } else {
                printf("ERROR: ACCESS DENIED.\n");
                exit(1);
        }
}
