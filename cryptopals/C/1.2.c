#include<stdio.h>
#include<string.h>

int main(int argc, char *argv[])
{

char *input; /* the value to be converted */
input= (char *) malloc(100); /* allocating size for the input */
scanf("%s",input);

char *input_2; /* the value to be converted */
input_2= (char *) malloc(100); /* allocating size for the input */
scanf("%s",input_2);

int input_length=strlen(input);

char *final_output; /* declare the base-64 variable */
double final_out_size=input_length*4; /* length of that variable */
final_output=(char *) malloc(final_out_size); /* allocating the size for the base-64 variable */

char *output; /* The binary equivalent of each hex substring */
output= (char *) malloc(4); /* allocating the size for the binary */

char *output_2; /* The binary equivalent of each hex substring */
output_2= (char *) malloc(4); /* allocating the size for the binary */  

char *temp; /* string variable to hold the substrings in the for loop */
temp=(char *) malloc(4);

int r,t,v,p; /* useful variables for the for loop */

char *lookup[16]={"0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"};
char *looksup[16]={"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"};

for(int i=0;i<=input_length-1;i++) /* partition the hex string into parts of 3 hex digits and work on them */
{ 

 memset(&output[0],0,sizeof(output)); /* reset the internal output every time */
 memset(&output_2[0],0,sizeof(output_2)); /* reset the internal output every time */
 memset(&temp[0],0,sizeof(temp)); /* reset the internal temp every time */

if(input[i]>57) t=input[i]-87;
else t=input[i]-48;
if(input_2[i]>57) r=input_2[i]-87;
else r=input_2[i]-48;


strcat(output, *(lookup+t) ); /*get the actual hex representation*/ 
strcat(output_2, *(lookup+r) ); 

for (int u=0;u<4;u++) /*perform the Xoring mechanism*/
{
if(output[u]==output_2[u]) strcat(temp,"0");
else strcat(temp,"1");
}

v = strtol(temp, 0, 2); /* convert the binary value to a number */

strcat(final_output, *(looksup+v));

}
puts(final_output); //print out the base-64

return 0;
}
