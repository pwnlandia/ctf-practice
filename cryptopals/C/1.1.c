#include<stdio.h>

int main(int argc, char *argv[])
{

char *input; /* the value to be converted */
input= (char *) malloc(100); /* allocating size for the input */
scanf("%s",input);

int input_length=strlen(input);
//printf(" length %d\n",input_length); /* check the length you got from the input */

char *modified_input;
modified_input= (char *) malloc(100); /* allocating size for the input */


if(input_length%3==0) /* if statements to do zero padding if the input is not divisible by 3 */
strcpy(modified_input,input);
else if(input_length%3==1)
{strcat(modified_input,"0");
 strcat(modified_input,input);
 input_length++;
}
else if(input_length%3==2)
{strcat(modified_input,"0");
 strcat(modified_input,"0");
 strcat(modified_input,input);
 input_length+=2;
}

char *final_output; /* declare the base-64 variable */
double final_out_size=input_length*2/3; /* length of that variable */
final_output=(char *) malloc(final_out_size); /* allocating the size for the base-64 variable */

char *temp; /* string variable to hold the substrings in the for loop */
temp=(char *) malloc(3);

char *b; /* string variable for the binary equivalent of each base-64 character*/
b= (char *) malloc(6);

char *output; /* The binary equivalent of each hex substring */
output= (char *) malloc(12); /* allocating the size for the binary */ 

char *lookup[64]={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U",
"V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u",
"v","w","x","y","z","0","1","2","3","4","5","6","7","8","9","+","/"};

int t,k,v; /* useful variables for the for loop */

for(int i=0;i<=input_length-1;i+=3) /* partition the hex string into parts of 3 hex digits and work on them */
{ t=2048;
  
  memset(&temp[0], 0, sizeof(temp));  /* reset the temp value every time */
  
  temp[0]=modified_input[i];
  temp[1]=modified_input[i+1];
  temp[2]=modified_input[i+2];
  
  v = strtol(temp, 0, 16); /* convert the hex value to a number */
  memset(&output[0],0,sizeof(output)); /* reset the internal output every time */


while(t) /* loop till output has the binary eqiuivalent of the hex input */
{
    strcat(output, t <= v ? "1" : "0");
    if(t <= v)
        v -= t;
    t /= 2;
}

for (int f=0;f<12;f+=6) /*loop to change the binary base to a base 64 */
  {
         b[0]=output[f];
         b[1]=output[f+1];
	 b[2]=output[f+2];
	 b[3]=output[f+3];
	 b[4]=output[f+4];
	 b[5]=output[f+5];
   k= strtoul(b, 0, 2); /* value of each substring */

strcat(final_output, *(lookup+k) ); /*concatenate the final string with the true base-64 value */
}
}

puts(final_output); //print out the base-64 result

return 0;
}
