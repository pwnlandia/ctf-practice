// Cryptopals hex to base64
// convert hex to base64 the easy way
//
// 49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d  string
// SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t  da hash

extern crate base64;
extern crate hex;

use hex::{decode};
use base64::{encode}; 
use std::char;

fn main() {
    let a = convert(decode("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d").unwrap());
    let mut s: String = a.into_iter().collect();
    s = encode(&s);
    println!("{}",s);
}

// casting loop
fn convert(vec: Vec<u8>) -> Vec<char> {
    let mut a: Vec<char> = vec!['x';vec.len()];  
    for i in 0..vec.len() {
        a[i] = vec[i] as char;
    }
    a
}

// Maybe I could implement a trait or whatever is needed for convert to be dotted instead of wrapping on line 15
