//extern crate base64;
extern crate rustc_serialize as serialize;
use serialize::base64::{self, ToBase64};
use serialize::hex::FromHex;
use std::ascii::AsciiExt;

//use base64::{encode, decode};

fn main() {
    //let a = b"hello world";
    //let b = "aGVsbG8gd29ybGQ=";
    //let c  = encode(a);
    //println!("encoded hellow is {}", c);
    //assert_eq!(encode(a), b);
    //assert_eq!(a, &decode(b).unwrap()[..]);
    //println!("Hello world!");
    let input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    let somestuff:&[u8] = b"49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    let result = input.from_hex().unwrap().to_base64(base64::STANDARD);
    let input_chars: Vec<char> = input.chars().collect();
    println!("result {}", result);
    println!("input_chars {:?}", input_chars);
    println!("input_chars {:?}", input.chars());
    println!("Input.as_bytes {:?}", input.as_bytes());
    println!("Input.to_ascii {:?}", input.to_ascii_lowercase().as_bytes());
    println!("Input.hex {:?}", input.from_hex());
    println!("Input.hex.unwrap {:?}", input.from_hex().unwrap());
    println!("somestuff {:?}", somestuff);
}
