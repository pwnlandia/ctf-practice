extern crate sbxor;

use sbxor::*;
use std::io::{BufReader,BufRead};
use std::fs::File;

fn main() {
   // for each line in that file, read it in and decode
    let enc_strs = populate_encoded_strings();
    for enc_str in enc_strs {
        let mut enc_eng = enc_str.clone();  // got to stop this clonieness!
        let mut sbxor = Sbxor::create(enc_str);
        sbxor.decode_sbxor();
        if sbxor.is_english {   // create sbxor object cause it's easier
            let mut eng_sbxor = Sbxor::create(enc_eng);
            eng_sbxor.decode_sbxor_char(sbxor.encoding_byte.unwrap());
            println!("{:?}", eng_sbxor.decoded_strs); 
        }
    }
}

fn populate_encoded_strings() -> Vec<String> {
    let mut encoded_strings = vec![]; 
    let file = File::open("strings.txt").expect("../strings.txt DNE");
    for line in BufReader::new(file).lines() {
        encoded_strings.push(line.unwrap());
    }
    
    encoded_strings
}
