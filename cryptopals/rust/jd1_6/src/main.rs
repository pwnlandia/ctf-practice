extern crate bit_vec;
extern crate sbxor;
extern crate base64;
extern crate hex;

use std::collections::HashMap;
use std::io::prelude::*;
use std::fs::File;
use bit_vec::BitVec;
use sbxor::*;
use base64::{decode_config};
use hex::encode;


fn main() {
    let text: Vec<char> = open_file();
    let text = convert(text);
    let text = decode_config(&text, base64::MIME).unwrap(); //TODO: Prove this unwrap
    let text = match String::from_utf8(text) {
        Ok(v) => v,
        Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
    };

    let candidate_keysizes = find_keysizes(text.as_str());   // returns vec of (distance, keysize)
   
    if !candidate_keysizes.is_empty() {
        let mut min = candidate_keysizes[0].0;  // first key is only known min
        let mut keysize = 0;                    // initial keysize when none are known
        for key in candidate_keysizes {        
            if min > key.0 {
                min = key.0;
                keysize = key.1; 
            }
        }
        let split_blocks = split_blocks_keysize(&text, keysize);
        let transposed_blocks = transpose(split_blocks, keysize);
        let mut all_encoding_bytes = vec![]; 
        for transposed_block in transposed_blocks {
            let encoding_byte = decode_sbxor(transposed_block);
            all_encoding_bytes.push(encoding_byte);
        }
        let fin: String = convert2(all_encoding_bytes).into_iter().collect();
        println!("{:?}", fin);
    }
}

/// Uses a window of varying size to scroll over the
/// strings. The size of the window is known as keysize
/// whose value changes in the outer loop. Each string 
/// within the window is passed to the hamming function.
/// returns a vector containing keys that are potential
/// candidates.
fn find_keysizes(text: &str) -> Vec<(f64, usize)> {
    let mut result: Vec<(f64, usize)> = vec![]; // (distance, keysize)
    for keysize in 2..41 {
        let mut l_win_edge = 0; // window that scrolls through the available characters
        let mut r_win_edge = keysize; 
        let mut dist: f64 = 0.0;
        let mut dist_list: Vec<f64> = vec![];
        while r_win_edge < text.len() - keysize {
            let string1 = &text[l_win_edge..r_win_edge];
            l_win_edge += keysize;
            r_win_edge += keysize;
            let string2 = &text[l_win_edge..r_win_edge];
            l_win_edge += keysize;
            r_win_edge += keysize;
            dist = hamming(string1.to_string(), string2.to_string());
            dist = dist as f64 / keysize as f64;
            dist_list.push(dist);
        }
        let mut avg: f64 = 0.0;
        let len: f64 = dist_list.len() as f64;
        for dist in dist_list { // i think other dist is shadowed in the forloop 
            avg += dist;
        }   
        let avg = avg/len; 
        result.push((avg, keysize));
    }

    result
}

/// Splits a slice of text up into keysize portions
fn split_blocks_keysize(text: &str, keysize: usize) -> Vec<String> {
    let mut l_win_edge: usize = 0;
    let mut r_win_edge = keysize;
    let mut blocks: Vec<String> = vec![];
    while r_win_edge + keysize < text.len() {
        blocks.push(text.to_string().get(l_win_edge..r_win_edge).unwrap().to_string());
        l_win_edge += keysize;
        r_win_edge += keysize;
    }

    blocks
}

//TODO: Change from Sbxor or update Sbxor to use character frequency
/// Transposes blocks provided in a vector of strings. There will be
/// keysize number of transposed blocks, returned in the form of a 
/// Sbxor object
fn transpose(blocks: Vec<String>, keysize: usize) -> Vec<Sbxor> {
    let mut transposed_blocks: Vec<Sbxor> = vec![];
    for _ in 0..keysize {
        transposed_blocks.push(Sbxor::new());
    }
    for block in blocks {
        let mut i = 0; 
        let block_bytes: &[u8] = block.as_bytes();
        for byte in block_bytes {
            transposed_blocks[i].encoded_str.push(char::from(*byte));
            i += 1;
        }
    }

    transposed_blocks
}

//TODO: Clean up this function
fn decode_sbxor(mut thing: Sbxor) -> u8{
    let mut tups: Vec<(bool, f32, String, u8)> = vec![]; 
    for i in 0..255 {
        let vec = thing.encoded_str.clone(); 
        let vec = vec.into_bytes(); 
        let mut chrs_after_xor: Vec<char> = convert2(vec.into_iter().map(|x| x ^ i).collect());
        let s: String = chrs_after_xor.into_iter().collect();
        let tup: (bool, f32) = is_english(&s);
        if tup.0 {
            tups.push((tup.0, tup.1, s, i)); // `i` is the encoding character 
        }
    }
    //take the highest score in tups and it's associated encoding character
    let mut max = 0.0;
    let mut ret_chr = 0;
    for tup in tups {
        if max < tup.1 {
            ret_chr = tup.3;
            max = tup.1;
        } 
    }

    ret_chr
}

//TODO: Clean up this function
fn is_english(trans_block_txt: &String) -> (bool, f32) {
    let mut freq_arr: [(char, f32);27] = // an array of English letters and their frequency
        [(32 as char, 0.13000), (97 as char, 0.08167), (98 as char, 0.01492),
         (99 as char, 0.02782), (100 as char, 0.04253), (101 as char, 0.12702),
         (102 as char, 0.02228), (103 as char, 0.02015), (104 as char, 0.06094),
         (105 as char, 0.06094), (106 as char, 0.00153), (107 as char, 0.00772), 
         (108 as char, 0.04025), (109 as char, 0.02406), (110 as char, 0.06749), 
         (111 as char, 0.07507), (112 as char, 0.01929), (113 as char, 0.00095), 
         (114 as char, 0.05987), (115 as char, 0.06327), (116 as char, 0.09056), 
         (117 as char, 0.02758), (118 as char, 0.00978), (119 as char, 0.02360), 
         (120 as char, 0.00150), (121 as char, 0.01974), (122 as char, 0.00074)]; 

    let total_num_chars:f32 = trans_block_txt.len() as f32;
    let mut num_alphabetic:f32 = 0.0; // number of seen characters that are English letters
    let mut score: f32 = 0.0;
    for chr in trans_block_txt.chars() {
        if chr.is_ascii_alphabetic() {
            num_alphabetic += 1_f32;
            match freq_arr.binary_search_by_key(&((chr.to_lowercase().to_string().as_bytes())[0]), |&(a,b)| a as u8) {
                Ok(x) => score += freq_arr[x].1, // x is an index in freq_arr
                Err(x) => panic!("shouldn't reach this") 
            }
        }
    }

    //filter all of the strings that have unreasonable number of non-English characters
    if 0.62 < num_alphabetic / total_num_chars {
        return (true, score);
    } 

    (false, score)
}

// Helper functions

/// Computes the hamming distance of a given string.
fn hamming(string1: String, string2: String) -> f64 {
    let mut distance = 0;
    let bv1 = BitVec::from_bytes(string1.as_bytes());
    let bv2 = BitVec::from_bytes(string2.as_bytes());
    let bv_tuple = bv1.iter().zip(bv2.iter());
    for pair in bv_tuple {
        if pair.0 != pair.1 {
            distance += 1;
        }
    }
   
    distance as f64
}

fn open_file() -> Vec<char> {
    let filename = "6.txt";
    let mut file_handle = File::open(filename).expect("file not found");    // removed ?
    let mut text = String::new();
    file_handle.read_to_string(&mut text)
                          .expect("something went wrong reading the file");

    text.chars().collect()
}

// casting loop
fn convert(vec: Vec<char>) -> Vec<u8> {
    let mut a: Vec<u8> = vec![0;vec.len()];
   for i in 0..vec.len() {
        a[i] = vec[i] as u8;
    }

    a
}

// casting loop
fn convert2(vec: Vec<u8>) -> Vec<char> {
    let mut a: Vec<char> = vec!['x';vec.len()];
    for i in 0..vec.len() {
        a[i] = vec[i] as char;
    }

    a
}
