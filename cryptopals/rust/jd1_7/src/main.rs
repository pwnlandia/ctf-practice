// example from https://docs.rs/openssl/0.10.11/openssl/symm/index.html
extern crate base64;
extern crate openssl;

use openssl::symm::{decrypt, Cipher};
use std::fs::File;
use std::io::prelude::*;

// file should be placed in same dir as src/
static FILENAME: &'static str = "7.txt";

// AES is symmetric, this is the key
static KEY: &'static [u8] = b"YELLOW SUBMARINE";

fn main() -> std::io::Result<()> {
    // open the file and read into string
    let mut file = File::open("7.txt")?;
    let mut data = String::new();
    file.read_to_string(&mut data)?;
    
    //get rid of trailing newline
    data.pop();    

    // define cipher as being AES-128
    let cipher = Cipher::aes_128_ecb();

    // data comes in b64 encrypted
    let mut data = base64::decode(&data).unwrap();

    // use openssl lib to decrypt the data using the key and AES-128
    let decrypted_data = decrypt(cipher, KEY, None, &data).unwrap();

    // convert from ascii chars to rust String type
    let contents = match String::from_utf8(decrypted_data) {
                       Ok(v) => v,
                       Err(e) => panic!("Invalid: {}", e)
    };

    Ok(())
}


// Turn it into a library!!!
