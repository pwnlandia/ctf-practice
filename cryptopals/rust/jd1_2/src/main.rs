// set 1 exercise 2 cryptopals
//
// 1c0111001f010100061a024b53535009181c  hex 
// 686974207468652062756c6c277320657965  xor against this
// 746865206b696420646f6e277420706c6179  should produce this
extern crate hex;

use hex::{encode, decode};
use std::str;

fn main() {
    let vec1 = decode("1c0111001f010100061a024b53535009181c").unwrap();
    let vec2 = decode("686974207468652062756c6c277320657965").unwrap();
    let soln: Vec<(u8)> = a.into_iter().zip(b).map(|x| x.0 ^ x.1).collect();
    let soln = hex::encode(soln);
    println!("{}", soln);
}