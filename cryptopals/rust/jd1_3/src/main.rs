extern crate sbxor;

use sbxor::*;

fn main() {
    let encoded_str = 
        "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736".to_string(); 
    let mut sbxor = Sbxor::create(encoded_str);
    sbxor.decode_sbxor();
    let encoding_byte = sbxor.encoding_byte.unwrap();
    sbxor.decode_sbxor_char(encoding_byte);
    if sbxor.is_english {
        println!("{:?}", sbxor.decoded_strs.pop().unwrap());
    }
}
