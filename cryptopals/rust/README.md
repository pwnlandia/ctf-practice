install rust
  (https://www.rust-lang.org/en-US/install.html)
  curl https://sh.rustup.rs -sSf | sh
  Add cargo bin path to your command PATH
     /usr/local/home/wuchang/.cargo/bin/rustc

Build your first project
  mashimaro <cryptopals/rust> 11:13AM % cargo new 1_1
       Created library `1_1` project
  mashimaro <cryptopals/rust> 11:14AM % cd 1_1/src
  mashimaro <1_1/src> 11:16AM % cat main.rs
  fn main() {
    println!("Hello world!");
  }
  mashimaro <cryptopals/rust> 11:36AM % cat 1_1/Cargo.toml
  [package]
  name = "1_1"
  version = "0.1.0"
  authors = ["wuchang <wuchangfeng@bitbucket.org>"]

  [dependencies]
  base64 = "0.9.0"
  mashimaro <rust/1_1> 11:15AM % cargo run
     Compiling 1_1 v0.1.0
  (file:///usr/local/home/wuchang/bitbucket/ctf-practice/cryptopals/rust/1_1)
      Finished debug [unoptimized + debuginfo] target(s) in 0.79 secs
       Running `target/debug/1_1`
  Hello world!
