extern crate sbxor;

use sbxor::*;

fn main() {
    let mut input_phrase = "Burning 'em, if you ain't quick and nimble I go crazy when I hear a cymbal".to_string();
    let mut encoding_phrase = "ICE".to_string();
    let mut sbxor = Sbxor::new();
    sbxor.encode_sbxor_phrase(encoding_phrase, input_phrase);
    println!("{:?}", sbxor.encoded_str);
}
