extern crate hex;
extern crate zipWith;
use zipWith::IntoZipWith;
use std::iter::Iterator;

fn main() {
    let input = "1c0111001f010100061a024b53535009181c";
    let key = "686974207468652062756c6c277320657965";
    let inputdecode: Vec<u8> = hex::decode(input).unwrap();
    let keydecode: Vec<u8> = hex::decode(key).unwrap();
    let result: Vec<u8> = inputdecode.zip_with(keydecode, | l, r | l ^ r).collect();
    let reshex = hex::encode(result);
    println!("{:?}", reshex);
}
