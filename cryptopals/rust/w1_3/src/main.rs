extern crate hex;
use std::iter::Iterator;

fn main() {
    let input = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    let inputdecode: Vec<u8> = hex::decode(input).unwrap();
    println!("{:?}", inputdecode);
    let result:bool = inputdecode.iter().map(|x| x^128).map(|i| i < 128).fold(true,|isstr,i| isstr&&i);
    let resultstr:Vec<u8> = inputdecode.iter().map(|x| x^128).collect();
    let outstr = resultstr.iter().map(|x| chr(x)).collect();
    //collect();
    //let reshex = hex::encode(resultstr);
    println!("{:?}", resultstr);
     //   .fold(0, |sum, i| sum + i);

}
