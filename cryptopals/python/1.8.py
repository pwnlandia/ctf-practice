import requests, util, pprint

# Get input data
data = requests.get("http://cryptopals.com/static/challenge-data/8.txt").text
data = list(filter(bool, map(util.parsehex, data.split('\n'))))

# Search for repeated blocks
for i in data:
    blocks = []
    ct = i
    while ct:
        blocks.append(ct[:16])
        ct = ct[16:]
    if len(set(blocks)) < len(blocks):
        print(util.dumphex(i))
        pprint.pprint(blocks)
