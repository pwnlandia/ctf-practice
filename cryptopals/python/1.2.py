import util

a = util.parsehex("1c0111001f010100061a024b53535009181c")
b = util.parsehex("686974207468652062756c6c277320657965")
res = util.binary_op(a,b,lambda x,y: x^y)
util.check(res,"746865206b696420646f6e277420706c6179")
