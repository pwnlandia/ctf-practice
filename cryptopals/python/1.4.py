import util, string, urllib.request
from collections import Counter
FILE_URL = "http://cryptopals.com/static/challenge-data/4.txt"

file_data = urllib.request.urlopen(FILE_URL).read().decode('utf8')

def rank(c):
    ptx, key = c
    letters = len(list(filter(lambda x: x in (string.ascii_letters+" "), ptx)))
    punct = len(list(filter(lambda x: x in string.punctuation, ptx)))
    other = len(list(filter(lambda x:\
            x not in string.ascii_letters and x not in string.punctuation, ptx)))
    #print("%s -> (%d,%d,%d) = %d" % (ptx, letters, punct, other, punct + 2*other - letters))
    return 8*punct + 4*other - letters

def makeranks(l):
    a = util.parsehex(l)

    candidate_keys = []
    
    printable = frozenset(map(lambda x: ord(x), string.printable))
    for c in range(256):
        st = [char ^ c for char in a]
        x = frozenset(st)
        if(x.issubset(printable)):
            candidate_keys.append(("".join(map(chr, st)), c))
    candidate_keys.sort(key=rank)
    return candidate_keys

lns = file_data.split("\n")
for i in lns:
    r = makeranks(i)
    if len(r) > 0:
        print(i, r)
