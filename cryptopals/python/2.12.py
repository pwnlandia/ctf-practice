#!/usr/bin/python3

from Crypto.Cipher import AES
from Crypto import Random
import util, string, time

KEY = Random.new().read(16)
CONSTR = util.parseb64("""Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
YnkK""".replace("\n",""))

def encryption_oracle(data):
    data = util.pkcs7pad(data + CONSTR.decode('utf8'), 16)
    cipher = AES.new(KEY, AES.MODE_ECB,b"\x00"*16)
    return cipher.encrypt(data)

def permutations(base, l):
    if len(base) == l:
        yield base
    else:
        for i in string.printable:
            yield from permutations(base+i, l)

# Discover the cipher's block size
enc = encryption_oracle("A"*2000000)
block_size = None
for bs in range(1,16384):
    if enc[:bs] == enc[bs:bs*2]:
        block_size = bs
        break
else:
    print("=== OUT OF CHEESE ERROR ===")
    print("=== REINSTALL UNIVERSE AND REBOOT ===")
    exit()
print("Block size: %d" % block_size)

# Decrypt the appended text
output_string = ""

def next_char(block, working, n):
    global block_size

    # Compute encrypted text and PLB table
    enc_os_block = encryption_oracle("A"*(block_size-1-n))

    plb_prefix = working[-(block_size-1):]
    plb_prefix = plb_prefix.rjust(15,'A')
    plb_dict = dict(map(lambda x: (encryption_oracle(x)[:block_size],x),
        permutations(plb_prefix, block_size)))

    # Get the next character
    scan_blk = enc_os_block[block_size*block:][:block_size]
    return plb_dict[scan_blk][-1]

try:
    while True:
        output_string += next_char(len(output_string) // block_size,
                output_string, len(output_string) % block_size)
except:
    pass
print(output_string)
