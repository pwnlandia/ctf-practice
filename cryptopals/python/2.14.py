#!/usr/bin/python3

from Crypto.Cipher import AES
from Crypto import Random
from urllib.parse import urlparse
import util
import random
import string

KEY = Random.new().read(16)
PREFIX = Random.new().read(random.randint(0,16))
CONSTR = util.parseb64("""Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
YnkK""".replace("\n",""))
print(len(PREFIX))

def enc_oracle(data):
    data = util.pkcs7pad(PREFIX + data + CONSTR, 16)
    cipher = AES.new(KEY, AES.MODE_ECB,b"\x00"*16)
    return cipher.encrypt(data)

def permutations(base, l):
    if len(base) == l:
        yield base
    else:
        for i in string.printable:
            yield from permutations(base + i.encode('utf8'), l)

# Discover the cipher's block size
block_size = None
for bs in range(1,16384):
    al = len(enc_oracle(b'A' * (bs+1)))
    bl = len(enc_oracle(b'A' * bs))
    if al != bl:
        block_size = al - bl
        break
else:
    print("=== OUT OF CHEESE ERROR ===")
    print("=== REINSTALL UNIVERSE AND REBOOT ===")
    exit()
print("Block size: %d" % block_size)

# Discover the length of the unknown prefix (via aligning to block)
# This just encrypts a block of "B" and a block of "A", then adds "A" chars to
# the prefix until the B block is part of the output
b_block = enc_oracle(b'B' * 25000)[8*block_size:9*block_size]
alignment = None
for al in range(1, block_size):
    out = enc_oracle(b'A' * al + b'B' * block_size + b'A' * 1000)
    if b_block in out:
        alignment = al
        break
else:
    print("Cannot find alignment. Sacrifice goat and retry.")
    exit()
print("Alignment: {}".format(alignment))

# Create oracle variant that strips the prefix
prefixlen = enc_oracle(b'A' * alignment + b'B' * 512).find(b_block)
print("Prefix length: {}".format(prefixlen))
def pfxless_oracle(data):
    return enc_oracle(b'A' * alignment + data)[prefixlen:]

# Decrypt the appended text
output_string = b""

def next_char(block, working, n):
    global block_size

    # Compute encrypted text and PLB table
    enc_os_block = pfxless_oracle(b"A"*(block_size-1-n))

    plb_prefix = working[-(block_size-1):]
    plb_prefix = plb_prefix.rjust(15,b'A')
    plb_dict = dict(map(lambda x: (pfxless_oracle(x)[:block_size],x),
        permutations(plb_prefix, block_size)))

    # Get the next character
    scan_blk = enc_os_block[block_size*block:][:block_size]
    return plb_dict[scan_blk][-1]

try:
    while True:
        output_string += bytes([next_char(len(output_string) // block_size,
                output_string, len(output_string) % block_size)])
except KeyError as e:
    # This loop throws a KeyError on end of input
    pass
print(output_string)

