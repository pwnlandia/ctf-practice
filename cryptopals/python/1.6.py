import util, string, requests, base64
from collections import Counter

in_data = base64.b64decode(\
        requests.get(\
            "http://cryptopals.com/static/challenge-data/6.txt").text)

def rank(c):
    ptx, key = c
    letters = len(list(filter(lambda x: x in (string.ascii_letters+" "), ptx)))
    punct = len(list(filter(lambda x: x in string.punctuation, ptx)))
    other = len(list(filter(lambda x:\
            x not in string.ascii_letters and x not in string.punctuation, ptx)))
    #print("%s -> (%d,%d,%d) = %d" % (ptx, letters, punct, other, punct + 2*other - letters))
    return 8*punct + 4*other - letters

def makeranks(a):
    candidate_keys = []
    
    printable = frozenset(map(lambda x: ord(x), string.printable))
    for c in range(256):
        st = [char ^ c for char in a]
        x = frozenset(st)
        if(x.issubset(printable)):
            candidate_keys.append(("".join(map(chr, st)), c))
    candidate_keys.sort(key=rank)
    return candidate_keys

# Find average edit distance
def try_keysize(keysize):
    print("Trying ks=%d" % keysize)
    # Split into KEYSIZE blocks
    buf = in_data
    ks_blocks = []
    while len(buf) >= keysize:
        ks_blocks.append(buf[:keysize])
        buf = buf[keysize:]

    # Find average edit distance
    last = ks_blocks[0]
    edists = []
    for i in ks_blocks[1:]:
        edists.append(util.hamming(last, i) / keysize)
        last = i
    print("\tSuitability: %f" % (sum(edists)/len(edists)))
    return sum(edists)/len(edists)

# Return list of likely key sizes
def find_keysize():
    candidates = list(map(lambda x: (x,try_keysize(x)), range(2,32)))
    candidates.sort(key=lambda x: x[1])
    return candidates[:4]

# Try decryption using the specified keysize
def try_decrypt(ks):
    # Break into blocks
    blocks = []
    ct = in_data
    while len(ct) > 0:
        blocks.append(ct[:ks])
        ct = ct[ks:]
    
    # Pad last block if needed
    if(len(blocks[-1]) != ks):
        blocks[-1] += b"\x00"*(ks-len(blocks[-1]))

    # Transpose blocks
    transposed_blocks = [bytes([b[n] for b in blocks]) for n in range(ks)]

    # Break xor for each block
    key_elems = []
    for b in transposed_blocks:
        ranks = makeranks(b)
        if ranks:
            key_elems.append(ranks[0][1]) # Get the solved key for each
        else:
            return None
    if not key_elems:
        return None
    k = bytes(key_elems)
    print("Key is %s" % util.dumphex(k))

    # Try decrypting
    return bytes([in_data[n] ^ k[n % ks] for n in range(len(in_data))])

kss = find_keysize()
for ks,suitability in kss:
    print("Trying ks=%d" % ks)
    print(try_decrypt(ks))
