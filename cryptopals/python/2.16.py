#!/usr/bin/python3

from Crypto.Cipher import AES
from Crypto import Random
from urllib.parse import urlparse
import util

def parse_obj(ostr):
    return dict(map(lambda x: x.split('='), ostr.split('&')))

def profile_for(uname):
    return 'comment1=cooking%%20MCs;userdata={};comment2=%%20like%%20a%%20pound%%20of%%20bacon'.format(uname)

KEY = Random.new().read(16)

def encrypt_oracle(uname):
    p = profile_for(uname)
    enc = util.pkcs7pad(p, 16)
    cipher = AES.new(KEY, AES.MODE_CBC, b'\x00'*16)
    return cipher.encrypt(enc)

def validate(enc_prof):
    cipher = AES.new(KEY, AES.MODE_CBC, b'\x00'*16)
    dec = cipher.decrypt(enc_prof)
    encr = util.pkcs7strip(dec)
    print(encr)
    return b';admin=true;' in encr

# ---- Attack implementation starts here ----

# Find an input length that will make the encrypted string a multiple of the
# block length
prefix = None
block_size = None
base_len = len(encrypt_oracle(b""))
for i in range(64):
    enc = encrypt_oracle(b"A"*i)
    if len(enc) > base_len:
        prefix = b"A"*i
        block_size = len(enc) - base_len
        break
print("Found prefix length: {}".format(len(prefix)))

# Generate vulnerable ciphertext. Vulnerable ciphertext is aligned on a block
# boundary and contains 2*blocksize 'A's in a known position.
vuln_pt = prefix + b'A' * (100 * block_size)
vuln_ct = encrypt_oracle(vuln_pt)
safe_range_start = base_len + (block_size - (base_len % block_size))
print("Safe range:", safe_range_start)

# XOR the block
evil_ct = vuln_ct[:safe_range_start]
evil_ct += util.xor(
        vuln_ct[safe_range_start:safe_range_start+block_size],
        util.xor(b'A'*block_size, b';admin=true;'.rjust(block_size, b'A')))
evil_ct += vuln_ct[safe_range_start+block_size:]

# Win.
print(validate(evil_ct))
