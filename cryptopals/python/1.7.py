from Crypto.Cipher import AES
from Crypto import Random
import requests, util

data = util.parseb64(
        requests.get("http://cryptopals.com/static/challenge-data/7.txt").text)

key = b"YELLOW SUBMARINE"
cipher = AES.new(key, AES.MODE_ECB, Random.new().read(AES.block_size))
print(cipher.decrypt(data))
