#!/bin/python3
import util, requests
from Crypto.Cipher import AES
from Crypto import Random

FILE_URL = "http://cryptopals.com/static/challenge-data/10.txt"

cipher = AES.new("YELLOW SUBMARINE", AES.MODE_ECB, b"\x00"*16)

data = util.parseb64(requests.get(FILE_URL).text)
blks = []
while len(data) > 16:
    blks.append(data[:16])
    data = data[16:]

def pkcs7pad(data,sz):
	cnt = sz - (len(data) % sz)
	data += (cnt*chr(cnt))
	return data

if len(blks[-1]) < 16:
    blks[-1] = pkcs7pad(blks[-1],16)

xor = lambda a,b: a^b
nil,results = util.mapAccumL(lambda a,x:
        (x,util.binary_op(cipher.decrypt(x),a,xor)),
        b"\x00"*16, blks)
res = b"".join(results)
print(res)
