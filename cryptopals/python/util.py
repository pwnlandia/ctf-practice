import base64

def parsehex(s):
    return bytes([int('0x'+s[n:n+2],16) for n in range(0,len(s),2)])

def dumphex(b):
    return "".join([hex(n)[2:].rjust(2,'0') for n in b])

def parseb64(s):
    return base64.b64decode(s)

def dumpb64(b):
    return base64.b64encode(b)

def binary_op(a,b,o):
    return bytes([o(a[n], b[n]) for n in range(len(a))])

def xor(a, b):
    return binary_op(a, b, lambda a,b: a^b)

def check(x,r):
    if x == parsehex(r):
        print("Solution OK")
    else:
        print("Incorrect result: %s" % dumphex(x))

def tobits(s):
    result = []
    for c in s:
        bits = bin(ord(c))[2:]
        bits = '00000000'[len(bits):] + bits
        result.extend([int(b) for b in bits])
    return result

def frombits(bits):
    chars = []
    for b in range(len(bits) / 8):
        byte = bits[b*8:(b+1)*8]
        chars.append(chr(int(''.join([str(bit) for bit in byte]), 2)))
    return ''.join(chars)

def hamming(s1, s2):
    s1 = tobits(s1)
    s2 = tobits(s2)
    assert(len(s1) == len(s2))
    total = 0
    for i in range(0, len(s1)):
        if s1[i] != s2[i]:
            total += 1
    return total

# from http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance
def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)
 
    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)
 
    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
 
    return previous_row[-1]

def mapAccumL(accum, acc, xs):
    res = []
    for i in xs:
        acc, d = accum(acc, i)
        res.append(d)
    return (acc, res)


def pkcs7pad(data,sz):
    if type(data) == str:
        data = data.encode('utf8')

    cnt = sz - (len(data) % sz)
    data += bytes(cnt*[cnt])
    return data

def pkcs7strip(data):
    cnt = data[-1]
    if not data.endswith(data[-1:] * cnt):
        raise ValueError("Bad padding")
    return data[:-cnt]
