#!/bin/python3
def pkcs7pad(data,sz):
	cnt = sz - (len(data) % sz)
	data += (cnt*chr(cnt))
	return data

newdata = pkcs7pad("hello",100)
print(newdata,len(newdata))
