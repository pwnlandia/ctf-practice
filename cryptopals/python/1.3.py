import util, string
from collections import Counter

a = util.parsehex("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")

candidate_keys = []

printable = frozenset(map(lambda x: ord(x), string.printable))
for c in range(256):
    st = [char ^ c for char in a]
    x = frozenset(st)
    if(x.issubset(printable)):
        candidate_keys.append(("".join(map(chr, st)), c))

def rank(c):
    string, key = c
    freqs = Counter(string.lower())
    commons = "".join(map(lambda x: x[0], freqs.most_common(6)))
    lev = util.levenshtein(commons, "etaoin")
    return lev

candidate_keys.sort(key=rank)
#candidate_keys = list(map(lambda x :x[0], candidate_keys))
print(candidate_keys)
