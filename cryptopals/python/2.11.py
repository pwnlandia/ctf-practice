#!/bin/python3
import util, requests
from Crypto.Cipher import AES
from Crypto import Random
import random

def generateAESkey():
    key = rndfile.read(16)
    return key

def encryption_oracle(data):
    aeskey = generateAESkey()
    head = 5 + ord(rndfile.read(1)) % 5
    tail = 5 + ord(rndfile.read(1)) % 5
    prebytes = rndfile.read(head)
    postbytes = rndfile.read(tail)
    data = util.pkcs7pad(str(prebytes) + data + str(postbytes), 16)
    # mode = random.randrange(0,1)
    mode = ord(rndfile.read(1)) % 2
    if mode == 0:
        cipher = AES.new(aeskey, AES.MODE_ECB,b"\x00"*16)
    else:
        iv = rndfile.read(16)
        cipher = AES.new(aeskey, AES.MODE_CBC,iv)
        
    return mode,cipher.encrypt(data)

def detectMode(data):
    blocks = []
    ct = data
    while ct:
        blocks.append(ct[:16])
        ct = ct[16:]
    if len(set(blocks)) < len(blocks):
        #print("ECB")
        return 0
    else:
        #print("CBC")
        return 1

rndfile = Random.new()
ok = 0
fail = 0
for i in range(8192):
    mode,data = encryption_oracle("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    #print(data)
    if detectMode(data) == mode:
        ok += 1
    else:
        fail += 1
    print("\rOK %d FAIL %d R %f" % (ok,fail,ok/(ok+fail)),end='')
print("\n")
