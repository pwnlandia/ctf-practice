#!/usr/bin/python3

from Crypto.Cipher import AES
from Crypto import Random
from urllib.parse import urlparse
import util

def parse_obj(ostr):
    return dict(map(lambda x: x.split('='), ostr.split('&')))

def profile_for(email):
    return 'email={}&uid={}&role={}'.format(
            email.replace('&','').replace('=',''), 10, 'user')

KEY = Random.new().read(16)

def encrypt_oracle(email):
    p = profile_for(email)
    enc = util.pkcs7pad(p, 16)
    cipher = AES.new(KEY, AES.MODE_ECB, b'\x00'*16)
    return cipher.encrypt(enc)

def validate(enc_prof):
    cipher = AES.new(KEY, AES.MODE_ECB, b'\x00'*16)
    dec = cipher.decrypt(enc_prof)
    encr = util.pkcs7strip(dec).decode('utf8')
    try:
        return parse_obj(encr)['role'] == 'admin'
    except Exception as e:
        return False

# ---- Attack implementation starts here ----

# Find an input length that will make the encrypted string a multiple of the
# block length
prefix = None
base_len = len(encrypt_oracle(""))
for i in range(64):
    enc = encrypt_oracle("A"*i)
    if len(enc) > base_len:
        prefix = "A"*i
        break
print("Found prefix length: {}".format(len(prefix)))

# Add four bytes so the user type starts on a block boundary. We'll swap the
# last block for something else later.
patient_a = encrypt_oracle(prefix + 'AAAA')

# Generate a properly-padded block that begins with 'admin' and is full of
# PKCS7 padding.
blksize = len(encrypt_oracle(prefix)) - base_len
blkidx = (len(encrypt_oracle(prefix)) // blksize) - 1
print("Block size: {}\nBlock index: {}".format(blksize, blkidx))
patient_b = encrypt_oracle(prefix + 'a' +\
        (util.pkcs7pad('admin', blksize)).decode('utf8'))\
        [(blkidx-1)*16:blkidx*16]
print("Patient B: {}".format(patient_b))

# Splice them
generated = patient_a[:-16] + patient_b

print(validate(generated))
