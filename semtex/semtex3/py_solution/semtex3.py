#!/usr/bin/python
# Solution is 2 3 1 1 3 1 2 0
v0 = [5,2,1,7,5]
v1 = [13,-7,-4,1,5]
v2 = [9,12,9,70,-4]
v3 = [-11,9,0,5,-13]
v4 = [4,17,12,9,24]
v5 = [11,-17,21,5,14]
v6 = [15,31,22,-12,3]
v7 = [19,-12,4,3,-7]
varray = [v0,v1,v2,v3,v4,v5,v6,v7]
answer = [0,0,0,0,0,0,0,0]
answer_vec = [5,2,1,7,5]
a = [0,0,0,0,0,0,0,0]

def calc_vec(varray,a):
    ans = [0,0,0,0,0]
    for v in xrange(8):
        for i in xrange(5):
            ans[i] = ans[i]+a[v]*varray[v][i]
    return ans

def print_vec(answer_vec, answer):
    thevals = " ".join([str(val)+" " for val in answer_vec])
    print(thevals)
    thevec= " ".join([str(val)+" " for val in answer])
    print(thevec)

for i in xrange(99999999):
    a[0] = i%10
    a[1] = (i/10)%10
    a[2] = (i/100)%10
    a[3] = (i/1000)%10
    a[4] = (i/10000)%10
    a[5] = (i/100000)%10
    a[6] = (i/1000000)%10
    a[7] = (i/10000000)%10
    thevals = " ".join([str(val)+" " for val in a])
    foo = calc_vec(varray,a)
    if (foo[0] == 100) and (foo[1] == 100) and (foo[2] == 100) and (foo[3] == 100) and (foo[4] == 100):
            print(a)
            break
