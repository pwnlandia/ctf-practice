#include <stdio.h>

#ifdef DEBUG
# define DBG_DO(stmt) stmt
#else
# define DBG_DO(stmt)
#endif

typedef int v[5];

v init  =   {300,300,300,300,300};
v targ  =   {400,400,400,400,400};
v zero  =   {  0,  0,  0,  0,  0};
v vs[8] = { {  5,  2,  1,  7,  5}
          , { 13, -7, -4,  1,  5}
          , {  9, 12,  9, 70, -4}
          , {-11,  9,  0,  5,-13}
          , {  4, 17, 12,  9, 24}
          , { 11,-17, 21,  5, 14}
          , { 15, 31, 22,-12,  3}
          , { 19,-12,  4,  3, -7}
          };

int *vset(v dst, v src) {
    dst[0] = src[0];
    dst[1] = src[1];
    dst[2] = src[2];
    dst[3] = src[3];
    dst[4] = src[4];

    return dst;
}

int *vadd(v res, v v1, v v2) {
    v scratch = {v1[0]+v2[0], v1[1]+v2[1], v1[2]+v2[2], v1[3]+v2[3], v1[4]+v2[4]};
    vset(res, scratch);
    return res;
}

int *vmul(v res, v v1, int c) {
    int i;
    vset(res, zero);
    for(i = 0; i < c; ++i)
        vadd(res, res, v1);
    return res;
}

int veq(v v1, v v2) {
    return v1[0]==v2[0] && v1[1]==v2[1] && v1[2]==v2[2] && v1[3]==v2[3] && v1[4]==v2[4];
}

void vshow(v v1) {
    printf("[%4d,%4d,%4d,%4d,%4d]\n", v1[0], v1[1], v1[2], v1[3], v1[4]);
}

void print_nonal(int i) {
    if(!i)
        puts("0");
    else {
        while(i) {
            printf("%d", i%9);
            i /= 9;
        }
        puts("");
    }
}

int main(void) {
    v scratch, res;
    int cs[8], cmax = 3, i;

    for(cs[0] = 0; cs[0] <= cmax; ++cs[0])
    for(cs[1] = 0; cs[1] <= cmax; ++cs[1])
    for(cs[2] = 0; cs[2] <= cmax; ++cs[2])
    for(cs[3] = 0; cs[3] <= cmax; ++cs[3])
    for(cs[4] = 0; cs[4] <= cmax; ++cs[4])
    for(cs[5] = 0; cs[5] <= cmax; ++cs[5])
    for(cs[6] = 0; cs[6] <= cmax; ++cs[6])
    for(cs[7] = 0; cs[7] <= cmax; ++cs[7]) {
        DBG_DO(printf("trying: "
                      "%dv0 + %dv1 + %dv2 + %dv3 + %dv4 + %dv5 + %dv6 + %dv7\n",
                      cs[0], cs[1], cs[2], cs[3], cs[4], cs[5], cs[6], cs[7]));
        vset(res, init);
        for(i = 0; i < 8; ++i)
            vadd(res, res, vmul(scratch, vs[i], cs[i]));
        DBG_DO(printf("got: "); vshow(res); getchar());

        if(veq(res, targ)) {
            printf("got it:"
                   "%dv0 + %dv1 + %dv2 + %dv3 + %dv4 + %dv5 + %dv6 + %dv7\n",
                    cs[0], cs[1], cs[2], cs[3], cs[4], cs[5], cs[6], cs[7]);
            return 0;
        }
    }
    
    return 0;
}
