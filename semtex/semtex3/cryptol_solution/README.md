Cryptol Solution
================

This solution requires cryptol. To get it you can

- run `cabal install cryptol`
- clone [the repository](https://github.com/GaloisInc/cryptol) and run
  `make install`

Then run `cryptol`, then run the repl in the `cryptol_solution` directory and
type

    :l combos.cry
    :sat existsCombo`{4}
