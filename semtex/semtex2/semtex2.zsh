#!/bin/zsh
# Need to change based on paths on semtex.labs.overthewire.org
# Send this script "semtex2.c"
gcc -shared -fPIC -m32 -o ${1/.c/.so} ${1}
export LD_PRELOAD=./${1/.c/.so}
/semtex/semtex2
