This is the same binary that we used to get the semtex4 password from semtex3.
It checks your EUID and dumps out your password. The problem is that your EUID
is for semtex4 and you want the program to believe you are semtex5.  Since
the binary is statically compiled, LD_PRELOAD won't work.  The level suggests
ptrace.  ptrace allows you to regain control over processes when system
calls are invoked.  The strategy would be to run until a system call is
reached, check whether or not it is a geteuid32 call (system call # 201
taken from /usr/include/asm/unistd_32.h), and to change the return value of
the call (stored in eax) from 6004 (semtex4) to 6005 (semtex5)





















password is HELICOTRMA
