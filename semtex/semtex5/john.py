import sys
import time
import socks
import _thread

PW=b'HELICOTRMA'
ID=b'PDXJOHNNY1'

all_done = 0


def binary_op(a,b,o):
    return bytes([o(a[n], b[n]) for n in range(len(a))])

def con(i):
    s = socks.socksocket()
    s.setproxy(socks.PROXY_TYPE_SOCKS5, sys.argv[1], 9000 + i)
    s.connect(("semtex.labs.overthewire.org",24027))
    print("Connnected")
    data = s.recv(10)
    print("data: " + str(data))
    return data, s

def doit(i):
    d, s = con(i)
    sendback = binary_op(PW, d, lambda a,b: a^b)
    sendback += ID
    s.sendall(sendback)
    print("Password: " + str(s.recv(20)))
    done()

def done():
    global all_done
    all_done += 1
    if all_done == 10:
        print("Everyone is done")
        sys.exit(0)

for i in range(0, 9):
    _thread.start_new_thread(doit, (i,))

time.sleep(500)

