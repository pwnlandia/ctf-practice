#!/usr/bin/python
import socks, time

PASSWORD = b"HELICOTRMA"

proxies = [socks.socksocket() for x in range(10)]

servers = [	("localhost", 9000),
		("localhost", 9001),
		("localhost", 9002),
		("localhost", 9003),
		("localhost", 9004),
		("localhost", 9005),
		("localhost", 9006),
		("localhost", 9007),
		("localhost", 9008),
		("localhost", 9009)]

for p, s in zip(proxies, servers):
	print(s)
	p.set_proxy(socks.SOCKS5, *s)

def recvall(s, n):
	buf = bytearray()
	while len(buf) < n:
		buf += s.recv(n - len(buf))
	return bytes(buf)

xorpw = lambda x: bytes(map(lambda y: y[0]^y[1], zip(PASSWORD,x)))

for p in proxies:
	print("Connecting...")
	p.connect(("semtex.labs.overthewire.org", 24027))

	print("Receiving...")
	chars = recvall(p, 10)
	print("in: " + repr(chars))
	print("out: " + repr(xorpw(chars)))
	send = (xorpw(chars) + b'pwnlandia0')
	if len(send) != 20:
		print("error")
	print("send: " + repr(send))
	p.sendall(send)

time.sleep(5)

for p in proxies:
	print(p.recv(64))
