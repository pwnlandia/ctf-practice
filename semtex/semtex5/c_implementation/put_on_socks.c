#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>

#define DIE_IF(EXPR) \
do{ \
  errno = 0; \
  char _buf[4096]; \
  snprintf(_buf, sizeof _buf, "%s:%d\t%s", __FILE__, __LINE__, #EXPR); \
  if(EXPR) { \
    if(errno) \
      perror(_buf); \
    else \
      fprintf(stderr, "%s: FAILURE\n", _buf); \
    exit(1); \
  } \
}while(0)

struct node {
  char *addr;
  char *port;
} proxies[] = { {"222.187.210.218", "1080"}
              , {"212.47.229.71",   "9005"}
              , {"129.241.126.49",  "10200"}
              , {"104.156.20.192",  "10200"}
              , {"204.42.255.230",  "64472"}
              , {"66.253.135.231",  "10200"}
              , {"187.94.99.197",   "1080"}
              , {"79.127.117.100",  "80"}
              , {"149.202.68.167",  "37231"}
              , {"75.105.4.48",     "10200"}
              , {"212.47.236.192",  "9001"}
              , {"186.233.176.38",  "1080"}
              , {"64.188.252.180",  "10200"}
              , {"212.47.239.185", "9013"}
              , {"212.47.226.97", "9012"}
              , {"184.173.240.231", "20911"}
              , {"74.51.163.41", "10200"}
              , {"75.118.229.68", "10200"}
              , {"212.47.226.97", "9006"}
              , {"75.107.159.151", "10200"}
              , {"191.37.30.1", "1080"}
              , {"189.219.239.92", "10000"}
              , {"70.124.40.170", "10200"}
              , {"104.238.138.90", "1080"}
              , {"72.12.94.181", "10200"}
              , {"198.45.183.201", "10200"}
              , {"83.233.148.57", "10200"}
              , {"64.188.132.92", "10200"}
              , {"202.117.54.118", "1080"}
              , {"217.160.133.151", "64815"}
              };
struct node target = {"semtex5.labs.overthewire.org", "24027"};
int fds[sizeof proxies / sizeof *proxies];

struct thread_info {char *proxy_addr; char *proxy_port; int fd; int id;};

void *put_on_socks(void *p)
{
  struct addrinfo *proxy = NULL, *dst = NULL, hints;
  int sockfd = -1, err, id = (int)(long)p, i;
  char buf[1024], pw[32];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  err = getaddrinfo(proxies[id].addr, proxies[id].port, &hints, &proxy);
  if(err) {
    snprintf(buf, sizeof buf, "thread %d: %s", id, gai_strerror(err));
    goto write;
  }
  err = getaddrinfo(target.addr, target.port, &hints, &dst);
  if(err) {
    snprintf(buf, sizeof buf, "thread %d: %s", id, gai_strerror(err));
    goto write;
  }

  struct sockaddr_in *dstaddr = (struct sockaddr_in*)dst->ai_addr;
  struct sockaddr_in *prxaddr = (struct sockaddr_in*)proxy->ai_addr;
  char dstaddrstr[16], prxaddrstr[16];
  strcpy(dstaddrstr, inet_ntoa(dstaddr->sin_addr));
  strcpy(prxaddrstr, inet_ntoa(prxaddr->sin_addr));
  snprintf(buf, sizeof buf, "thread %d: trying to socks to %s:%d via %s:%d", id
      , dstaddrstr, ntohs(dstaddr->sin_port)
      , prxaddrstr, ntohs(prxaddr->sin_port));
  write(fds[id], buf, sizeof buf);

  sockfd = socket(proxy->ai_family, proxy->ai_socktype, 0);
  if(sockfd < 0) {
    snprintf(buf, sizeof buf, "thread %d: socket(%d,%d) failed", id, proxy->ai_family, proxy->ai_socktype);
    goto write;
  }
  err = connect(sockfd, proxy->ai_addr, sizeof(struct sockaddr_in));
  if(err) {
    snprintf(buf, sizeof buf, "thread %d: connect() failed (%d)", id, errno);
    goto write;
  }

  snprintf(buf, 9, "\x04\x01%2s%4s\x00", (char*)&dstaddr->sin_port, (char*)&dstaddr->sin_addr);
  while(send(sockfd, buf, 9, 0) != 9);
  while(recv(sockfd, buf, 8, 0) != 8);
  if(buf[1] != 0x5a) {
    snprintf(buf, sizeof buf, "thread %d: server refused with code %#x", id, buf[1]);
    goto write;
  } else {
    snprintf(buf, sizeof buf, "thread %d: server said ok", id, buf[1]);
    write(fds[id], buf, sizeof buf);
  }

  while(recv(sockfd, buf, 10, 0) != 10);
  for(i = 0; i < 10; ++i) buf[i] ^= "HELICOTRMA"[i];
  memcpy(&buf[10], "foozleblat", 10);
  while(send(sockfd, buf, 20, 0) != 20);
  err = recv(sockfd, pw, sizeof(pw)-1, 0);
  if(err > 0) {
    pw[err] = '\0';
    snprintf(buf, sizeof buf, "SUCCESS! thread %d: got password %s", id, pw);
  } else {
    goto end;
  }
  
write:
  write(fds[id], buf, sizeof buf);
end:
  if(sockfd >= 0) close(sockfd);
  if(proxy) freeaddrinfo(proxy);
  if(dst) freeaddrinfo(dst);
  return NULL;
}

int main(void)
{
  int nproxies = sizeof proxies / sizeof *proxies;
  int i, pipefds[2], err, readfds[nproxies], len;
  char buf[1024];
  pthread_t threads[nproxies];
  fd_set set;

  FD_ZERO(&set);
  for(i = 0; i < nproxies; ++i) {
    DIE_IF(err = pipe(pipefds));
    readfds[i] = pipefds[0];
    fds[i] = pipefds[1];
    FD_SET(readfds[i], &set);
    pthread_create(&threads[i], NULL, put_on_socks, (void*)(long)i);
  }

  while(select(readfds[nproxies-1]+1, &set, NULL, NULL, NULL) != -1) {
    for(i = 0; i < nproxies; ++i)
      if(FD_ISSET(readfds[i], &set)) {
        len = read(readfds[i], buf, sizeof(buf));
        puts(buf);
        if(!memcmp(buf, "SUCCESS!", 8))
          goto done;
      }
    FD_ZERO(&set);
    for(i = 0; i < nproxies; ++i)
      FD_SET(readfds[i], &set);
  }

done:
  return 0;
}
