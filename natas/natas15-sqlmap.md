Solving natas 15 with sqlmap
============================

Natas 15 can be solved automatically with `sqlmap`. The following is an
invocation to do so:

```sh
sqlmap -u 'http://natas15.natas.labs.overthewire.org' \
        --auth-type basic \
        --auth-cred natas15:AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J \
        --data username=foo \
        --dbms mysql \
        --dump \
        --level 2 \
        --batch \
        --time-sec 1
```

The meaning of the flags is as follows:
* `--auth-type`, `--auth-cred`: Lets `sqlmap` log into the challenge. The
  credentials are the same ones you would use to log into it with your browser.
* `--data`: Tells `sqlmap` that you want it to try to inject into the POST
  parameter `username`.
* `--dbms`: Tells `sqlmap` what the backend database server is. You can
  find this out using `nmap` or other tools. If you omit this parameter,
  `sqlmap` will take longer to finish, since it has to figure out the
  information for itself.
* `--dump`: Dump the all the information in all tables all tables.
* `--level`: Setting this above 1 (max 5) tells `sqlmap` to try more
  attack-types and payloads. The payload we need isn't included at level 1, so
  we'll set this to 2.
* `--batch`: Tells `sqlmap` not to prompt us with questions, and just use the
  default behavior.
* `--time-sec`: Sleep time to inject when doing timing-based attacks. You might
  need to raise this if your connection to the natas server is overloaded.

The attack takes about ten minutes to run, much of which is due to `sqlmap`
dumping the entire user database with a time-based attack.
